/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   AMiRoDefault configuration application container.
 */

#include "apps.h"
#include <math.h>
#include <amiroos.h>
#include <AMiRoDefault_topics.h>
#include <AMiRoDefault_services.h>
#include <amiro_differentialmotionestimator.h>
#include <amiro_differentialmotorcontrol.h>
#include <amiro_floor.h>
#include <accelerometer.h>
#include <gyroscope.h>
#include <compass.h>
#include <canbridge.h>
#include <edgefollowing.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#if !defined(M_PI)
#define M_PI                                      3.14159265358979323846
#endif

/**
 * @brief   Wheel diameter in um.
 * @todo    Should be two values (left & right) stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_DIAMETER                            55710

/**
 * @brief   Wheel offset from the center in um.
 * @todo    Should be stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_OFFSET                              (34000 + (9000/2))

/**
 * @brief   Frequency of the DME in Hertz.
 */
#define DME_FREQUENCY                             100

/**
 * @brief   Driver frequency (in Hz) of the accelerometer, gyroscope and compass.
 */
#define IMU_FREQUENCY                             50.0f

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Topics for the different data.
 */
static urt_topic_t _amiro_floor_proximity_topic;
static urt_topic_t _amiro_floor_ambient_topic;
static urt_topic_t _acco_topic;
static urt_topic_t _gyro_topic;
static urt_topic_t _compass_topic;
static urt_topic_t _edgefollowing_event_topic;

/*
 * Payloads of the different data.
 */
static floor_proximitydata_t _amiro_floor_proximity_payload;
static floor_ambientdata_t _amiro_floor_ambient_payload;
static gyro_sensor_t   _gyro_payload;
static acco_sensor_t   _acco_payload;
static compass_sensor_t _compass_payload;
static edgefollowing_event_data_t _edgefollowing_event_payload;

/**
 * @brief   DME instance.
 */
static amiro_dme_t _amiro_dme;

/*
 * Floor node instance.
 */
static amiro_floor_node_t _amiro_floor;

/*
 * Gyroscope node instance
 */
static gyro_node_t _gyro;

/*
 * Accelerometer node instance
 */
static acco_node_t _acco;

/*
 * Compass node instance
 */
static compass_node_t _compass;

/**
 * EdgeFollowing node instance.
 */
static edgefollowing_node_t _edgefollowing;


/**
 * @brief   DME configuration.
 */
static const amiro_dme_config_t _amiro_dme_config = {
  .left = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_LEFT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .right = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_RIGHT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .interval = MICROSECONDS_PER_SECOND / DME_FREQUENCY,
};

/**
 * @brief   DME motion topic.
 */
static urt_topic_t _amiro_dme_motion_topic;

/**
 * @brief   Payload for the DME motion topic mandatory message.
 */
static dme_motionpayload_t _amiro_dme_motion_topic_payload;

/**
 * @brief   DMC instance.
 */
static amiro_dmc_t _amiro_dmc;

/**
 * @brief   DMC configuration.
 */
static const amiro_dmc_config_t _amiro_dmc_config = {
  .motors = {
    .left = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD,
      },
    },
    .right = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD,
      },
    },
  },
  .lpf = {
    .factor = 10.0f,
    .max = {
      .steering = 3.14f,
      .left = 1.0f,
      .right = 1.0f,
    },
  },
};

/******************************************************************************/
/* CAN DATA                                                                   */
/******************************************************************************/

/**
 * Event masks to set on can events.
 */
#define CAN_FLOOR_PROXEVENT                 (urt_osEventMask_t)(1<< 1)


#define NUM_SERVICES  0
#define NUM_SUBSCRIBER  1
/**
 * @brief   CANBridge related data.
 */
struct {
  /**
   * @brief   CANBridge application node.
   */
  canBridge_node_t app;

  /**
   * @brief   Set of Service list elements for the DiWheelDrive.
   */
  canBridge_service_list_t services[NUM_SERVICES];

  /**
   * @brief   Set of Subscriber list elements for the DiWheelDrive.
   */
  canBridge_subscriber_list_t subscriber[NUM_SUBSCRIBER];
} _can;

urt_service_t can_service[NUM_SERVICES];
size_t payload_sizes[NUM_SERVICES];

urt_nrtsubscriber_t can_subscriber[NUM_SUBSCRIBER];
urt_osEventMask_t subscriber_masks[NUM_SUBSCRIBER];
size_t subscriber_payload_sizes[NUM_SUBSCRIBER];
urt_topicid_t subscriber_topic_ids[NUM_SUBSCRIBER];

urt_service_t can_service[NUM_SERVICES];
size_t payload_sizes[NUM_SERVICES];

#define NUM_REQUESTS 2
urt_serviceid_t service_IDS[NUM_REQUESTS];

#define NUM_PUBLISHER   0
urt_topicid_t pub_topic_IDs[NUM_PUBLISHER];


/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC target velocity shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of command arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setVelocity(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_setVelocity(stream, argc, argv, urtCoreGetService(SERVICEID_DMC_TARGET));
}

/**
 * @brief   DMC target velocity shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setVelocity, "DMC:setVelocity", _appsDmcShellCmdCb_setVelocity);

/**
 * @brief   DMC get gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_getGains(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_getGains(stream, argc, argv, &_amiro_dmc);
}

/**
 * @brief   DMC get gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_getGains, "DMC:getGains", _appsDmcShellCmdCb_getGains);

/**
 * @brief   DMC set gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setGains(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_setGains(stream, argc, argv, &_amiro_dmc);
}

/**
 * @brief   DMC set gains shell command.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setGains, "DMC:setGains", _appsDmcShellCmdCb_setGains);

#if (DMC_CALIBRATION_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC auto calibration shell coammand callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_autoCalibration(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return dmcShellCallback_autoCalibration(stream, argc, argv, urtCoreGetService(SERVICEID_DMC_CALIBRATION));
}

/**
 * @brief   DMC auto calibration shell coammand.
 */
static AOS_SHELL_COMMAND(_appsDmcShellCmd_autoCalibration, "DMC:calibration", _appsDmcShellCmdCb_autoCalibration);

#endif /* (DMC_CALIBRATION_ENABLE == true) */

/**
 * @brief   Floor offset calibration shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _amiro_floorCalibShellCmdCb(BaseSequentialStream* stream, int argc, const char* argv[])
{
  return floorShellCallback_calibrate(stream, argc, argv, urtCoreGetService(SERVICEID_FLOOR_CALIBRATION));
}

/**
 * @brief   Floor offset calibration shell coammand.
 */
static AOS_SHELL_COMMAND(_amiro_floorCalibShellCmd, "floor:calibration", _amiro_floorCalibShellCmdCb);

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/
/**
 * @addtogroup configs_amirodefault_modules_diwheeldrive11
 * @{
 */


/**
 * @brief   Initializes all data applications for the AMiRo default configuration.
 */
void appsInit(void)
{
  // initialize common data
  appsCommonInit();

  // initialize the different topics
  urtTopicInit(&_amiro_dme_motion_topic, TOPICID_DME, &_amiro_dme_motion_topic_payload);
  urtTopicInit(&_amiro_floor_proximity_topic, TOPICID_FLOOR_PROXIMITY, &_amiro_floor_proximity_payload);
  urtTopicInit(&_amiro_floor_ambient_topic, TOPICID_FLOOR_AMBIENT, &_amiro_floor_ambient_payload);
  urtTopicInit(&_gyro_topic, TOPICID_GYROSCOPE, &_gyro_payload);
  urtTopicInit(&_acco_topic, TOPICID_ACCELEROMETER, &_acco_payload);
  urtTopicInit(&_compass_topic, TOPICID_COMPASS, &_compass_payload);
  urtTopicInit(&_edgefollowing_event_topic, TOPICID_EDGEFOLLOWING_EVENT, &_edgefollowing_event_payload);

  // initialize DME app
  amiroDmeInit(&_amiro_dme, &_amiro_dme_config, TOPICID_DME, URT_THREAD_PRIO_RT_MAX);

  // initialize DMC app
#if (DMC_CALIBRATION_ENABLE == true)
  amiroDmcInit(&_amiro_dmc, &_amiro_dmc_config, TOPICID_DME, SERVICEID_DMC_TARGET, SERVICEID_DMC_CALIBRATION, URT_THREAD_PRIO_RT_MAX);
#else
  amiroDmcInit(&_amiro_dmc, &_amiro_dmc_config, TOPICID_DME, SERVICEID_DMC_TARGET, URT_THREAD_PRIO_RT_MAX);
#endif

  // initialize Floor app
  amiroFloorInit(&_amiro_floor,
            &moduleLldProximity,
            &moduleLldI2cMultiplexer,
            VCNL4020_LLD_PROXRATEREG_62_5_HZ,
            &_amiro_floor_proximity_topic,
            SERVICEID_FLOOR_CALIBRATION,
            (VCNL4020_LLD_ALPARAMREG_RATE_10_HZ | VCNL4020_LLD_ALPARAMREG_AUTOOFFSET_DEFAULT | VCNL4020_LLD_ALPARAMREG_AVG_64_CONV),
            &_amiro_floor_ambient_topic,
            URT_THREAD_PRIO_RT_MAX-1);

    // initialize AMiRo CAN app
  service_IDS[0] = SERVICEID_DMC_TARGET;
  service_IDS[1] = SERVICEID_EDGEFOLLOWING;

  subscriber_masks[0] = CAN_FLOOR_PROXEVENT;
  subscriber_payload_sizes[0] = sizeof(floor_proximitydata_t);
  subscriber_topic_ids[0] = TOPICID_FLOOR_PROXIMITY;
  _can.subscriber[0].subscriber = &can_subscriber[0];
  _can.subscriber[0].topic_id = subscriber_topic_ids[0];
  _can.subscriber[0].payload_size = subscriber_payload_sizes[0];
  _can.subscriber[0].transmit_factor = 1;
  _can.subscriber[0].mask = &subscriber_masks[0];
  _can.subscriber[0].next = NULL;

//  pub_topic_IDs[0] = TOPICID_PROXIMITYSENSOR_PROXIMITY;

  // initialize AMiRo CAN app
  canBridgeInit(&_can.app,
               _can.subscriber,
               pub_topic_IDs,
               (uint8_t)NUM_PUBLISHER,
               NULL, //_can.services,
               service_IDS,
               (uint8_t)NUM_REQUESTS,
               URT_THREAD_PRIO_HIGH_MAX);


  // initialize Gyroscope app
  gyroInit(&_gyro,
           TOPICID_GYROSCOPE,
           URT_THREAD_PRIO_NORMAL_MIN,
           IMU_FREQUENCY);

  // initialize Accelerometer app
  accoInit(&_acco,
           TOPICID_ACCELEROMETER,
           URT_THREAD_PRIO_NORMAL_MIN,
           IMU_FREQUENCY);

  // initialize Compass app
  compassInit(&_compass,
              TOPICID_COMPASS,
              URT_THREAD_PRIO_NORMAL_MIN,
              IMU_FREQUENCY);

  edgefollowingInit(&_edgefollowing,
                SERVICEID_EDGEFOLLOWING,
                SERVICEID_DMC_TARGET,
                TOPICID_EDGEFOLLOWING_EVENT,
                TOPICID_FLOOR_PROXIMITY,
                //TOPICID_PROXIMITYSENSOR_PROXIMITY,
                URT_THREAD_PRIO_HIGH_MIN);


#if (AMIROOS_CFG_SHELL_ENABLE == true)
  // add DMC shell commands
  aosShellAddCommand(&_appsDmcShellCmd_setVelocity);
  aosShellAddCommand(&_appsDmcShellCmd_getGains);
  aosShellAddCommand(&_appsDmcShellCmd_setGains);
#if (DMC_CALIBRATION_ENABLE == true)
  aosShellAddCommand(&_appsDmcShellCmd_autoCalibration);
#endif /* (DMC_CALIBRATION_ENABLE == true) */
  aosShellAddCommand(&_amiro_floorCalibShellCmd);
#endif /* AMIROOS_CFG_SHELL_ENABLE == true */

  return;
}

/** @} */
