/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    AMiRoDefault_services.h
 * @brief   AMiRoDefault µRT service identifiers.
 *
 * @addtogroup configs_amirodefault_modules
 * @{
 */

#ifndef AMIRODEFAULT_SERVICES_H
#define AMIRODEFAULT_SERVICES_H

#include <urt.h>
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/motiondata.h"
#if (URT_CFG_RPC_ENABLED == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

//Ids need to be bitwise for the CANBridge filter and not 0
//Maximum number of IDs is 16

#define NUM_SERVICEIDS 6

/**
 * @brief   Service identifiers.
 */
enum services {
  SERVICEID_LIGHT,                           /**< Light service. */ /*payload=LightRing_leddata.h:light_led_data_t*/
  SERVICEID_DMC_TARGET,                      /**< DifferentialMotorControl target motion service. */ /*payload=motiondata.h:motion_ev_csi*/
  SERVICEID_DMC_CALIBRATION,                 /**< DifferentialMotorControl calibration service. */ /*payload=*/
  SERVICEID_FLOOR_CALIBRATION,               /**< Floor proximity sensor calibration service. */ /*payload=*/
  SERVICEID_PROXIMITYSENSOR_CALIBRATION,     /**< ProximitySensor proximity sensor calibration service. */ /*payload=*/
  SERVICEID_ODOMETRY                         /**< Odometry reset values service. */ /*payload=*/
};

/**
 * @brief List of all payload sizes indexed by service id.
 */
static size_t service_payload_sizes[NUM_SERVICEIDS] = {
  sizeof(light_led_data_t),     // SERVICEID_LIGHT
  sizeof(motion_ev_csi),        // SERVICEID_DMC_TARGET
  sizeof(0),                    // SERVICEID_DMC_CALIBRATION
  sizeof(0),                    // SERVICEID_FLOOR_CALIBRATION
  sizeof(0),                    // SERVICEID_PROXIMITYSENSOR_CALIBRATION
  sizeof(0)                     // SERVICEID_ODOMETRY
};


/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/**
 * @brief Macro for apps.c to set the forwarding of a service onto the CANBridge.
 * 
 * @param[in] i         Number of service on apps.c
 * 
 * @param[in] SERVICEID Name of the service as part of SERVICEID_{NAME} and CAN_{NAME}_REQUESTEVENT
 */ 
#define INIT_SERVICE(i, SERVICEID) \
  can_service[i].id = SERVICEID_##SERVICEID; \
  can_service[i].notification.mask = CAN_##SERVICEID##_REQUESTEVENT; \
  _can.services[i].service = &can_service[i]; \
  _can.services[i].payload_size = service_payload_sizes[SERVICEID_##SERVICEID]; \
  _can.services[i].next = (i < NUM_SERVICES - 1) ? &_can.services[i + 1] : NULL;

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* URT_CFG_RPC_ENABLED == true */
#endif /* AMIRODEFAULT_SERVICES_H */

/** @} */
