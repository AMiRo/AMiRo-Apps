/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    AMiRoDefault_topics.h
 * @brief   AMiRoDefault µRT topic identifiers.
 *
 * @addtogroup configs_amirodefault_modules
 * @{
 */

#ifndef AMIRODEFAULT_TOPICS_H
#define AMIRODEFAULT_TOPICS_H

#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/TouchSensordata.h"
#include "../../messagetypes/DWD_accodata.h"
#include "../../messagetypes/DWD_gyrodata.h"
#include "../../messagetypes/DWD_gravitydata.h"
#include "../../messagetypes/DWD_magnodata.h"
#include "../../messagetypes/positiondata.h"
#include "../../messagetypes/timedata.h"
#include "../../messagetypes/chargedata.h"
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

#define NUM_TOPICIDS 13

enum {
  TOPICID_FLOOR_PROXIMITY,                      /**< Floor proximity topic. */ /*payload=DWD_floordata.h:floor_proximitydata_t*/
  TOPICID_FLOOR_AMBIENT,                        /**< Floor ambient light topic. */ /*payload=DWD_floordata.h:floor_ambientdata_t*/
  TOPICID_DME,                                  /**< DifferentialMotioEstimator motion topic. */ /*payload=motiondata.h:motion_ev_csi*/
  TOPICID_PROXIMITYSENSOR_AMBIENT,              /**< ProximitySensor ambient light topic. */ /*payload=ProximitySensordata.h:proximitysensor_ambientdata_t*/
  TOPICID_PROXIMITYSENSOR_PROXIMITY,            /**< ProximitySensor proximity topic. */ /*payload=ProximitySensordata.h:proximitysensor_proximitydata_t*/
  TOPICID_TOUCH,                                /**< Touch topic. */ /*payload=TouchSensordata.h:touch_data_t*/
  TOPICID_ACCELEROMETER,                        /**< Accelerometer motion topic. */ /*payload=DWD_accodata.h:acco_converted_data_t*/
  TOPICID_GYROSCOPE,                            /**< Gyroscope motion topic. */ /*payload=DWD_gyrodata.h:gyro_converted_data_t*/
  TOPICID_COMPASS,                              /**< Gravity direction topic. */ /*payload=DWD_gravitydata.h:gravity_data_t*/
  TOPICID_MAGNETOMETER,                         /**< TBD */ /*payload=DWD_magnodata.h:magno_converted_data_t*/
  TOPICID_ODOMETRY,                             /**< Odometry values topic. */ /*payload=positiondata.h:position_cv_ccsi*/
  TOPICID_TIMESEND,                             /**< Timesend values topic. */ /*payload=timedata.h:time_data_t*/
  TOPICID_BATTERY                               /**< Battery values topic. */ /*payload=chargedata.h:battery_data_t*/
};

/**
 * @brief List of all payload sizes indexed by topic id.
 */
static size_t topic_payload_sizes[NUM_TOPICIDS] = {
  sizeof(floor_proximitydata_t),                   // TOPICID_FLOOR_PROXIMITY
  sizeof(floor_ambientdata_t),                     // TOPICID_FLOOR_AMBIENT
  sizeof(motion_ev_csi),                           // TOPICID_DME
  sizeof(proximitysensor_ambientdata_t),           // TOPICID_PROXIMITYSENSOR_AMBIENT
  sizeof(proximitysensor_proximitydata_t),         // TOPICID_PROXIMITYSENSOR_PROXIMITY
  sizeof(touch_data_t),                            // TOPICID_TOUCH
  sizeof(acco_converted_data_t),                   // TOPICID_ACCELEROMETER
  sizeof(gyro_converted_data_t),                   // TOPICID_GYROSCOPE
  sizeof(gravity_data_t),                          // TOPICID_COMPASS
  sizeof(magno_converted_data_t),                  // TOPICID_MAGNETOMETER
  sizeof(position_cv_ccsi),                        // TOPICID_ODOMETRY
  sizeof(time_data_t),                             // TOPICID_TIMESEND
  sizeof(battery_data_t)                           // TOPICID_BATTERY
};

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/**
 * @brief Macro for apps.c to set the forwarding of a topic onto the CANBridge.
 * 
 * @param[in] i Number of topic on apps.c
 * 
 * @param[in] TOPICID Name of the service as part of TOPICID_{NAME} and CAN_{NAME}_EVENT
 * 
 * @param[in] TRANSMITFACTOR Number of omitted transmits. Higher number means less transmits.
 * 
 */ 
#define INIT_SUBSCRIBER(i, TOPICID, TRANSMITFACTOR) \
  subscriber_masks[i] = CAN_##TOPICID##_EVENT; \
  subscriber_payload_sizes[i] = topic_payload_sizes[TOPICID_##TOPICID]; \
  subscriber_topic_ids[i] = TOPICID_##TOPICID; \
  _can.subscriber[i].subscriber = &can_subscriber[i]; \
  _can.subscriber[i].topic_id = subscriber_topic_ids[i]; \
  _can.subscriber[i].payload_size = subscriber_payload_sizes[i]; \
  _can.subscriber[i].transmit_factor = TRANSMITFACTOR; \
  _can.subscriber[i].mask = &subscriber_masks[i]; \
  _can.subscriber[i].next = (i < NUM_SUBSCRIBER - 1) ? &_can.subscriber[i + 1] : NULL;

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* URT_CFG_PUBSUB_ENABLED == true */
#endif /* AMIRODEFAULT_TOPICS_H */

/** @} */
