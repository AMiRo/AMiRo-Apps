/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    dwd_eeprom_partition.h
 * @brief   DiWheelDrive board's eeprom usage overview.
 *
 * @addtogroup configs_amirodefault_modules
 * @{
 */

#ifndef DWD_EEPROM_PARTITION_H
#define DWD_EEPROM_PARTITION_H

///from AMiRo-Apps/os/AMiRo-OS/periphery-lld/AMiRo-LLD/drivers/AT24C01B/v1/alld_AT24C01B.h
///**
// * @brief Memory size of the EEPROM in bytes
// */
//#define AT24C01B_LLD_SIZE_BYTES             128
//
//
///**
// * @brief Size of a page in bytes
// */
//#define AT24C01B_LLD_PAGE_SIZE_BYTES        8
//
//
///**
// * @brief  Time in microseconds a write operation takes to complete (I2C will not respond).
// * @note   The system should wait slightly longer.
// */
//#define AT24C01B_LLD_WRITECYCLETIME_US      5000

#ifndef EEPROM_TIMEOUT_DEFAULT
#define EEPROM_TIMEOUT_DEFAULT                MICROSECONDS_PER_SECOND
#endif

#ifndef EEPROM_PAGESIZE
#define EEPROM_PAGESIZE                       8
#endif

enum DWD_EEPROM_Partition {
    DWD_ID_ADDR = 0x00,
    DWD_ID_SIZE = 1,
};


#endif /* DWD_EEPROM_PARTITION_H */