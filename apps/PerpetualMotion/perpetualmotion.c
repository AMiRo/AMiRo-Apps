/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "perpetualmotion.h"

#include <amiroos.h>
#include <stdlib.h>
#include <math.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief   Event masks to listen on
 */
#define TIMER0_EVENT              (urtCoreGetEventMask() << 1)
#define TIMER1_EVENT              (urtCoreGetEventMask() << 2) //Blinker
#define FLOORPROX_TOPICEVENT      (urtCoreGetEventMask() << 3)
#define RINGPROX_TOPICEVENT       (urtCoreGetEventMask() << 4)
#define ODOM_TOPICEVENT           (urtCoreGetEventMask() << 5)
#define BATTERY_TOPICEVENT        (urtCoreGetEventMask() << 6)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of perpetualmotion nodes.
 */
static const char _app_name[] = "perpetualmotion";

float radian = 0.0;
int stable_count;

bool pm_returning_to_charge = false;

void pm_control_driving(perpetualmotion_node_t* self, pm_driving_modes_t mode, float speed) {
  self->driving_mode = mode;
  self->driving_speed = speed; //ToDo: Randomize speed if driving
  pm_signalEdgeFollowingService(self, self->driving_mode, self->driving_speed);
}

void pm_docking_movement(perpetualmotion_node_t* self, float velocity){
  int targetL = 4000; //vorher 5000;
  int targetR = 22000;

  int FL = self->proximity_floor.data.sensors.left_front;
  int FR = self->proximity_floor.data.sensors.right_front;

  float error = (targetL + targetR - FL - FR) / 100;
  float K_p = 0.25;
  float correctionSpeed = K_p * error;
  pm_signalMotorService(self, velocity, correctionSpeed);
}

bool pm_checkPower() {
    uint8_t charging_active;
    ltc4412_lld_get_stat(&moduleLldPowerPathController, &charging_active);
    return (bool)charging_active;
}

void pm_getData(perpetualmotion_node_t* self, urt_osEventMask_t event) {
  urtDebugAssert(self != NULL);
  urt_status_t status;

  if (event & RINGPROX_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->proximity_ring.nrt,
                                         &self->proximity_ring.data,
                                         &self->proximity_ring.data_size,
                                         NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    
    event &= ~RINGPROX_TOPICEVENT;
  } 
  if (event & FLOORPROX_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->proximity_floor.nrt,
                                         &self->proximity_floor.data,
                                         &self->proximity_floor.data_size,
                                         NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~FLOORPROX_TOPICEVENT;
  }                                                     
  if (event & ODOM_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->odom_data.nrt,
                                        &self->odom_data.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~ODOM_TOPICEVENT;
  }
  if (event & BATTERY_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->battery_data.nrt,
                                         &self->battery_data.data,
                                         NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);

    if (!pm_returning_to_charge && self->battery_data.data.percentage < PM_MIN_CHARGELEVEL) {
        self->light_data.data.color[22] = ORANGE;
        pm_signalLightService(self);
        //pm_returning_to_charge = true;
        pm_returning_to_charge = false; //ToDo: Remove this line

    }
    event &= ~BATTERY_TOPICEVENT;
  }
  return;
}

void pm_signalLightService(perpetualmotion_node_t* self) {
  urtDebugAssert(self != NULL);

  urt_status_t status = urtNrtRequestTryAcquire(&self->light_data.request);
  if (status == URT_STATUS_OK) {
    urtNrtRequestSubmit(&self->light_data.request,
                        urtCoreGetService(self->light_data.serviceid),
                        sizeof(self->light_data.data),
                        0);
  } else {
    urtPrintf("Could not acquire urt while signaling light service!");
  }
  return;
}

void pm_signalMotorService(perpetualmotion_node_t* self, float translation, float rotation) {
  urtDebugAssert(self != NULL);
  self->motor_data.data.translation.axes[0] = translation;
  self->motor_data.data.rotation.vector[2] = rotation;

  urt_status_t status = urtNrtRequestTryAcquire(&self->motor_data.request);
  if (status == URT_STATUS_OK) {
    urtNrtRequestSubmit(&self->motor_data.request,
                        self->motor_data.service,
                        sizeof(self->motor_data.data),
                        0);
  } else {
    urtPrintf("Could not acquire urt while signaling motor service!");
  }
}
void pm_signalEdgeFollowingService(perpetualmotion_node_t* self, pm_driving_modes_t mode, float velocity) {
  urtDebugAssert(self != NULL);

  self->edge_following.data.instruction = (ef_instruction_t)mode;
  self->edge_following.data.velocity = velocity;

  urt_status_t status = urtNrtRequestTryAcquire(&self->edge_following.request);
  if (status == URT_STATUS_OK) {
    //urtPrintf("Acquired the EdgeFollowing Service!\n");
    urtNrtRequestSubmit(&self->edge_following.request,
                        self->edge_following.service,
                        sizeof(self->edge_following.data),
                        0);
  } else {
    urtPrintf("Failed to acquire the EdgeFollowing Service! Reason :%i\n", status);
  }
  return;
}

static void _perpetualmotion_triggercb(virtual_timer_t* timer, void* self)
{
  // local constants
  perpetualmotion_node_t* const _self = (perpetualmotion_node_t*)self;
  // signal node thread to read proximity data
  if (timer == &_self->timer.vt) {
    urtEventSignal(_self->node.thread, TIMER0_EVENT);
  }
  // signal node thread to read environment data
  if (timer == &_self->blinker_timer.vt) {
    urtEventSignal(_self->node.thread, TIMER1_EVENT);
  }
  return;
}

urt_osEventMask_t _perpetualmotion_Setup(urt_node_t* node, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // local constants
  perpetualmotion_node_t* const _self = (perpetualmotion_node_t*)self;

  // set thread name
  chRegSetThreadName(_app_name);

  // Start timer
  aosTimerPeriodicInterval(&_self->timer, (1.0f / 100.0f) * MICROSECONDS_PER_SECOND + 0.5f, _perpetualmotion_triggercb, _self);
  aosTimerPeriodicInterval(&_self->blinker_timer, 0.4f * MICROSECONDS_PER_SECOND + 0.5f, _perpetualmotion_triggercb, _self);

  // subscribe to the topics
  urt_topic_t* const proximity_ring_topic = urtCoreGetTopic(_self->proximity_ring.topicid);
  urt_topic_t* const proximity_floor_topic = urtCoreGetTopic(_self->proximity_floor.topicid);
  urt_topic_t* const odom_data_topic = urtCoreGetTopic(_self->odom_data.topicid);
  urt_topic_t* const battery_data_topic = urtCoreGetTopic(_self->battery_data.topicid);

  urtDebugAssert(proximity_ring_topic != NULL);
  urtDebugAssert(odom_data_topic != NULL);
  urtDebugAssert(battery_data_topic != NULL);

  urtNrtSubscriberSubscribe(&_self->proximity_ring.nrt, proximity_ring_topic, RINGPROX_TOPICEVENT);
  urtNrtSubscriberSubscribe(&_self->proximity_floor.nrt, proximity_floor_topic, FLOORPROX_TOPICEVENT);
  urtNrtSubscriberSubscribe(&_self->odom_data.nrt, odom_data_topic, ODOM_TOPICEVENT);
  urtNrtSubscriberSubscribe(&_self->battery_data.nrt, battery_data_topic, BATTERY_TOPICEVENT);

  return TIMER0_EVENT | FLOORPROX_TOPICEVENT | RINGPROX_TOPICEVENT | ODOM_TOPICEVENT | TIMER1_EVENT | BATTERY_TOPICEVENT;
}

void PerpetualMotion(perpetualmotion_node_t* self){
  urtDebugAssert(self != NULL);
  switch (self->state) {
    case PM_IDLE:
      if (self->previous_state != PM_IDLE) {
        self->light_data.data.color[0] = GREEN;
        self->light_data.data.color[23] = GREEN;
        pm_signalLightService(self);
        pm_control_driving(self, EF_DRIVING_STOP, 0.0);
      }
      else {
        if (self->proximity_ring.data.sensors.ese > 60000 && self->proximity_ring.data.sensors.wsw > 60000) {
          pm_control_driving(self, EF_FOLLOW_RIGHT, 0.04);
          self->state = PM_DRIVING;
        }
      }
      self->previous_state = PM_IDLE;
      break;
    case PM_DRIVING:
      if (self->previous_state != PM_DRIVING) {
        self->light_data.data.color[0] = TURQUOISE;
        self->light_data.data.color[23] = TURQUOISE;
        pm_signalLightService(self);
      }
      if (self->proximity_ring.data.sensors.nne > 5000 && self->proximity_ring.data.sensors.nnw > 5000) {
        pm_control_driving(self, EF_DRIVING_STOP, 0.0);
        self->state = PM_ROTATE;
      }
      if (self->proximity_ring.data.sensors.ene > 60000 && self->proximity_ring.data.sensors.wnw > 60000) {
          self->state = PM_IDLE;
      }
      self->previous_state = PM_DRIVING;
      break;
    case PM_ROTATE:
      if (self->previous_state != PM_ROTATE) {
        self->light_data.data.color[0] = RED;
        self->light_data.data.color[23] = RED;
        pm_signalLightService(self);

        radian = self->odom_data.data.orientation.angle + 3.14;
        radian = (radian > 3.14) ? (radian - 2 * 3.14) : ((radian < -3.14) ? (radian + 2 * 3.14) : radian);
        pm_signalMotorService(self, 0.0, 1);
      }
      if (fabs(self->odom_data.data.orientation.angle - radian) < 0.05) {
        if (pm_returning_to_charge && self->driving_mode == 1) {
          self->state = PM_DOCKING;
          break;
        }
        if (self->driving_mode == 1) {
          self->driving_mode == 2;
        } else {
          self->driving_mode == 1;
        }
        pm_control_driving(self, self->driving_mode, 0.07);
        self->state = PM_DRIVING;

      }
      self->previous_state = PM_ROTATE;
      break;
    case PM_DOCKING:
      if (self->previous_state != PM_DOCKING) {
        self->light_data.data.color[0] = PURPLE;
        self->light_data.data.color[23] = PURPLE;
        pm_signalLightService(self);

        pm_control_driving(self, EF_DRIVING_STOP, 0.0);
      }

      if (pm_align(self)) {
        pm_docking_movement(self, -0.025);
        if (self->proximity_ring.data.sensors.sse > 64000 && self->proximity_ring.data.sensors.ssw > 64000) {
          ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_ACTIVE);
          self->state = PM_CHARGING;
        }
      }
      if (self->proximity_ring.data.sensors.ene > 40000 && self->proximity_ring.data.sensors.wnw > 40000) {
          self->state = PM_DRIVING;
      }
      self->previous_state = PM_DOCKING;
      break;
    case PM_CHARGING:
      if (self->previous_state != PM_CHARGING) {
        pm_control_driving(self, EF_BLOCKED, 0.0);
        self->light_data.data.color[12] = ORANGE;
        self->light_data.data.color[11] = ORANGE;
        pm_signalLightService(self);
        stable_count = 0;
      }
      if (pm_checkPower()) {
        if (self->battery_data.data.percentage >= PM_MAX_CHARGELEVEL) {
          ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
          if (!pm_checkPower()) {
            pm_control_driving(self, EF_DRIVING_LEFT, 0.07);
            self->state = PM_DRIVING;
          }
        }
      }
      if (stable_count >= 100) {
        ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
        pm_docking_movement(self, 0.1);
        if (self->proximity_ring.data.sensors.sse < 1000 && self->proximity_ring.data.sensors.ssw > 1000) {
          self->state = PM_DOCKING;
        }
      }
      self->previous_state = PM_CHARGING;
      break;
    case PM_ERROR:
      if (self->previous_state != PM_ERROR) {
        for (int i = 0; i<24; i++) {
          self->light_data.data.color[i] = RED;
        }
        pm_signalLightService(self);
        pm_control_driving(self, EF_DRIVING_STOP, 0.0);
      }
      if (self->proximity_ring.data.sensors.ene > 40000 && self->proximity_ring.data.sensors.wnw > 40000) {
        self->state = PM_IDLE;
      }
      self->previous_state = PM_ERROR;

  }
}


bool pm_align(perpetualmotion_node_t* self) {
  int sse = self->proximity_ring.data.sensors.sse;
  int ssw = self->proximity_ring.data.sensors.ssw;
  int ese = self->proximity_ring.data.sensors.ese;
  int wsw = self->proximity_ring.data.sensors.wsw;

  sse = fmin(sse, 40000);
  ssw = fmin(ssw, 40000);
  
  float error = (sse - ssw);
  float rotation = error * 0.0001;

  if (rotation < 0.155) {
    ++stable_count; 
  }

  if (stable_count >= 100) {
    return true;
  } else {
    pm_signalMotorService(self, 0.0, rotation);
    return false;
  }  
}

urt_osEventMask_t _perpetualmotion_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // local constants
  perpetualmotion_node_t* const _self = (perpetualmotion_node_t*)self;
  pm_getData(_self, event);

  if (event & TIMER0_EVENT) {
    switch (_self->mode) {
      case PM_OFF:
        urtThreadSSleep(1); 
        break;
      case PM_ON:
        PerpetualMotion(self);
        break;
      default:
        urtDebugAssert(0);
        break;
    }
    event &= ~TIMER0_EVENT;
  }
  return TIMER0_EVENT | FLOORPROX_TOPICEVENT | RINGPROX_TOPICEVENT | ODOM_TOPICEVENT | TIMER1_EVENT | BATTERY_TOPICEVENT;
}
void _perpetualmotion_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;
  (void)reason;

  // local constants
  perpetualmotion_node_t* const _self = (perpetualmotion_node_t*)self;

  urtNrtSubscriberUnsubscribe(&_self->proximity_ring.nrt);
  urtNrtSubscriberUnsubscribe(&_self->proximity_floor.nrt);
  urtNrtSubscriberUnsubscribe(&_self->odom_data.nrt);
  urtNrtSubscriberUnsubscribe(&_self->battery_data.nrt);
  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initialize a perpetualmotion node.
 *
 * @param[in] perpetualmotion           Pointer to the perpetualmotion node to initialize.
 * @param[in] dmc_target_serviceid      Service identifier for the DifferentialMotorControl target motion service.
 * @param[in] odom_reset_serviceid      Service identifier for the Odometry position reset service.
 * @param[in] led_light_serviceid       Service identifier for the LED lights.
 * @param[in] proximity_topicid         Topic ID to subscribe proximity data.
 * @param[in] environment_topicid       Topic ID to subscribe environment data.
 * @param[in] odom_topicid              Topic ID to subscribe odometry data.
 * @param[in] battery_topicid           Topic ID to subscribe battery level data.
 * @param[in] prio                      Priority of the perpetualmotion node thread.
 */
void perpetualMotionInit(perpetualmotion_node_t* self,
                 urt_serviceid_t edgefollowing_serviceid,
                 urt_serviceid_t dmc_target_serviceid,
                 urt_serviceid_t odom_reset_serviceid,
                 urt_serviceid_t led_light_serviceid,
                 urt_topicid_t proximity_topicid,
                 urt_topicid_t environment_topicid,
                 urt_topicid_t odom_topicid,
                 urt_topicid_t battery_topicid,
                 urt_osThreadPrio_t prio)
{
  self->mode = PM_ON;
  self->state = PM_IDLE;
  self->previous_state = PM_DOCKING;

  self->driving_mode = EF_DRIVING_STOP;
  self->driving_speed = 0.0;
  
  self->proximity_ring.topicid = environment_topicid;
  self->proximity_floor.topicid = proximity_topicid;
  self->odom_data.topicid = odom_topicid;
  self->battery_data.topicid = battery_topicid;

  self->proximity_ring.data_size = sizeof(((perpetualmotion_node_t*)self)->proximity_ring.data);
  self->proximity_floor.data_size = sizeof(((perpetualmotion_node_t*)self)->proximity_floor.data);
  self->odom_data.data_size = sizeof(((perpetualmotion_node_t*)self)->odom_data.data);
  self->battery_data.data_size = sizeof(((perpetualmotion_node_t*)self)->battery_data.data);

  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _perpetualmotion_Setup, self,
              _perpetualmotion_Loop, self,
              _perpetualmotion_Shutdown, self);

  // initialize service requests
  urtNrtRequestInit(&self->edge_following.request, &self->edge_following.data);
  urtNrtRequestInit(&self->motor_data.request, &self->motor_data.data);
  urtNrtRequestInit(&self->odom_data.request, &self->odom_data.data);
  urtNrtRequestInit(&self->light_data.request, &self->light_data.data);
  self->edge_following.service = urtCoreGetService(edgefollowing_serviceid);
  self->motor_data.service = urtCoreGetService(dmc_target_serviceid);
  self->odom_data.service = urtCoreGetService(odom_reset_serviceid);
  self->light_data.serviceid = led_light_serviceid;

  // initialize trigger timer
  aosTimerInit(&self->timer);
  aosTimerInit(&self->blinker_timer);

  // initialize subscriber
  urtNrtSubscriberInit(&((perpetualmotion_node_t*)self)->proximity_ring.nrt);
  urtNrtSubscriberInit(&((perpetualmotion_node_t*)self)->proximity_floor.nrt);
  urtNrtSubscriberInit(&((perpetualmotion_node_t*)self)->odom_data.nrt);
  urtNrtSubscriberInit(&((perpetualmotion_node_t*)self)->battery_data.nrt);
  return;
}