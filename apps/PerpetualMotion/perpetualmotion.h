/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    perpetualmotion.h
 * @brief   Example app testing motor functions.
 *
 * @addtogroup perpetualMotion
 * @{
 */

#ifndef PERPETUALMOTION_H
#define PERPETUALMOTION_H

#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/positiondata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/chargedata.h"
#include "../../messagetypes/perpetualmotion_data.h"
#include "../../messagetypes/EdgeFollowingData.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(perpetualMotion_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of floor threads.
 */
#define perpetualMotion_STACKSIZE 1024
#endif /* !defined(perpetualMotion_STACKSIZE) */

#define MAP_SIZE 14
#define PM_MAX_CHARGELEVEL 99
#define PM_MIN_CHARGELEVEL 98



#define RAND_TRESH 20000
#define MAX_CORRECTED_SPEED 1000000*100

#define REQ_CHARGELEVEL 5
#define FORWARD_K_P 0.9f
#define REVERSE_K_P 1.5f
#define FORWARD_SPEED 0.07f
#define REVERSE_SPEED -0.07f
#define HALT_SPEED 0.0f
#define WHITE_STOP 100

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/


/**
* @brief  Data of the light
*/
typedef struct pm_light {
  light_led_data_t data;
  urt_nrtrequest_t request;
  urt_serviceid_t serviceid;
}pm_light_t;

/**
* @brief   Trigger related data of the environment sensors.
*/
typedef struct pm_proxring{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 proximitysensor_proximitydata_t data;
 size_t data_size;
}pm_proxring_t;

typedef struct pm_proxfloor{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 floor_proximitydata_t data;
 size_t data_size;
}pm_proxfloor_t;

/**
* @brief   Trigger related data of the odometry data.
*/
typedef struct pm_odom{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 position_cv_ccsi data;
 size_t data_size;
 urt_nrtrequest_t request;
 urt_service_t* service;
}pm_odom_t;

/**
* @brief   Trigger related data of the battery data.
*/
typedef struct pm_battery{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 battery_data_t data;
 size_t data_size;
}pm_battery_t;


typedef struct pm_edge_following {
  urt_nrtrequest_t request;
  edgefollowing_service_data_t data;
  urt_service_t* service;
}pm_edge_following_t;

typedef struct pm_motor {
  urt_nrtrequest_t request;
  motion_ev_csi data;
  urt_service_t* service;
}pm_motor_t;
/**
 * @brief   perpetualMotion node.
 * @struct  perpetualmotion_node
 */
typedef struct perpetualmotion_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, perpetualMotion_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  pm_mode_t mode;
  pm_state_t state;
  pm_state_t previous_state;

  pm_driving_modes_t driving_mode;
  float driving_speed;

  pm_proxring_t proximity_ring;
  pm_proxfloor_t proximity_floor;

  /**
   * @brief   debug 
   */
  pm_debug_t debug;
  pm_debug_id_t id;



  /**
   * @brief  Backup of the latest state. 
   *         Used for temporary state changes to return to an intended state.
  */
  pm_state_t backup_state;

  /**
   * @brief   Publisher to publish the state data
   */
  urt_publisher_t state_publisher;

  /**
   * @brief   Publisher to publish the backup state data
   */
  urt_publisher_t nextstate_publisher;

  /**
   * @brief   Publisher to publish the strategy
   */
  urt_publisher_t strategy_publisher;

  /**
   * @brief   Publisher to publish the debug msg
   */
  urt_publisher_t debug_publisher;

    /**
   * @brief   Publisher to publish the debug msg
   */
  urt_publisher_t id_debug_publisher;

  /**
   * @brief   Publisher to publish the map position
   */
  urt_publisher_t position_publisher;

  /**
   * @brief   Publisher to publish the docking attempts
   */
  urt_publisher_t docking_publisher;

  /**
   * @brief   Publisher to publish the power check results
   */
  urt_publisher_t power_publisher;

  /**
   * @brief   Contains data of the LED lights and service.
   */
  pm_light_t light_data;

  /**
   * @brief  Contains odometry subscriber and data.
   */
  pm_odom_t odom_data;

  /**
   * @brief  Contains odometry subscriber and data.
   */
  pm_battery_t battery_data;

  pm_edge_following_t edge_following;

  pm_motor_t motor_data;

  /**
   * @brief   Timer to trigger loop for motor request.
   */
  aos_timer_t timer;

  /**
   * @brief   Timer to trigger a blinking light.
   */
  aos_timer_t blinker_timer;

  /**
   * @brief   Time of latest measurement.
   */
  urt_osTime_t latestTime;

} perpetualmotion_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void perpetualMotionInit(perpetualmotion_node_t* perpetualmotion,
                 urt_serviceid_t edge_following_serviceid,
                 urt_serviceid_t dmc_target_serviceid,
                 urt_serviceid_t odom_reset_serviceid,
                 urt_serviceid_t led_light_serviceid,
                 urt_topicid_t proximity_topicid,
                 urt_topicid_t environment_topicid,
                 urt_topicid_t odom_topicid,
                 urt_topicid_t battery_topicid,
                 urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

void pm_signalEdgeFollowingService(perpetualmotion_node_t* self, pm_driving_modes_t mode, float velocity);
void pm_signalLightService(perpetualmotion_node_t* self);
void pm_signalMotorService(perpetualmotion_node_t* self, float translation, float rotation);

// charging related functions
bool pm_align(perpetualmotion_node_t* self);
void pm_getError(void* perpetualmotion);


// robot state related functions
void status_initiateRotation(void* perpetualmotion);
void status_concludeRotation(void* perpetualmotion);
void status_beginStandby(void* perpetualmotion);
void status_beginDriving(void* perpetualmotion);
void status_beginDocking(void* perpetualmotion);
void status_beginCharging(void* perpetualmotion);
void status_beginDeparture(void* perpetualmotion);

// functions related to process of data received outside of the app
void pm_getData(perpetualmotion_node_t* self, urt_osEventMask_t event);
bool pm_checkForJunctionEnd(void* perpetualmotion);
bool pm_checkForWall(void* perpetualmotion);
bool pm_checkForSignal(void* perpetualmotion);
bool pm_checkForPower(void* perpetualmotion);
void pm_updateDistance(void* perpetualmotion);
bool pm_updateLights(void* perpetualmotion);

// functions related to services outside the app

void pm_signalOdometryResetService(perpetualmotion_node_t* perpetualmotion);

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* PERPETUALMOTION_H */

/** @} */
