/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aasconnectivity.c
 */

#include <aasconnectivity.h>
#include <amiroos.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define INFORMATION_SERVICEEVENT            (urtCoreGetEventMask() << 1)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of aasconnectivity nodes.
 */
static const char _app_name[] = "AASConnectivity";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

void _saveBoardID(void* self, uint8_t board_id) {
  // local variables
  aasconnectivity_node_t* const _self = (aasconnectivity_node_t*)self;

  _self->board_id = board_id;
  urtDebugAssert(DWD_ID_SIZE == sizeof(_self->board_id));
  
  apalExitStatus_t status = at24c01b_lld_write_byte(_self->drivers.eeprom, DWD_ID_ADDR, _self->board_id, EEPROM_TIMEOUT_DEFAULT);
  /*while (at24c01b_lld_poll_ack(_self->drivers.eeprom, EEPROM_TIMEOUT_DEFAULT)) {
    continue;
  }*/
  if (status != APAL_STATUS_OK) {
    urtPrintf("APAL Write Status:%u\n", status);
  }
}

void _read_BoardID(void* self) {
  // local constants
  aasconnectivity_node_t* const _self = (aasconnectivity_node_t*)self;

  uint8_t* data = (uint8_t*) &_self->board_id;
  urtDebugAssert(DWD_ID_SIZE == sizeof(_self->board_id));

  apalExitStatus_t status = at24c01b_lld_read(_self->drivers.eeprom, DWD_ID_ADDR, data, DWD_ID_SIZE, EEPROM_TIMEOUT_DEFAULT);

  if (_self->board_id == 255) {
    CH_CFG_SYSTEM_EMERGENCY_PRINT("Board ID is not defined!\n");
  }
  //ToDo: check if the read was successful -> method below always print status = 8
  /*if (status != APAL_STATUS_OK) {
    urtPrintf("APAL Read STatus:%u\n", status);
  }*/
}


/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */

urt_osEventMask_t _aasconnectivity_Setup(urt_node_t* node, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_app_name);

  _read_BoardID(self);

  return INFORMATION_SERVICEEVENT;
}

/**
 * @brief   Loop callback function for aasconnectivity nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] aasconnectivity    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _aasconnectivity_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // local constants
  aasconnectivity_node_t* const _self = (aasconnectivity_node_t*)self;
  
  while (event & INFORMATION_SERVICEEVENT) {
    aasconnectivity_service_data_t requestdata;
    urt_service_dispatched_t dispatched;
    bool fireandforget = false;
    if (urtServiceDispatch(&_self->info_service, &dispatched, &requestdata, NULL, &fireandforget)) {
      urtDebugAssert(urtRequestGetPayloadSize(urtServiceDispatchedGetRequest(&dispatched)) == sizeof(aasconnectivity_service_data_t));
      switch (requestdata.request.info) {
        case Version_Identifier:
          {
          // acquire request
          if (urtServiceTryAcquireRequest(&_self->info_service, &dispatched) == URT_STATUS_OK) {
            // set return data
            //urtPrintf("Acquire Request was succesful\n");
            aasconnectivity_service_data_t* const dispatchedpayload = (aasconnectivity_service_data_t*)urtRequestGetPayload(urtServiceDispatchedGetRequest(&dispatched));
            __itoa((uint8_t)COUNTER, dispatchedpayload->response.data, 10); //snprintf should be used to avoid buffer overflowgit 
            // respond
            urtServiceRespond(&dispatched, sizeof(aasconnectivity_service_data_t));
          }
          break;
        }
        case AMiRo_Identifier:
          {
          // acquire request
          _read_BoardID(_self);
          if (urtServiceTryAcquireRequest(&_self->info_service, &dispatched) == URT_STATUS_OK) {
            // set return data
            //urtPrintf("Acquire Request was succesful\n");
            aasconnectivity_service_data_t* const dispatchedpayload = (aasconnectivity_service_data_t*)urtRequestGetPayload(urtServiceDispatchedGetRequest(&dispatched));
            __itoa(_self->board_id, dispatchedpayload->response.data, 10);
            // respond
            urtServiceRespond(&dispatched, sizeof(aasconnectivity_service_data_t));
          }
          break;
        }
        case AAS_HOST:
          {
          // acquire request
          if (urtServiceAcquireRequest(&_self->info_service, &dispatched) == URT_STATUS_OK) {
            // set return data
            aasconnectivity_service_data_t* const dispatchedpayload = (aasconnectivity_service_data_t*)urtRequestGetPayload(urtServiceDispatchedGetRequest(&dispatched));
            //dispatchedpayload->data = AAS_HOSTNAME;
            strncpy(dispatchedpayload->response.data, AAS_HOSTNAME, sizeof(dispatchedpayload->response.data) - 1);
            dispatchedpayload->response.data[sizeof(dispatchedpayload->response.data) - 1] = '\0';  // Null-terminieren, um Überläufe zu vermeiden
            // respond
            urtServiceRespond(&dispatched, sizeof(aasconnectivity_service_data_t));
          }
          break;
        }
        case AAS_PORT:
          {
          // acquire request
          if (urtServiceAcquireRequest(&_self->info_service, &dispatched) == URT_STATUS_OK) {
            // set return data
            aasconnectivity_service_data_t* const dispatchedpayload = (aasconnectivity_service_data_t*)urtRequestGetPayload(urtServiceDispatchedGetRequest(&dispatched));
            //dispatchedpayload->data = AAS_HOSTNAME;
            strncpy(dispatchedpayload->response.data, AAS_PORTNUMBER, sizeof(dispatchedpayload->response.data) - 1);
            dispatchedpayload->response.data[sizeof(dispatchedpayload->response.data) - 1] = '\0';  // Null-terminieren, um Überläufe zu vermeiden
            // respond
            urtServiceRespond(&dispatched, sizeof(aasconnectivity_service_data_t));
          }
          break;
        }
        case AAS_ID:
          {
          // acquire request
          if (urtServiceAcquireRequest(&_self->info_service, &dispatched) == URT_STATUS_OK) {
            // set return data
            aasconnectivity_service_data_t* const dispatchedpayload = (aasconnectivity_service_data_t*)urtRequestGetPayload(urtServiceDispatchedGetRequest(&dispatched));
            //dispatchedpayload->data = AAS_HOSTNAME;
            strncpy(dispatchedpayload->response.data, AAS_IDENTIFIER, sizeof(dispatchedpayload->response.data) - 1);
            dispatchedpayload->response.data[sizeof(dispatchedpayload->response.data) - 1] = '\0';  // Null-terminieren, um Überläufe zu vermeiden
            // respond
            urtServiceRespond(&dispatched, sizeof(aasconnectivity_service_data_t));
          }
          break;
        }
        default:
          urtServiceRespond(&dispatched, 0);
          break;
      }
    }
    event &= ~INFORMATION_SERVICEEVENT;
  }

  return INFORMATION_SERVICEEVENT;
}

/**
 * @brief   Shutdown callback function for aasconnectivity nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] reason  Reason of the termination.
 * @param[in] aasconnectivity  Pointer to the aasconnectivity structure
 *                           Must not be NULL.
 */
void _aasconnectivity_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);

  (void)node;
  (void)reason;

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/
/**
 * @addtogroup apps_aasconnectivity
 * @{
 */

int aasconnectivity_ShellCallback_readwrite_BoardId(BaseSequentialStream* stream, int argc, const char* argv[], aasconnectivity_node_t* self) {
  urtDebugAssert(self != NULL);
  (void)argc;
  (void)argv;
  if (argc == 2 && strcmp(argv[1], "read") == 0) {
    _read_BoardID(self);
    chprintf(stream, "Board ID: %u\n", self->board_id);
  } else if (argc == 3 && strcmp(argv[1], "write") == 0) {
    uint8_t board_id = atoi(argv[2]);
    _saveBoardID(self, board_id);
    chprintf(stream, "Board ID: %u\n", self->board_id);
  } else {
    chprintf(stream, "To read BoardID: %s read\n", argv[0]);
    chprintf(stream, "To write BoardID: %s write [BOARD_ID]\n", argv[0]);
    chprintf(stream, "\n");
    chprintf(stream, "  BOARD_ID:\n");
    chprintf(stream, "    Board ID to be set. Must be one of the following:\n");
    chprintf(stream, "    0..255\n");
    return AOS_INVALIDARGUMENTS;
  }
  return AOS_OK;
}


void aasconnectivityInit(aasconnectivity_node_t* self,
                    AT24C01BDriver* eeprom_driver,
                    urt_serviceid_t info_serviceid,
                    urt_osThreadPrio_t prio)
{
  urtDebugAssert(self != NULL);
  //set drivers
  self->drivers.eeprom = eeprom_driver;
  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _aasconnectivity_Setup, self,
              _aasconnectivity_Loop, self,
              _aasconnectivity_Shutdown, self);

  // initialize service requests
/*  if (URT_STATUS_OK != urtServiceInit(&self->info_service, info_serviceid, self->node.thread, INFORMATION_SERVICEEVENT)) {
    urtPrintf("Error");
  };*/
  urtDebugAssert(urtServiceInit(&self->info_service, info_serviceid, self->node.thread, INFORMATION_SERVICEEVENT) == URT_STATUS_OK);
  urtDebugAssert(urtCoreGetService(info_serviceid)!=NULL);
  return;
}

/** @} */
