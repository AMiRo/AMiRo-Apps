/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aasconnectivity.h
 * @brief   A application that let the AMiRo avoid obstacles.
 *
 * @defgroup apps_aasconnectivity
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_aasconnectivity
 * @{
 */

#ifndef AASCONNECTIVITY_H
#define AASCONNECTIVITY_H

#include <apps.h>
#include <urt.h>
#include "../../messagetypes/AASConnectivityData.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(AASCONNECTIVITY_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of aasconnectivity threads.
 */
#define AASCONNECTIVITY_STACKSIZE             512
#endif /* !defined(AASCONNECTIVITY_STACKSIZE) */

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/***********************
*******************************************************/
#define AAS_HOSTNAME    "http://pyke"
#define AAS_PORTNUMBER  "14081"
#define AAS_IDENTIFIER  "ubi:ks:fms:citec"


/**
 * @brief   AASConnectivity node.
 * @struct  AASConnectivity_node
 */
typedef struct aasconnectivity_node {

  URT_THREAD_MEMORY(thread, AASCONNECTIVITY_STACKSIZE);
  urt_node_t node;

  struct {
    AT24C01BDriver* eeprom;
  } drivers;

  //information_data_t amiro_information;
  urt_service_t info_service;
  aasconnectivity_service_data_t amiro_information_data;

  uint8_t board_id;
  
} aasconnectivity_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void aasconnectivityInit(aasconnectivity_node_t* self,
                 AT24C01BDriver* eeprom_driver,
                 urt_serviceid_t info_serviceid,
                 urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */


/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

void aasconnectivity(aasconnectivity_node_t* self);
int aasconnectivity_ShellCallback_readwrite_BoardId(BaseSequentialStream* stream, int argc, const char* argv[], aasconnectivity_node_t* self);
/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* AASCONNECTIVITY_H */

/** @} */
