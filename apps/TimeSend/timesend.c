/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    timesend.c
 */

#include <timesend.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of timesend nodes.
 */
static const char _timesend_name[] = "TimeSend";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _tsend_triggercb(virtual_timer_t* timer, void* params)
{
  (void)timer;
  (void)params;

  // broadcast ring event
  chEvtBroadcastFlagsI(&((timesend_node_t*)params)->trigger.source, (urt_osEventFlags_t)0);

  return;
}

//Signal the led light service to update current colors
void tsend_signalLightService(void* tsend) {
  urtDebugAssert(tsend != NULL);

  //((timesend_node_t*)tsend)->light_data.data.color[0] = GREEN;
  //((timesend_node_t*)tsend)->light_data.data.color[7] = GREEN;
  //((timesend_node_t*)tsend)->light_data.data.color[14] = GREEN;
  //((timesend_node_t*)tsend)->light_data.data.color[21] = GREEN;
  ((timesend_node_t*)tsend)->light_data.data.color[((timesend_node_t*)tsend)->light_counter] = OFF;
  ((timesend_node_t*)tsend)->light_counter += 1;
  if (((timesend_node_t*)tsend)->light_counter >= 24)
    ((timesend_node_t*)tsend)->light_counter = 0;
  ((timesend_node_t*)tsend)->light_data.data.color[((timesend_node_t*)tsend)->light_counter] = GREEN;

  urt_status_t status = urtNrtRequestTryAcquire(&((timesend_node_t*)tsend)->light_data.request);
  if (status == URT_STATUS_OK) {
    urtNrtRequestSubmit(&((timesend_node_t*)tsend)->light_data.request,
                        urtCoreGetService(((timesend_node_t*)tsend)->light_data.serviceid),
                        sizeof(((timesend_node_t*)tsend)->light_data.data),
                        0);
    //urtPrintf("Success!\n");
  } else {
    urtPrintf("Could not acquire urt while signaling light service! Reason:%i\n", status);
  }
  return;
}

/**
 * @brief   Setup callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _tsend_Setup(urt_node_t* node, void* tsend)
{
  urtDebugAssert(tsend != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_timesend_name);

  // register trigger event
  urtEventRegister(&((timesend_node_t*)tsend)->trigger.source, &((timesend_node_t*)tsend)->trigger.listener, TS_TRIGGEREVENT, 0);

  // activate the timer
  aosTimerPeriodicInterval(&((timesend_node_t*)tsend)->trigger.timer, (1.0f / ((timesend_node_t*)tsend)->frequency) * MICROSECONDS_PER_SECOND, _tsend_triggercb, tsend);

  return TS_TRIGGEREVENT;
}

/**
 * @brief   Loop callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _tsend_Loop(urt_node_t* node, urt_osEventMask_t event, void* tsend)
{
  urtDebugAssert(tsend != NULL);
  (void)node;

  switch(event) {
    case TS_TRIGGEREVENT:
    {
      urtTimeNow(&((timesend_node_t*)tsend)->time);
      ((timesend_node_t*)tsend)->data.time = ((timesend_node_t*)tsend)->time.time;
      // publish ambient data
      urtPublisherPublish(&((timesend_node_t*)tsend)->publisher,
                          &((timesend_node_t*)tsend)->data,
                          sizeof(((timesend_node_t*)tsend)->data),
                          &((timesend_node_t*)tsend)->time,
                          URT_PUBLISHER_PUBLISH_ENFORCING);
      tsend_signalLightService(tsend);
      //urtPrintf("[%llu] send time data %llu\n", ((timesend_node_t*)tsend)->time.time, ((timesend_node_t*)tsend)->data);
      //urtThreadSSleep(1);
      break;
    }
    default: break;
  }

  return TS_TRIGGEREVENT;
}

/**
 * @brief @brief   TimeSend shutdown callback.
 *
 * @param[in] node      Execution node of the tsend.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] tsend  Pointer to the TimeSend instance.
 */
void _tsend_Shutdown(urt_node_t* node, urt_status_t reason, void* tsend)
{
  urtDebugAssert(tsend != NULL);
  (void)node;
  (void)reason;

  // unregister trigger event
  urtEventUnregister(&((timesend_node_t*)tsend)->trigger.source, &((timesend_node_t*)tsend)->trigger.listener);

  return;
}


/**
 * @brief   TimeSend (tsend) initialization function.
 *
 * @param[in] tsend               tsend object to initialize.
 * @param[in] topicID_timeSend       Topic ID where timesend publishes.
 * @param[in] frequency              Publish frequency in Hz.
 * @param[in] prio                   Priority of the execution thread.
 */
void tsendInit(timesend_node_t* tsend,
                   urt_topicid_t topicID_timeSend,
                   urt_serviceid_t led_light_serviceid,
                   float frequency, 
                   urt_osThreadPrio_t prio)
{
  urtDebugAssert(tsend != NULL);

  // set/initialize event data
  urtEventSourceInit(&tsend->trigger.source);
  urtEventListenerInit(&tsend->trigger.listener);
  // initialize the timer
  aosTimerInit(&tsend->trigger.timer);
  // set the topicid
  tsend->topicid = topicID_timeSend;
#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize the publisher for the tsend data
  urtPublisherInit(&tsend->publisher, urtCoreGetTopic(topicID_timeSend));
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  // initialize the light service
  urtNrtRequestInit(&tsend->light_data.request, &tsend->light_data.data);
  tsend->light_data.serviceid = led_light_serviceid;
  tsend->light_counter = 0;
  // set the frequency
  tsend->frequency = frequency;

  // initialize the node
  urtNodeInit(&tsend->node, (urt_osThread_t*)tsend->thread, sizeof(tsend->thread), prio,
              _tsend_Setup, tsend,
              _tsend_Loop, tsend,
              _tsend_Shutdown, tsend);


  return;
}

/** @} */
