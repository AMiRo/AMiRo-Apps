/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    timesend.h
 *
 * @defgroup apps_intro LineFollowing
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_intro
 * @{
 */

#ifndef TIMESEND_H
#define TIMESEND_H

#include <urt.h>
#include "../../messagetypes/timedata.h"
#include "../../messagetypes/LightRing_leddata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(TIMESEND_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of timesend threads.
 */
#define TIMESEND_STACKSIZE             256
#endif /* !defined(TIMESEND_STACKSIZE) */   

/**
 * @brief   Event mask to set on a trigger event.
 */
#define TS_TRIGGEREVENT                (urt_osEventMask_t)(1<< 1)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
* @brief  Data of the light
*/
typedef struct ts_light {
  urt_nrtrequest_t request;
  light_led_data_t data;
  urt_serviceid_t serviceid;
} ts_light_t;

/**
 * @brief   timesend node.
 * @struct  timesend_node
 */
typedef struct timesend_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, TIMESEND_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief Id of the topic where the timesend data are published on.
   */
  urt_topicid_t topicid;

  /**
   * @brief   Time stamp of the published data.
   */
  urt_osTime_t time;

  /**
   * @brief   The data itself is also the time.
   */
  time_data_t data;

  /**
   * @brief   Data for the light service/request.
   */
  ts_light_t light_data;

  uint8_t light_counter;

  /**
   * @brief node frequency in Hz.
   */
  float frequency;

  /**
   * @brief   Trigger related data.
   */
  struct {
    /**
     * @brief   Pointer to the trigger event source.
     */
    urt_osEventSource_t source;

    /**
     * @brief   Event listener for trigger events.
     */
    urt_osEventListener_t listener;

    /**
     * @brief   Timer to trigger accelerometer data.
     */
    aos_timer_t timer;
  } trigger;

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  /**
   * @brief   Publisher to publish the accelerometer data.
   */
  urt_publisher_t publisher;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

} timesend_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void tsendInit(timesend_node_t* tsend,
                   urt_topicid_t topicID_timeSend,
                   urt_serviceid_t led_light_serviceid,
                   float frequency, 
                   urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* TIMESEND_H */

/** @} */
