/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    bivitalcontroller.h
 * @brief   A application that let the AMiRo avoid obstacles.
 *
 * @defgroup apps_bivitalcontroller
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_bivitalcontroller
 * @{
 */

#ifndef BIVITALCONTROLLER_H
#define BIVITALCONTROLLER_H

#include <urt.h>
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/bivitaldata.h"
#include "../../messagetypes/LightRing_leddata.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(BIVITALCONTROLLER_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of bivitalcontroller threads.
 */
#define BIVITALCONTROLLER_STACKSIZE             512
#endif /* !defined(BIVITALCONTROLLER_STACKSIZE) */

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/***********************
*******************************************************/

/**
* @brief  Data of the motor
*/
typedef struct bvc_motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t* service;
}bvc_motor_t;

/**
* @brief  Data of the light
*/
typedef struct bvc_light {
  urt_nrtrequest_t request;
  light_led_data_t data;
  urt_serviceid_t serviceid;
}bvc_light_t;

/**
* @brief   Data of the bivital .
*/
typedef struct bvc_bivitaldata{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 bivital_data_t data;
 size_t data_size;
}bvc_bivitaldata_t;

/**
 * @brief   BiVitalController node.
 * @struct  BiVitalController_node
 */
typedef struct bivitalcontroller_node {

  URT_THREAD_MEMORY(thread, BIVITALCONTROLLER_STACKSIZE);
  urt_node_t node;
  bvc_motor_t motor_data;
  bvc_light_t light_data;
  bvc_bivitaldata_t bivital_data;
  
  aos_timer_t timer;
} bivitalcontroller_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void bivitalcontrollerInit(bivitalcontroller_node_t* bivitalcontroller,
                 urt_serviceid_t dmc_target_serviceid,
                 urt_serviceid_t led_light_serviceid,
                 urt_topicid_t environment_topicid,
                 urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */


/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

void bivitalController(bivitalcontroller_node_t* bivitalcontroller);

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* BIVITALCONTROLLER_H */

/** @} */
