/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    bivitalcontroller.c
 */

#include <bivitalcontroller.h>
#include <amiroos.h>
#include <stdlib.h>
#include <math.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define BIVITAL_EVENT            (urtCoreGetEventMask() << 1)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of bivitalcontroller nodes.
 */
static const char _bivitalcontroller_name[] = "BiVitalController";

/**
 * BiVitalController Setup
 */


/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

//Signal the led light service to update current colors
void bvc_signalLightService(void* bivitalcontroller) {
  urtDebugAssert(bivitalcontroller != NULL);

  if (urtNrtRequestTryAcquire(&((bivitalcontroller_node_t*)bivitalcontroller)->light_data.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&((bivitalcontroller_node_t*)bivitalcontroller)->light_data.request,
                        urtCoreGetService(((bivitalcontroller_node_t*)bivitalcontroller)->light_data.serviceid),
                        sizeof(((bivitalcontroller_node_t*)bivitalcontroller)->light_data.data),
                        0);
  } else {
    urtPrintf("Could not acquire urt while signaling light service!");
  }
  return;
}

//Signal the service with the new motor data
void bvc_signalMotorService(bivitalcontroller_node_t* bivitalcontroller, float trans, float rot) {
  urtDebugAssert(bivitalcontroller != NULL);

  bivitalcontroller->motor_data.data.translation.axes[0] = trans;
  bivitalcontroller->motor_data.data.rotation.vector[2] = rot;

  urt_status_t status = urtNrtRequestTryAcquire(&bivitalcontroller->motor_data.request);
  if (status == URT_STATUS_OK) {
    urtNrtRequestSubmit(&bivitalcontroller->motor_data.request,
                        bivitalcontroller->motor_data.service,
                        sizeof(bivitalcontroller->motor_data.data),
                        0);
  } else {
    urtPrintf("Could not acquire urt while signaling motor service! Reason:%i\n", status);
  }
  return;
}

void bvc_control(bivitalcontroller_node_t* bivitalcontroller, urt_osEventMask_t event) {
  urtDebugAssert(bivitalcontroller != NULL);

  //local variables
  urt_status_t status;

  if (event & BIVITAL_EVENT) {
    // fetch NRT of the touch sensor data
    do {
      status = urtNrtSubscriberFetchNext(&bivitalcontroller->bivital_data.nrt,
                                         &bivitalcontroller->bivital_data.data,
                                         &bivitalcontroller->bivital_data.data_size,
                                         NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~BIVITAL_EVENT;

    urtPrintf("BiVitalEvent: x: %d, y: %d, z: %d, button: %d", bivitalcontroller->bivital_data.data.x_axes, bivitalcontroller->bivital_data.data.y_axes, bivitalcontroller->bivital_data.data.z_axes, bivitalcontroller->bivital_data.data.buttons);
    //TODO: Motor steering
  }
  return;
}

urt_osEventMask_t _bivitalcontroller_Setup(urt_node_t* node, void* bivitalcontroller)
{
  urtDebugAssert(bivitalcontroller != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_bivitalcontroller_name);

#if (URT_CFG_PUBSUB_ENABLED == true)
  //Subscribe to the environment sensor topic
  urt_topic_t* const bivital_topic = urtCoreGetTopic(((bivitalcontroller_node_t*)bivitalcontroller)->bivital_data.topicid);
  urtDebugAssert(bivital_topic != NULL);
  urtNrtSubscriberSubscribe(&((bivitalcontroller_node_t*)bivitalcontroller)->bivital_data.nrt, bivital_topic, BIVITAL_EVENT);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  return BIVITAL_EVENT;
}

/**
 * @brief   Loop callback function for bivitalcontroller nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] bivitalcontroller    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _bivitalcontroller_Loop(urt_node_t* node, urt_osEventMask_t event, void* bivitalcontroller)
{
  urtDebugAssert(bivitalcontroller != NULL);
  (void)node;

  // local constants
  bivitalcontroller_node_t* const bvc_node = (bivitalcontroller_node_t*)bivitalcontroller;
  
  if (event & (BIVITAL_EVENT)) {
    // get the proximity and ambient data of the ring and floor sensors
    bvc_control(bvc_node, event);
  }
  return BIVITAL_EVENT;
}

/**
 * @brief   Shutdown callback function for bivitalcontroller nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] reason  Reason of the termination.
 * @param[in] bivitalcontroller  Pointer to the bivitalcontroller structure
 *                           Must not be NULL.
 */
void _bivitalcontroller_Shutdown(urt_node_t* node, urt_status_t reason, void* bivitalcontroller)
{
  urtDebugAssert(bivitalcontroller != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&((bivitalcontroller_node_t*)bivitalcontroller)->bivital_data.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/
/**
 * @addtogroup apps_bivitalcontroller
 * @{
 */

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */


void bivitalcontrollerInit(bivitalcontroller_node_t* bivitalcontroller,
                    urt_serviceid_t dmc_target_serviceid,
                    urt_serviceid_t led_light_serviceid,
                    urt_topicid_t bivital_topicid,
                    urt_osThreadPrio_t prio)
{
  urtDebugAssert(bivitalcontroller != NULL);

  bivitalcontroller->bivital_data.topicid = bivital_topicid;
  bivitalcontroller->bivital_data.data_size = sizeof(((bivitalcontroller_node_t*)bivitalcontroller)->bivital_data.data);

  // initialize the node
  urtNodeInit(&bivitalcontroller->node, (urt_osThread_t*)bivitalcontroller->thread, sizeof(bivitalcontroller->thread), prio,
              _bivitalcontroller_Setup, bivitalcontroller,
              _bivitalcontroller_Loop, bivitalcontroller,
              _bivitalcontroller_Shutdown, bivitalcontroller);

  // initialize service requests
#if (URT_CFG_RPC_ENABLED == true)
  urtNrtRequestInit(&bivitalcontroller->motor_data.request, &bivitalcontroller->motor_data.data);
  urtNrtRequestInit(&bivitalcontroller->light_data.request, &bivitalcontroller->light_data.data);
  bivitalcontroller->motor_data.service = urtCoreGetService(dmc_target_serviceid);
  bivitalcontroller->light_data.serviceid = led_light_serviceid;
#endif /* (URT_CFG_RPC_ENABLED == true) */

  // initialize subscriber
#if (URT_CFG_PUBSUB_ENABLED == true)
  urtNrtSubscriberInit(&bivitalcontroller->bivital_data.nrt);
#endif /* URT_CFG_PUBSUB_ENABLED == true */
  return;
}

/** @} */
