/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    breakoutboardmanager.h
 *
 * @defgroup breakoutboardmanager breakoutboardmanager
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_intro
 * @{
 */

#ifndef BREAKOUTBOARDMANAGER_H
#define BREAKOUTBOARDMANAGER_H

#include <urt.h>
#include "../../messagetypes/breakoutboardmanager.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(BREAKOUTBOARDMANAGER_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of BREAKOUTBOARDMANAGER threads.
 */
#define BREAKOUTBOARDMANAGER_STACKSIZE             256
#endif /* !defined(BREAKOUTBOARDMANAGER_STACKSIZE) */   
/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   breakoutboardmanager_node node.
 * @struct  breakoutboardmanager_node
 */
typedef struct breakoutboardmanager_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, BREAKOUTBOARDMANAGER_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;
  breakoutboard_control_t breakoutboardcontrol_request;
  urt_service_t breakoutboardcontrol_service;

} breakoutboardmanager_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
void breakoutboardmanagerInit(breakoutboardmanager_node_t* self,
                      urt_serviceid_t serviceid_breakoutboardcontrol,
                      urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* BREAKOUTBOARDMANAGER_H */

/** @} */
