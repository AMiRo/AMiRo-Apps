/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    breakoutboardmanager.c
 */

#include <breakoutboardmanager.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/
/**
 * @brief   Event mask to set on a trigger event.
 */
#define BREAKOUTBOARDCONTROL_SERVICEEVENT  (urtCoreGetEventMask()<<1)
/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of breakoutboardmanager nodes.
 */
static const char _app_name[] = "BreakoutboardManager";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Setup callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _breakoutboardmanager_Setup(urt_node_t* node, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  breakoutboardmanager_node_t* const _self = self;
  chRegSetThreadName(_app_name);
  return BREAKOUTBOARDCONTROL_SERVICEEVENT;
}

/**
 * @brief   Loop callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _breakoutboardmanager_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  breakoutboardmanager_node_t* const _self = self;

  if (event & BREAKOUTBOARDCONTROL_SERVICEEVENT)
  {
    // local variables
    urt_service_dispatched_t dispatched;
    // get the request
    while(urtServiceDispatch(&_self->breakoutboardcontrol_service,
                          &dispatched,
                          &_self->breakoutboardcontrol_request,
                          NULL,
                          NULL)) {
      if (_self->breakoutboardcontrol_request.action == V33) {
        mic9404x_lld_set(&moduleLldPowerSwitchV33, _self->breakoutboardcontrol_request.param);
      } else if (_self->breakoutboardcontrol_request.action == V50) {
        mic9404x_lld_set(&moduleLldPowerSwitchV50, _self->breakoutboardcontrol_request.param);
      }
      if (dispatched.request != NULL) {
        if (urtServiceTryAcquireRequest(&_self->breakoutboardcontrol_service, &dispatched) == URT_STATUS_OK) {
          urtServiceRespond(&dispatched, 0);
        }
      }
    }
    event &= ~BREAKOUTBOARDCONTROL_SERVICEEVENT;
  }
  return BREAKOUTBOARDCONTROL_SERVICEEVENT;
}

/**
 * @brief @brief   TimeSend shutdown callback.
 *
 * @param[in] node      Execution node of the tsend.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] tsend  Pointer to the TimeSend instance.
 */
void _breakoutboardmanager_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;
  (void)reason;
}


/**
 * @brief   TimeSend (tsend) initialization function.
 *
 * @param[in] tsend               tsend object to initialize.
 * @param[in] topicID_timeSend       Topic ID where timesend publishes.
 * @param[in] frequency              Publish frequency in Hz.
 * @param[in] prio                   Priority of the execution thread.
 */
void breakoutboardmanagerInit(breakoutboardmanager_node_t* self,
                      urt_serviceid_t serviceid_breakoutboardcontrol,
                      urt_osThreadPrio_t prio)
{
  urtDebugAssert(self != NULL);
  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _breakoutboardmanager_Setup, self,
              _breakoutboardmanager_Loop, self,
              _breakoutboardmanager_Shutdown, self);

  urtServiceInit(&self->breakoutboardcontrol_service, serviceid_breakoutboardcontrol, self->node.thread, BREAKOUTBOARDCONTROL_SERVICEEVENT);
}

/** @} */
