About & License
===============

CANBridgeLite is an application for the Autonomous
Mini Robot (AMiRo) [1]. It is tightly coupled to the internal CANBus of the AMiRo and acts as the main connection between the three modules DiWheelDrive, PowerManagement and LightRing. For this it sends Data over the CANBus and receives Data from the CANBus. This is a more lightweight, faster version of the basic CANBridge application.

Copyright (C) 2018..2022 Thomas Schöpping et al. (a complete list of all authors
is given below)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU (Lesser) General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

This research/work was supported by the Cluster of Excellence Cognitive
Interaction Technology 'CITEC' (EXC 277) at Bielefeld University, which is
funded by the German Research Foundation (DFG).


Authors
-------

- Lauritz Keysberg

Switch to CANBridgeLite 
-------

In order to switch between CANBridge and CANBridgeLite, the apps.c files of all communicating boards need to be adjusted to `#include <canbridge.h>` or `#include <canbridgelite.h>`. Further, the Makefiles of the boards need to build `CANBridge` or `CANBridgeLite` respectively. All other datatypes and functions are equivalent in usage.

Differences to CANBridge
-------

The CANBridgeLite does not use meta frames and instead sends compact information in the EID of the CAN message. This reduces the amount of required frames by up to 100 percent.

Caution for message size: Total message size is limited to a maximum of 4 payloads per message (32 byte). Because the counter currently occupies just two bits, it cannot provide the order for longer messages. A bigger message size could be achieved by adapting the bit order on the EID according to preferences. E.g. by reducing bits for id or type.

Caution for time stamps: The reduction comes at the cost of less accuracy in the time stamps. This inaccuracy is usually in the range of a few miliseconds (one milisecond plus the base inaccuracy between the two boards). The CANBridgeLite assumes the message delay plus the difference between local clocks does not exceed 2 seconds. If the messages take very long, or the clocks are severely out of sync, the received time stamps will be read with a wrong time stamp. (Either way, even in the normal CANBridge, a base inaccuracy between boards will make the relative evaluation of time stamps difficult)

In detail, the time stamps are processed as follows. Instead of sending the exact time stamps in microseconds in 58 bits of a meta frame, 12 bits in the EID are used to send the time stamp with a base unit of 1024 microseconds. After about 4 seconds (1024 * 2<sup>12</sup> = 4194304 microseconds), a new "cycle" repeats. The receiving CANBridgeLite compares the received time to the local time and adds it to the begin of the current "cycle" of itself. Time differences between incoming time and a 4 second range of the local time (the least-significant 22 bit) are interpreted as edge cases for the next and previous "cycle", which are added or subtracted accordingly.

CANFrames (Light)
-------
Each CANFrame (Light) has a 29 bit identifier and a data array of eight bytes. Beginning with least significant bit, which in most representations is the rightermost bit, the identifier is composed as follows:
```c
Bits 0 -1  =  2 (  4): counter (of message packet)
Bits 2 -13 = 12 ( 4k): time stamp 
Bit    14  =  1 (  2): fire and forget (for service/request data) 
Bits 15-21 =  7 (128): topic/service id 
Bits 22-25 =  4 ( 16): type (topic data = 0, Request = 1(nrt)/3(frt)/5(srt)/7(hrt), Request Answer = 8)
Bit  26-28 =  3 (  8): board_id (DiWheelDrive = 0, PowerManagement = 1, LightRing = 2, Unknown = 3)
Bits 29-31: Unused by EID definition
```

Usage infos (equivalent to CANBridge)
=======
Send data over the CANBus
-------

After adding the CANBridge to your configuration you can send PubSub data over it by defining a `canBridge_subscriber_list_t` object which is the second argument of the `canBridgeInit` function. The structure of the `canBridge_subscriber_list_t` is as following: 
```c
typedef struct canBridge_subscriber_list {
  urt_nrtsubscriber_t* subscriber;
  urt_osEventMask_t* mask;
  size_t payload_size;
  urt_topicid_t topic_id;
  struct canBridge_subscriber_list* next;
}canBridge_subscriber_list_t;
```

When you want to send Service data over the internal CANBus you need to define a `canBridge_service_list_t` object which is the fifth argument of the `canBridgeInit` function. The structure of the `canBridge_service_list_t` is as following:

```c
typedef struct canBridge_service_list {
  urt_service_t* service;
  size_t payload_size;
  struct canBridge_service_list* next;
}canBridge_service_list_t;
```


Receive data from the CANBus
-------
When you want to receive PubSub data from the internal CANBus you need to define a `urt_topicid_t` array containing the topic ids where you want to receive data from. This is the third argument of the `canBridgeInit` function. Also you need to set the fourth argument which is the number of topic ids in your `urt_topicid_t` array. <br>
Receiving data from Service/Request data is similar: You need to define a `urt_serviceid_t` array containing the service ids where you want to receive data from. This is the sixth argument of the `canBridgeInit` function. Also you need to set the eighth argument which is the number of service ids in your `urt_serviceid_t` array.
