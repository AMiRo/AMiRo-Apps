/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    intro_wallfollowing.c
 */

#include <intro_wallfollowing.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define NUM_LEDS            24
#define NUM_RING_SENSORS    8

/**
 * @brief   Event masks.
 */
#define PROXRINGEVENT                 (urt_osEventMask_t)(1<< 1)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of intro_wallfollowing nodes.
 */
static const char _intro_wallfollowing_name[] = "Intro_WL";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

//Signal the light service with the new colors for each LED
void intro_wl_signalLightService(intro_wallfollowing_node_t* intro_wl) {
  urtDebugAssert(intro_wl != NULL);

  //Try to acquire the light service
  if (urtNrtRequestTryAcquire(&intro_wl->light.request) == URT_STATUS_OK) {
    //Signal the light service
    urtNrtRequestSubmit(&intro_wl->light.request,
                        intro_wl->light.service,
                        sizeof(intro_wl->light.data),
                        0);
  }

  return;
}

void intro_wl_getData(intro_wallfollowing_node_t* intro_wl, urt_osEventMask_t event) {
  urtDebugAssert(intro_wl != NULL);

  //local variables
  urt_status_t status;

  if (event & PROXRINGEVENT) {
    // fetch NRT of the ring proximity data
    do {
      status = urtNrtSubscriberFetchNext(&intro_wl->ring.nrt,
                                         &intro_wl->ring.data,
                                         NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);

    event &= ~PROXRINGEVENT;
  }

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @addtogroup apps_intro
 * @{
 */

/**
 * @brief   Setup callback function for intro_wallfollowing nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] intro_wl  Pointer to the intro_wl structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _intro_wl_Setup(urt_node_t* node, void* intro_wl)
{
  urtDebugAssert(intro_wl != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_intro_wallfollowing_name);

  // local constants
  intro_wallfollowing_node_t* const wl = (intro_wallfollowing_node_t*)intro_wl;

  #if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the ring proximity topic
  urt_topic_t* const ring_prox_topic = urtCoreGetTopic(wl->ring.topicid);
  urtDebugAssert(ring_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&wl->ring.nrt, ring_prox_topic, PROXRINGEVENT);
#endif /* URT_CFG_PUBSUB_ENABLED == true */


  return PROXRINGEVENT;
}

/**
 * @brief   Loop callback function for intro_wallfollowing nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] intro_wl  Pointer to the intro_wl structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _intro_wl_Loop(urt_node_t* node, urt_osEventMask_t event, void* intro_wl)
{
  urtDebugAssert(intro_wl != NULL);
  (void)node;

  // local constants
  intro_wallfollowing_node_t* const wl = (intro_wallfollowing_node_t*)intro_wl;

  // get the proximity data of the ring sensors
  intro_wl_getData(wl, event);

  switch (wl->state) {
  case WL_IDLE: {
      // TODO: Define a condition to change into the WALLFOLLOWING state
      urtThreadSSleep(2);
    }
    break;
  case WALLFOLLOWING: {
    /* TODO
     * WallFollowing:
     *    Write code that let the AMiRo follow the wall
     *    Define the motor data struct in the intro_wallfollowing.h
     *    Initialize the Motor Request
     *    Write a function to signal the motor service
     */
    break;
  }
  default: break;
  }

  return PROXRINGEVENT;
}

/**
 * @brief @brief   Intro_WallFollowing shutdown callback.
 *
 * @param[in] node      Execution node of the intro_wl.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] intro_wl  Pointer to the Intro_WallFollowing instance.
 */
void _intro_wl_Shutdown(urt_node_t* node, urt_status_t reason, void* intro_wl)
{
  urtDebugAssert(intro_wl != NULL);
  (void)node;
  (void)reason;

  // local constants
  intro_wallfollowing_node_t* const wl = (intro_wallfollowing_node_t*)intro_wl;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&wl->ring.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  return;
}


/**
 * @brief   Intro_WallFollowing (intro_wl) initialization function.
 *
 * @param[in] intro_wl               intro_wl object to initialize.
 * @param[in] prio                   Priority of the execution thread.
 * @param[in] serviceID_Light        Service ID of the light service.
 * @param[in] serviceID_Motor        Service ID of the motor service.
 * @param[in] topicID_ringProx       Topic ID to get ring proximity data.
 */
void intro_wl_Init(intro_wallfollowing_node_t* intro_wl,
                   urt_osThreadPrio_t prio,
                   urt_serviceid_t serviceID_Light,
                   urt_serviceid_t serviceID_Motor,
                   urt_topicid_t topicID_ringProx)
{
  urtDebugAssert(intro_wl != NULL);

  intro_wl->state = WL_IDLE;
  intro_wl->ring.topicid = topicID_ringProx;
  // TODO: set the motor service
  intro_wl->light.service = urtCoreGetService(serviceID_Light);

  urtNrtSubscriberInit(&intro_wl->ring.nrt);
  urtNrtRequestInit(&intro_wl->light.request, &intro_wl->light.data);
  // TODO: Initialize the motor request

  // initialize the node
  urtNodeInit(&intro_wl->node, (urt_osThread_t*)intro_wl->thread, sizeof(intro_wl->thread), prio,
              _intro_wl_Setup, intro_wl,
              _intro_wl_Loop, intro_wl,
              _intro_wl_Shutdown, intro_wl);

  return;
}

/** @} */
