/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    fpgamanager.h
 *
 * @defgroup fpgamanager fpgamanager
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_intro
 * @{
 */

#ifndef FPGAMANAGER_H
#define FPGAMANAGER_H

#include <urt.h>
#include "../../messagetypes/fpgamanager.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(FPGAMANAGER_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of FPGAMANAGER threads.
 */
#define FPGAMANAGER_STACKSIZE             256
#endif /* !defined(FPGAMANAGER_STACKSIZE) */   
/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   fpgamanager_node node.
 * @struct  fpgamanager_node
 */
typedef struct fpgamanager_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, FPGAMANAGER_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief Id of the topic where the FPGAMANAGER data are published on.
   */
  urt_topicid_t serial_data_topicid;
  urt_topicid_t fpga_status_change_topicid;
  urt_topicid_t fpga_control_topicid;
  urt_nrtsubscriber_t serial_data_nrt;
  urt_nrtsubscriber_t fpga_control_nrt;
  fpga_status_t last_fpga_status;
  float frequency;
  aos_timer_t timer;

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  /**
   * @brief   Publisher to publish the accelerometer data.
   */
  urt_publisher_t fpga_status_publisher;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

} fpgamanager_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
void fpgaManagerInit(fpgamanager_node_t* self,
                     urt_topicid_t topicID_fpgaSerialData,
					 urt_topicid_t topicID_fpgaStatusChange,
					 urt_topicid_t topicID_fpgaControl,
                     float frequency, 
                     urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* FPGAMANAGER_H */

/** @} */
