/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    fpgamanager.c
 */

#include <fpgamanager.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/
ROMCONST SPIConfig moduleHalSpiTestConfig2 = {
  /* circular buffer mode             */ false,
  /* slave mode                       */ false,
  /* data callback function pointer   */ NULL,
  /* error callback function pointer  */ NULL,
  /* chip select line port       */ LINE_SYS_SPI_SS0_N,
  /* CR1                         */ SPI_CR1_BR_1 | SPI_CR1_BR_0,        // 36/16 Mbps
  /* CR2                         */ SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN,  // 0
};
/**
 * @brief   Event mask to set on a trigger event.
 */
#define TIMER0_EVENT                     (urtCoreGetEventMask()<<1)
#define SERIALDATA_TOPICEVENT            (urtCoreGetEventMask()<<2)
#define FPGACONTROL_TOPICEVENT           (urtCoreGetEventMask()<<3)
/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of fpgamanager nodes.
 */
static const char _app_name[] = "FpgaManager";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * Timer Event for periodic work, could also use interrupts but this is easier and is flooding proof
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _fpgamanager_triggercbI(virtual_timer_t* timer, void* self)
{
  (void)timer;
  fpgamanager_node_t* const _self = self;
  if (timer == &_self->timer.vt)
  {
    urtEventSignal(_self->node.thread, TIMER0_EVENT);
  }
}

/**
 * @brief   Setup callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _fpgamanager_Setup(urt_node_t* node, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  fpgamanager_node_t* const _self = self;
  chRegSetThreadName(_app_name);
  // Topics:
  urt_topic_t* const serial_data_topic = urtCoreGetTopic(_self->serial_data_topicid);
  urtDebugAssert(serial_data_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->serial_data_nrt, serial_data_topic, SERIALDATA_TOPICEVENT);
  urt_topic_t* const fpga_control_topic = urtCoreGetTopic(_self->fpga_control_topicid);
  urtDebugAssert(fpga_control_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->fpga_control_nrt, fpga_control_topic, FPGACONTROL_TOPICEVENT);
  // activate the timer
  aosTimerPeriodicInterval(&_self->timer, (1.0f / _self->frequency) * MICROSECONDS_PER_SECOND, _fpgamanager_triggercbI, _self);
  return SERIALDATA_TOPICEVENT | FPGACONTROL_TOPICEVENT | TIMER0_EVENT;
}

/**
 * @brief   Loop callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _fpgamanager_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  fpgamanager_node_t* const _self = self;
  //urtPrintf(".\n");
  if (event & SERIALDATA_TOPICEVENT) 
  {
    fpga_serial_data_t data;
    urt_status_t status;
    do 
    {
      status = urtNrtSubscriberFetchNext(&_self->serial_data_nrt,
                                         &data.data,
                                         NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    apalSPITransmit(&SPID1, data.data, FPGA_SERIAL_DATA_SIZE_IN_BYTES);
    event &= ~SERIALDATA_TOPICEVENT;
  }
  if (event & TIMER0_EVENT)
  {
    // Acquire status of fpga status pins
    fpga_status_t fpga_status = {0};
    apalControlGpioState_t gpio_status;
    urtDebugAssert(apalControlGpioGet(&moduleGpioSysDone, &gpio_status) == APAL_STATUS_OK);
    fpga_status.signals.done = gpio_status; // Assumes APAL_GPIO_ON defined as 1
    urtDebugAssert(apalControlGpioGet(&moduleGpioSysProg, &gpio_status) == APAL_STATUS_OK);
    fpga_status.signals.prog_n = gpio_status; // Assumes APAL_GPIO_ON defined as 1
    urtDebugAssert(apalControlGpioGet(&moduleGpioSysSpiDir, &gpio_status) == APAL_STATUS_OK);
    fpga_status.signals.spi_dir = gpio_status; // Assumes APAL_GPIO_ON defined as 1
    if (((fpgamanager_node_t*)self)->last_fpga_status.data != fpga_status.data) // Publish only on changes
    {
      urt_osTime_t time;
      urtTimeNow(&time);
      urtDebugAssert(
      urtPublisherPublish(&_self->fpga_status_publisher,
                          &fpga_status,
                          sizeof(fpga_status_t),
                          &time,
                          URT_PUBLISHER_PUBLISH_ENFORCING) == URT_STATUS_OK
                    );
    }
    ((fpgamanager_node_t*)self)->last_fpga_status.data = fpga_status.data;
    event &= ~TIMER0_EVENT;
  }
  if (event & FPGACONTROL_TOPICEVENT)
  {
    urt_status_t status;
    fpga_control_t data;
    urtPrintf("OK\n");
    do 
    {
      status = urtNrtSubscriberFetchNext(&_self->fpga_control_nrt, &data,
                                         NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    switch (data.action)
    {
      case FPGA_CHANGE_SPI:
        if (data.param == 1)
        {
          spiStart(&SPID1, &moduleHalSpiTestConfig2);
        } else {
          spiStop(&SPID1);
        }
        break;
      case FPGA_SET_PROGN:
        apalControlGpioSet(&moduleGpioSysProg, data.param);
        break;
      default:
        urtPrintf("Unimpl\n");
    }
    event &= ~FPGACONTROL_TOPICEVENT;
  }
  return TIMER0_EVENT | SERIALDATA_TOPICEVENT | FPGACONTROL_TOPICEVENT;
}

/**
 * @brief @brief   TimeSend shutdown callback.
 *
 * @param[in] node      Execution node of the tsend.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] tsend  Pointer to the TimeSend instance.
 */
void _fpgamanager_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;
  (void)reason;
}


/**
 * @brief   TimeSend (tsend) initialization function.
 *
 * @param[in] tsend               tsend object to initialize.
 * @param[in] topicID_timeSend       Topic ID where timesend publishes.
 * @param[in] frequency              Publish frequency in Hz.
 * @param[in] prio                   Priority of the execution thread.
 */
void fpgaManagerInit(fpgamanager_node_t* self,
                     urt_topicid_t topicID_fpgaSerialData,
                     urt_topicid_t topicID_fpgaStatusChange,
                     urt_topicid_t topicID_fpgaControl,
                     float frequency, 
                     urt_osThreadPrio_t prio)
{
  urtDebugAssert(self != NULL);
  // set the topicid
  self->serial_data_topicid = topicID_fpgaSerialData;
  self->fpga_status_change_topicid = topicID_fpgaStatusChange;
  self->fpga_control_topicid = topicID_fpgaControl;
  urtPublisherInit(&self->fpga_status_publisher, urtCoreGetTopic(topicID_fpgaStatusChange));

  // initialize the light service
  urtNrtSubscriberInit(&self->serial_data_nrt);
  urtNrtSubscriberInit(&self->fpga_control_nrt);
  self->frequency = frequency;
  // initialize the timer
  aosTimerInit(&self->timer);
  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _fpgamanager_Setup, self,
              _fpgamanager_Loop, self,
              _fpgamanager_Shutdown, self);
}

/** @} */
