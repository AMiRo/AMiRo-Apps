/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    edgefollowing.h
 *
 * @defgroup apps_edgefollowing EdgeFollowing
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_edgefollowing
 * @{
 */

#ifndef EDGEFOLLOWING_H
#define EDGEFOLLOWING_H

#include <urt.h>
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/EdgeFollowingData.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(EDGEFOLLOWING_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of edgefollowing threads.
 */
#define EDGEFOLLOWING_STACKSIZE             512
#endif /* !defined(EDGEFOLLOWING_STACKSIZE) */   


/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
* @brief   Trigger related data of the floor proximity sensors.
*/
typedef struct ef_floor_prox {
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 floor_proximitydata_t data;
} ef_floor_prox_t;


/**
 * @brief   Trigger related data of the ring proximity sensors.
 */
typedef struct ef_ring_prox {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  proximitysensor_proximitydata_t data;
} ef_ring_prox_t;

/**
* @brief  Data of the motor
*/
typedef struct ef_motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t* service;
} ef_motor_t;


/**
 * @brief   edgefollowing node.
 * @struct  edgefollowing_node
 */
typedef struct edgefollowing_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, EDGEFOLLOWING_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  ef_floor_prox_t floor_prox;
  //ef_ring_prox_t ring_prox;
  ef_motor_t motor;

  float velocity;

  urt_publisher_t event_publisher;
  urt_service_t instruction_service;

  ef_instruction_t instruction;
  ef_instruction_t previous_instruction;
  bool instruction_is_new;

} edgefollowing_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void edgefollowingInit(edgefollowing_node_t* self, 
                  urt_serviceid_t edgefollowing_serviceid,
                  urt_serviceid_t motor_serviceid,
                  urt_topicid_t edgefollowing_event_topicid,
                  urt_topicid_t floor_prox_topicid,
                  urt_topicid_t ring_prox_topicid,
                  urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* EDGEFOLLOWING_H */

/** @} */