/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    edgefollowing.c
 */

#include <edgefollowing.h>
#include <amiroos.h>
#include <math.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define NUM_LEDS            24
#define NUM_RING_SENSORS    8
#define NUM_FLOOR_SENSORS   4

#define WHITE               20000
#define BLACK               4000 
#define MAX_ERROR           (WHITE - BLACK)
#define MAX_ROTATION        3.14 //3.00 //3.14
//#define RING_PROX_BLOCKED   3000

/**
 * @brief   Event masks.
 */
#define RINGPROX_TOPICEVENT                     (urt_osEventMask_t)(1<< 1)                    
#define FLOORPROX_TOPICEVENT                    (urt_osEventMask_t)(1<< 2)
#define INSTRUCTION_SERVICEEVENT                (urt_osEventMask_t)(1<< 3)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of edgefollowing nodes.
 */
static const char _edgefollowing_name[] = "EdgeFollowing";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

int min(int a, int b) {
  return a < b ? a : b;
}

int max(int a, int b) {
  return a >= b ? a : b;
}

float interpolate_lin(float in_min, float in_max, float out_min, float out_max, float x) {
  float x_in = fmaxf(fminf(x, in_max), in_min);
  float x_p = (x_in - in_min) / (in_max - in_min);
  float x_out = x_p * (out_max - out_min) + out_min;
  return x_out;
}

//Signal the service with the new motor data
void signalMotorService(edgefollowing_node_t* ef, float translation, float rotation) {
  //urtDebugAssert(ef != NULL);

  translation = fmin(0.35, translation); // limit translation to 0.35
  ef->motor.data.translation.axes[0] = translation;
  ef->motor.data.rotation.vector[2] = rotation;

  if (urtNrtRequestTryAcquire(&ef->motor.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&ef->motor.request,
                        ef->motor.service,
                        sizeof(ef->motor.data),
                        0);
  }

  return;
}

// publish an event 
void publishEvent(edgefollowing_node_t* ef, ef_event_t event, uint8_t data1, uint8_t data2) {
  
  edgefollowing_event_data_t event_payload;
  event_payload.event = event;
  event_payload.instruction = ef->instruction;
  event_payload.data1 = data1;
  event_payload.data2 = data2;

  urt_osTime_t timestamp;
  urtTimeNow(&timestamp);

  urtPublisherPublish(&ef->event_publisher,
                      &event_payload,
                      sizeof(event_payload),
                      &timestamp,
                      URT_PUBLISHER_PUBLISH_LAZY);
}

void updateData(edgefollowing_node_t* ef, urt_osEventMask_t event) {
  //urtDebugAssert(ef != NULL);

  //local variables
  urt_status_t status;

//  if (event & RINGPROX_TOPICEVENT) {
//    // fetch NRT of the ring proximity data
//    do {
//      status = urtNrtSubscriberFetchNext(&ef->ring_prox.nrt,
//                                         &ef->ring_prox.data,
//                                         NULL, NULL, NULL);
//    } while (status != URT_STATUS_FETCH_NOMESSAGE);
//
//    event &= ~RINGPROX_TOPICEVENT;
//  }

  if (event & FLOORPROX_TOPICEVENT) {
    // fetch NRT of the floor proximity data
    do {
      status = urtNrtSubscriberFetchNext(&ef->floor_prox.nrt,
                                         &ef->floor_prox.data,
                                         NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);

    event &= ~FLOORPROX_TOPICEVENT;
  }

  return;
}

void checkInstruction(edgefollowing_node_t* ef, urt_osEventMask_t event) {
  //urtDebugAssert(ef != NULL);

  //ef->instruction_is_new = false;

  if (event & INSTRUCTION_SERVICEEVENT) {
    // local variables
    urt_service_dispatched_t dispatched;
    edgefollowing_service_data_t new_instruction;

    // get the request              
    while(urtServiceDispatch(&ef->instruction_service,
                          &dispatched,
                          &new_instruction,
                          NULL,
                          NULL)) {
    
      // signal the request
      if (dispatched.request != NULL) {
        if (urtServiceTryAcquireRequest(&ef->instruction_service, &dispatched) == URT_STATUS_OK) {
          urtServiceRespond(&dispatched, 0);
        }
      }
    }
    ef->velocity = new_instruction.velocity;
    event &= ~INSTRUCTION_SERVICEEVENT;

    urtPrintf("NEW INSTRUCTION: %d\n", new_instruction.instruction);
    urtPrintf("NEW VELOCITY: %f\n", new_instruction.velocity);

    // set the new instruction and publish the change
    if (new_instruction.instruction != ef->instruction) {
      if (new_instruction.instruction < EF_INSTRUCTION_COUNT) {
        ef->previous_instruction = ef->instruction;
        ef->instruction = new_instruction.instruction;
        ef->instruction_is_new = true;
        //urtPrintf("NEW INSTRUCTION: %d\n", ef->instruction);
        publishEvent(ef, EF_INSTRUCTION_CHANGED_EVENT, ef->previous_instruction, 0);
      }
      else {
        urtPrintf("INVALID INSTRUCTION: %d\n", new_instruction);
      }
    }

    return;
  }

  return;
}

void followEdge(edgefollowing_node_t* ef) {

  //check if blocked first
  //const int NNW = ef->ring_prox.data.data[0];
  //const int NNE = ef->ring_prox.data.data[7];
  //const int NWN = ef->ring_prox.data.data[1];
  //const int NEN = ef->ring_prox.data.data[6];
  //const int SWS = ef->ring_prox.data.data[5];
  //const int SES = ef->ring_prox.data.data[2];

  //IF ring proximity is blocked, stop

  //if (NNW > RING_PROX_BLOCKED || NNE > RING_PROX_BLOCKED || NWN > RING_PROX_BLOCKED || NEN > RING_PROX_BLOCKED) {
    //signalMotorService(ef, 0, 0);
    //publishEvent(ef, EF_BLOCKED_EVENT, 0, 0);
    //ef->previous_instruction = ef->instruction;
    //ef->instruction = EF_IDLE;
    //ef->instruction_is_new = true;
    //publishEvent(ef, EF_INSTRUCTION_CHANGED_EVENT, ef->previous_instruction, 0);
    //return;
  //}
  //else {

  // otherwise compute motor signals for edgefollowing 

  float translation = ef->velocity;
  float rotation = 0;

  const int fl = ef->floor_prox.data.sensors.left_front;
  const int fr = ef->floor_prox.data.sensors.right_front;
 
  int transition_factor = (ef->instruction == EF_FOLLOW_RIGHT && fr > fl) || (ef->instruction == EF_FOLLOW_LEFT && fl > fr) ? 1 : 0;
  
  int e = 0;
  if (ef->instruction == EF_FOLLOW_RIGHT) {
    e += max(fl - BLACK, 0) * transition_factor;
    e += min(fr - WHITE, 0);
  } else {
    e -= min(fl - WHITE, 0);
    e -= max(fr - BLACK, 0) * transition_factor;
  }

  rotation = interpolate_lin((float)-MAX_ERROR, (float)MAX_ERROR, (float)-MAX_ROTATION, (float)MAX_ROTATION, (float)e);

  signalMotorService(ef, translation, rotation);
  
};

void turnAround(edgefollowing_node_t* ef) {
  //urtDebugAssert(ef != NULL);
  if (ef->previous_instruction == EF_FOLLOW_RIGHT) {
    signalMotorService(ef, 0, -1.0);
  } else {
    signalMotorService(ef, 0, 1.0);
  }
  // check if the turn is finished
  if (ef->floor_prox.data.sensors.left_front < (BLACK+1000) && ef->floor_prox.data.sensors.right_front < (BLACK+1000)) {
    ef->previous_instruction = ef->instruction;
    ef->instruction = EF_IDLE;
    ef->instruction_is_new = true;
    publishEvent(ef, EF_INSTRUCTION_CHANGED_EVENT, ef->previous_instruction, 0);
  };
}

void followLine(edgefollowing_node_t* ef) {
  const int fl = ef->floor_prox.data.sensors.left_front;
  const int fr = ef->floor_prox.data.sensors.right_front;

  float rotation = ((fl-BLACK) - (fr-BLACK)) / -10000.0;
  urtPrintf("Rotation: %f\n", rotation);
  /*if (ef->velocity < 0.0) {
    rotation = -rotation;
  }*/
  float translation = ef->velocity;
  signalMotorService(ef, translation, rotation);
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/


/**
 * @addtogroup apps_edgefollowing
 * @{
 */

/**
 * @brief   Setup callback function for edgefollowing nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] ef  Pointer to the ef structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _ef_Setup(urt_node_t* node, void* edgefollowing)
{
  //urtDebugAssert(edgefollowing != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_edgefollowing_name);

  // local constants
  edgefollowing_node_t* const ef = (edgefollowing_node_t*)edgefollowing;

  #if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the floor proximity topic
  urt_topic_t* const floor_prox_topic = urtCoreGetTopic(ef->floor_prox.topicid);
  //urtDebugAssert(floor_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&ef->floor_prox.nrt, floor_prox_topic, FLOORPROX_TOPICEVENT);

  // subscribe to the ring proximity topic
  //urt_topic_t* const ring_prox_topic = urtCoreGetTopic(ef->ring_prox.topicid);
  //urtDebugAssert(ring_prox_topic != NULL);
  //urtNrtSubscriberSubscribe(&ef->ring_prox.nrt, ring_prox_topic, RINGPROX_TOPICEVENT);
#endif /* URT_CFG_PUBSUB_ENABLED == true */


  return RINGPROX_TOPICEVENT | FLOORPROX_TOPICEVENT | INSTRUCTION_SERVICEEVENT;
}

/**
 * @brief   Loop callback function for edgefollowing nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] ef  Pointer to the ef structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _ef_Loop(urt_node_t* node, urt_osEventMask_t event, void* edgefollowing)
{
  //urtDebugAssert(edgefollowing != NULL);
  (void)node;

  // local constants
  edgefollowing_node_t* const ef = (edgefollowing_node_t*)edgefollowing;

  // update ring and floor data
  updateData(ef, event);
  // check for a new instruction
  checkInstruction(ef, event);

  switch (ef->instruction) {
    case EF_IDLE:
      if (ef->instruction_is_new) {
        signalMotorService(ef, 0, 0);
        ef->instruction_is_new = false;
      }
      break;
    case EF_FOLLOW_RIGHT:
    case EF_FOLLOW_LEFT:
      followEdge(ef);
      break;
    case EF_TURN_AROUND:
      turnAround(ef);
      break;
    case EF_FOLLOW_STRAIGHT:
      followLine(ef);
      break;
    case EF_BLOCKED:
      if (ef->instruction_is_new) {
        signalMotorService(ef, NAN, NAN);
      }
      break;
  }

  return RINGPROX_TOPICEVENT | FLOORPROX_TOPICEVENT | INSTRUCTION_SERVICEEVENT;
}

/**
 * @brief @brief   EdgeFollowing shutdown callback.
 *
 * @param[in] node      Execution node of the ef.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] ef  Pointer to the EdgeFollowing instance.
 */
void _ef_Shutdown(urt_node_t* node, urt_status_t reason, void* edgefollowing)
{
  //urtDebugAssert(edgefollowing != NULL);
  (void)node;
  (void)reason;

  // local constants
  edgefollowing_node_t* const ef = (edgefollowing_node_t*)edgefollowing;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&ef->floor_prox.nrt);
  //urtNrtSubscriberUnsubscribe(&ef->ring_prox.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  return;
}


/**
 * @brief   EdgeFollowing (ef) initialization function.
 *
 * @param[in] ef                          ef object to initialize.
 * @param[in] edgefollowing_serviceid     Topic ID of the Edgefollowing instruction service
 * @param[in] motor_serviceid             Service ID of the motor service.
 * @param[in] edgefollowing_event_topicid Topic ID of Edgefollowing events
 * @param[in] floor_prox_topicid          Topic ID to get floor proximity data.
 * @param[in] ring_prox_topicid           Topic ID to get ring proximity data.
 * @param[in] prio                        Priority of the execution thread.
 */
void edgefollowingInit(edgefollowing_node_t* self, 
                  urt_serviceid_t edgefollowing_serviceid,
                  urt_serviceid_t motor_serviceid,
                  urt_topicid_t edgefollowing_event_topicid,
                  urt_topicid_t floor_prox_topicid,
                  urt_topicid_t ring_prox_topicid,
                  urt_osThreadPrio_t prio)
{
  urtDebugAssert(self != NULL);
  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _ef_Setup, self,
              _ef_Loop, self,
              _ef_Shutdown, self);

  // initialize service requests
  #if (URT_CFG_RPC_ENABLED == true)
    // initialize the incoming service to receive instructions
    urtServiceInit(&self->instruction_service, edgefollowing_serviceid, self->node.thread, INSTRUCTION_SERVICEEVENT);

    // initialize outgoing services to set light and motor
    self->motor.service = urtCoreGetService(motor_serviceid);
    urtNrtRequestInit(&self->motor.request, &self->motor.data);
    //urtNrtRequestInit(&ef->light.request, &ef->light.data);
  #endif /* (URT_CFG_RPC_ENABLED == true) */

  // initialize pubsub
  #if (URT_CFG_PUBSUB_ENABLED == true)
    // initialize publishing edgefollowing events
    urtPublisherInit(&((edgefollowing_node_t*)self)->event_publisher, urtCoreGetTopic(edgefollowing_event_topicid));

    // initiaize subscribing to ring and floor data
    self->floor_prox.topicid = floor_prox_topicid;
    //ef->ring_prox.topicid = ring_prox_topicid;
    urtNrtSubscriberInit(&self->floor_prox.nrt);
    //urtNrtSubscriberInit(&ef->ring_prox.nrt);
  #endif /* URT_CFG_PUBSUB_ENABLED == true */

  self->instruction = EF_IDLE;
  self->previous_instruction = EF_IDLE;
  self->instruction_is_new = true;
  self->velocity = 0.08;

  return;
}

/** @} */
