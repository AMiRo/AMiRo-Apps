/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    autocharge.c
 */

#include <autocharge.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief   Event mask to set on a trigger event.
 */
#define TIMER0_EVENT                     (urtCoreGetEventMask()<<1)
#define BATTEVENT_TOPICEVENT             (urtCoreGetEventMask()<<2)
#define STATEUPDATE_TOPICEVENT           (urtCoreGetEventMask()<<3)
/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of autocharge nodes.
 */
static const char _app_name[] = "AutonomousCharging";


bool online = true;
urt_osTime_t current_time;
urt_osTime_t update_time;
/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void autocharge_getData(autocharge_node_t* self, urt_osEventMask_t event){
  urtDebugAssert(self != NULL);

  // local variables
  urt_status_t status;

  if (event & BATTEVENT_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->battery_topic.nrt,
                                        &self->battery_topic.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~BATTEVENT_TOPICEVENT;
  }

  if (event & STATEUPDATE_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&self->StateUpdate_topic.nrt,
                                        &self->StateUpdate_topic.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    urtTimeNow(&update_time);
    event &= ~STATEUPDATE_TOPICEVENT;
  }
}

void autocharge_publishStateUpdateRequest(autocharge_node_t* self){
  urt_osTime_t timestamp;
  urtTimeNow(&timestamp);
  urtPublisherPublish(&self->autocharge_StateUpdateRequest_publisher,
                        &self->autocharge_stateupdate_request,
                        sizeof(StateUpdateRequest_t),
                        &timestamp,
                        URT_PUBLISHER_PUBLISH_LAZY);
}

void autocharge_publishStatus(autocharge_node_t* self, bool charging){
  self->autocharge_status.charging = charging;
  urt_osTime_t timestamp;
  urtTimeNow(&timestamp);
  urtPublisherPublish(&self->autocharge_status_publisher,
                      &self->autocharge_status,
                      sizeof(autonomouscharging_status_t),
                      &timestamp,
                      URT_PUBLISHER_PUBLISH_LAZY);
}

/**
 * Timer Event for periodic work, could also use interrupts but this is easier and is flooding proof
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _autocharge_triggercbI(virtual_timer_t* timer, void* self)
{
  (void)timer;
  autocharge_node_t* const _self = self;
  if (timer == &_self->timer.vt)
  {
    urtEventSignal(_self->node.thread, TIMER0_EVENT);
  }
}

/**
 * @brief   Setup callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _autocharge_Setup(urt_node_t* node, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  autocharge_node_t* const _self = self;
  chRegSetThreadName(_app_name);
  // Topics:
  urt_topic_t* const battery_topic = urtCoreGetTopic(_self->battery_topic.topicid);
  urtDebugAssert(battery_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->battery_topic.nrt, battery_topic, BATTEVENT_TOPICEVENT);

  urt_topic_t* const StateUpdate_topic = urtCoreGetTopic(_self->StateUpdate_topic.topicid);
  urtDebugAssert(StateUpdate_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->StateUpdate_topic.nrt, StateUpdate_topic, STATEUPDATE_TOPICEVENT);

  // activate the timer
  aosTimerPeriodicInterval(&_self->timer, (1.0f) * MICROSECONDS_PER_SECOND, _autocharge_triggercbI, _self);
  urtTimeNow(&update_time);

  return TIMER0_EVENT | BATTEVENT_TOPICEVENT | STATEUPDATE_TOPICEVENT;
}

/**
 * @brief   Loop callback function for timesend nodes.
 *
 * @param[in] node      Pointer to the node object.
 *                      Must not be NULL.
 * @param[in] event     Received event.
 * @param[in] tsend  Pointer to the tsend structure.
 *                      Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _autocharge_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  (void)node;
  urtDebugAssert(self != NULL);
  autocharge_node_t* const _self = self;

  autocharge_getData(self, event);

  if (event & TIMER0_EVENT) 
  {
    urtTimeNow(&current_time);
    uint64_t diff = urtTimeDiff(&update_time, &current_time);
    online = (diff <= (uint64_t)(20 * MICROSECONDS_PER_SECOND));
    online = false;
    switch (_self->autonomouscharging_state)
    {
    case REQUEST:
      if (_self->battery_topic.data.percentage < MIN_CHARGE_LEVEL)
      {
        /*_self->autocharge_stateupdate_request.ObjectID = 0;
        _self->autocharge_stateupdate_request.Action = QUERY_STATE_UPDATE_REQUEST;
        autocharge_publishStateUpdateRequest(_self);
        */
        if (_self->StateUpdate_topic.data.UsedByID == 0 || !online)
        {
          autocharge_publishStatus(_self, true);
          _self->autonomouscharging_state = RELEASE;
        }
      }
      break;
    case RELEASE:
      if (_self->battery_topic.data.percentage > MAX_CHARGE_LEVEL)
      {
        //_self->autocharge_stateupdate_request.Action = RELEASE_STATE_UPDATE_REQUEST;
        //autocharge_publishStateUpdateRequest(_self);
        autocharge_publishStatus(_self, false);
        
        _self->autonomouscharging_state = REQUEST;
      }
      break;
    }
    event &= ~TIMER0_EVENT;
    }
  return TIMER0_EVENT | BATTEVENT_TOPICEVENT | STATEUPDATE_TOPICEVENT;
}

/**
 * @brief @brief   TimeSend shutdown callback.
 *
 * @param[in] node      Execution node of the tsend.
 * @param[in] reason    Reason for the shutdown.
 * @param[in] tsend  Pointer to the TimeSend instance.
 */
void _autocharge_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;
  (void)reason;
}


/**
 * @brief   TimeSend (tsend) initialization function.
 *
 * @param[in] tsend               tsend object to initialize.
 * @param[in] topicID_timeSend       Topic ID where timesend publishes.
 * @param[in] frequency              Publish frequency in Hz.
 * @param[in] prio                   Priority of the execution thread.
 */
void autochargeInit(autocharge_node_t* self,
                    uint8_t boardid,
                    urt_topicid_t topicID_battery,
                    urt_topicid_t topicID_StateUpdate,
                    urt_topicid_t topicID_StateUpdateRequest,
                    urt_topicid_t topicID_ChargingState,
                    urt_osThreadPrio_t prio)
{
  urtDebugAssert(self != NULL);

  self->battery_topic.topicid = topicID_battery;
  self->StateUpdate_topic.topicid = topicID_StateUpdate;

  self->battery_topic.data_size = sizeof(self->battery_topic.data);
  self->StateUpdate_topic.data_size = sizeof(self->StateUpdate_topic.data);

  urtNrtSubscriberInit(&self->battery_topic.nrt);
  urtNrtSubscriberInit(&self->StateUpdate_topic.nrt);

  urtPublisherInit(&self->autocharge_StateUpdateRequest_publisher, urtCoreGetTopic(topicID_StateUpdateRequest));
  urtPublisherInit(&self->autocharge_status_publisher, urtCoreGetTopic(topicID_ChargingState));


  aosTimerInit(&self->timer);
  
  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
        _autocharge_Setup, self,
        _autocharge_Loop, self,
        _autocharge_Shutdown, self);

  self->battery_topic.data.percentage = 100;
  self->StateUpdate_topic.data.UsedByID = 255;
  self->autonomouscharging_state = REQUEST;
  self->autocharge_stateupdate_request.AMiRo_ID = boardid;
}

/** @} */
