/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    autocharge.h
 *
 * @defgroup autocharge autocharge
 * @ingroup apps
 * @brief   todo
 * @details todo
 *
 * @addtogroup apps_intro
 * @{
 */

#ifndef AUTOCHARGE_H
#define AUTOCHARGE_H

#include <urt.h>
#include "../../messagetypes/AutonomousChargingData.h"
#include "../../messagetypes/chargedata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(AUTOCHARGE_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of AUTOCHARGE threads.
 */
#define AUTOCHARGE_STACKSIZE             512
#endif /* !defined(AUTOCHARGE_STACKSIZE) */   
/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

#define MIN_CHARGE_LEVEL 80
#define MAX_CHARGE_LEVEL 90

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

typedef struct autocharge_battery_topic {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  battery_data_t data;
  size_t data_size;
} autocharge_battery_topic_t;

typedef struct autocharge_stateupdate_topic {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  StateUpdate_t data;
  size_t data_size;
} autocharge_stateupdate_topic_t;


/**
 * @brief   autocharge_node node.
 * @struct  autocharge_node
 */
typedef struct autocharge_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, AUTOCHARGE_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  urt_publisher_t autocharge_StateUpdateRequest_publisher;
  StateUpdateRequest_t autocharge_stateupdate_request;

  urt_publisher_t autocharge_status_publisher;
  autonomouscharging_status_t autocharge_status;

  autocharge_battery_topic_t battery_topic;
  autocharge_stateupdate_topic_t StateUpdate_topic;

  autonomouscharging_state_t autonomouscharging_state;
  aos_timer_t timer;

} autocharge_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
void autochargeInit(autocharge_node_t* self,
                    uint8_t boardid,
                    urt_topicid_t topicID_battery,
                    urt_topicid_t topicID_StateUpdate,
                    urt_topicid_t topicID_StateUpdateRequest,
                    urt_topicid_t topicID_ChargingState,
                    urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* AUTOCHARGE_H */

/** @} */
