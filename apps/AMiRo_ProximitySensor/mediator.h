// Adapted from https://gist.github.com/ashelly/5665911
// Copyright (c) 2011 ashelly.myopenid.com under <http://w...content-available-to-author-only...e.org/licenses/mit-license>
 
#ifndef SLIDING_MEDIATOR_H
#define SLIDING_MEDIATOR_H

typedef __UINT8_TYPE__ Item;
 
#define PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE      11

typedef struct Mediator_t
{
   Item data[PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE];  //circular queue of values
   __INT8_TYPE__  pos[PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE];   //index into `heap` for each value
   __INT8_TYPE__  heapStorage[PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE];  //max/median/min heap holding indexes into `data`.
   __INT8_TYPE__*  heap;  //points to middle of storage.
   __INT8_TYPE__   N;     //allocated size.
   __INT8_TYPE__   idx;   //position in circular queue
   __INT8_TYPE__   ct;    //count of items in queue
} Mediator;
 
/*--- Public Interface ---*/
 
//initializes a new mediator
void MediatorNew(Mediator* m);
 
//Inserts item, maintains median in O(lg nItems)
void MediatorInsert(Mediator* m, Item v);
 
//returns median item
Item MediatorMedian(Mediator* m);

#endif // SLIDING_MEDIATOR_H