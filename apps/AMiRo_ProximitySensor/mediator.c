// Adapted from https://gist.github.com/ashelly/5665911
// Copyright (c) 2011 ashelly.myopenid.com under <http://w...content-available-to-author-only...e.org/licenses/mit-license>
 
#include "mediator.h"

#include <amiroos.h>
#include <stdlib.h>
#include <urt.h>
 
/*--- Helper Functions ---*/
 
#define ItemLess(a,b)  ((a)<(b))
#define minCt(m) (((m)->ct-1)/2) //count of items in minheap
#define maxCt(m) (((m)->ct)/2)   //count of items in maxheap 
 
//returns 1 if heap[i] < heap[j]
//note: key function, as it is the condition for innermost nested sorting
int mmless(Mediator* m, int i, int j)
{
   return ItemLess(m->data[m->heap[i]],m->data[m->heap[j]]);
}
 
//swaps items i&j in heap, maintains indexes
int mmexchange(Mediator* m, int i, int j) // 1. 0
{
   int t = m->heap[i];
   m->heap[i]=m->heap[j];
   m->heap[j]=t;
   m->pos[m->heap[i]]=i;
   m->pos[m->heap[j]]=j;
   return 1;
}
 
//swaps items i&j only if i<j;  returns true if swapped
int mmCmpExch(Mediator* m, int i, int j)
{
   return (mmless(m,i,j) && mmexchange(m,i,j));
}
 
//maintains minheap property for all items below i/2.
void minSortDown(Mediator* m, int i)
{
   for (; i <= minCt(m); i*=2)
   {  if (i>1 && i < minCt(m) && mmless(m, i+1, i)) { ++i; }
      if (!mmCmpExch(m,i,i/2)) { break; }
   }
}
 
//maintains maxheap property for all items below i/2. (negative indexes)
void maxSortDown(Mediator* m, int i)
{
   for (; i >= -maxCt(m); i*=2)
   {  if (i<-1 && i > -maxCt(m) && mmless(m, i, i-1)) { --i; }
      if (!mmCmpExch(m,i/2,i)) { break; }
   }
}
 
//maintains minheap property for all items above i, including median
//returns true if median changed
int minSortUp(Mediator* m, int i)
{
   while (i>0 && mmCmpExch(m,i,i/2)) i/=2;
   return (i==0);
}
 
//maintains maxheap property for all items above i, including median
//returns true if median changed
int maxSortUp(Mediator* m, int i)
{
   while (i<0 && mmCmpExch(m,i/2,i))  i/=2;
   return (i==0);
}
 
/*--- Public Interface ---*/
 
 
//initializes a new mediator
void MediatorNew(Mediator* m)
{
   // Avoid even window size for pointer/memory management
   aosDbgAssert(PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE % 2 != 0);
   m->heap = &m->heapStorage[PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE / 2];
   m->N=PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE;
   m->ct = m->idx = 0;
   //set up initial heap fill pattern: median,max,min,max,...
   for (int i = PROXIMITYSENSOR_MEDIAN_WINDOW_SIZE-1; i >= 0; i--) {
      m->pos[i] = ((i+1)/2) * ((i&1)?-1:1);
      m->heap[m->pos[i]]=i;
   }
}
 
 
//Inserts item, maintains median in O(lg nItems)
void MediatorInsert(Mediator* m, Item v)
{
   int isNew=(m->ct<m->N);
   int p = m->pos[m->idx];
   Item old = m->data[m->idx];
   m->data[m->idx]=v;
   m->idx = (m->idx+1) % m->N;
   m->ct+=isNew;
   if (p>0)         //new item replaces value from minHeap
   {  if (!isNew && ItemLess(old,v)) { minSortDown(m,p*2);  }
      else if (minSortUp(m,p)) { maxSortDown(m,-1); }
   }
   else if (p<0)   //new item replaces value from maxheap
   {  if (!isNew && ItemLess(v,old)) { maxSortDown(m,p*2); }
      else if (maxSortUp(m,p)) { minSortDown(m, 1); }
   }
   else            //new item replaces value at median
   {  if (maxCt(m)) { maxSortDown(m,-1); }
      if (minCt(m)) { minSortDown(m, 1); }
   }
   
}
 
//returns median item
Item MediatorMedian(Mediator* m)
{
   Item v= m->data[m->heap[0]];
   return v;
}

///*--- Test Code ---*/
//void PrintMaxHeap(Mediator* m)
//{
//   int i;
//   if(maxCt(m))
//      urtPrintf("Max: %3d",m->data[m->heap[-1]]);
//   for (i=2;i<=maxCt(m);++i)
//   {
//      urtPrintf("|%3d ",m->data[m->heap[-i]]);
//      if(++i<=maxCt(m)) urtPrintf("%3d",m->data[m->heap[-i]]);
//   }
//   urtPrintf("\n");
//}
//void PrintMinHeap(Mediator* m)
//{
//   int i;
//   if(minCt(m))
//      urtPrintf("Min: %3d",m->data[m->heap[1]]);
//   for (i=2;i<=minCt(m);++i)
//   {
//      urtPrintf("|%3d ",m->data[m->heap[i]]);
//      if(++i<=minCt(m)) urtPrintf("%3d",m->data[m->heap[i]]);
//   }
//   urtPrintf("\n");
//}
// 
//void ShowTree(Mediator* m)
//{
//   PrintMaxHeap(m);
//   urtPrintf("Mid: %3d\n",m->data[m->heap[0]]);
//   PrintMinHeap(m);
//   urtPrintf("\n");
//}