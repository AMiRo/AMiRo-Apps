/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imagerecordingmotion.h"

#include <amiroos.h>
#include <stdlib.h>
#include <math.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define TIMER_TRIGGEREVENT          (urtCoreGetEventMask() << 1)
#define PROXFLOOREVENT              (urtCoreGetEventMask() << 2)
#define PROXENVEVENT                (urtCoreGetEventMask() << 3)
#define ODOMEVENT                   (urtCoreGetEventMask() << 4)
#define BLINKER_TRIGGEREVENT        (urtCoreGetEventMask() << 5)
#define BATTERYEVENT                (urtCoreGetEventMask() << 6)
#define CHARGINGSTATUS_TOPICEVENT   (urtCoreGetEventMask() << 7)
#define STATEUPDATE_TOPICEVENT      (urtCoreGetEventMask() << 8)
#define TIMER_REQUESTEVENT          (urtCoreGetEventMask() << 9)

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of imagerecordingmotion nodes.
 */
static const char _app_name[] = "imagerecordingmotion";

// -------------------------------------------
// Map variables

/**
 * @brief Indicates whether the wheel sensors currently detect a sideline.
 */
bool junction_line_detected = false;

/**
 * @brief Indicates whether the wheel sensors are currently looking for junctions.
 */
bool turn_imminent = false;

/**
 * @brief Position of the chargestation on the map.
 */
const int map_chargestation_pos = 0;

/**
 * @brief Hard-coded map of the showcase scenario.
 * The first dimension of next and next_legal are the neighbouring segments in clockwise direction.
 * The second dimension are the neighbouring segments in counterclockwise direction.
 */



 /*
// Outer Circle
const irm_map_segment_t map[MAP_SIZE] = {
  {.id = 0,  .length = 0.11 ,  .next = {{9,  0}, {0,  1}}, .next_legal = {{true, false}, {false, true}}}, // For this segment there is no clockwise/counterclockwise
  {.id = 1,  .length = 0.10 , .next = {{0,  9}, {10, 2}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 2,  .length = 0.24 , .next = {{1, 10}, {11, 3}}, .next_legal = {{true,  true}, {false, true}}},
  {.id = 3,  .length = 0.15 , .next = {{2, 11}, {12, 4}}, .next_legal = {{true,  false}, {false,  true}}},
  {.id = 4,  .length = 1.10 , .next = {{3, 12}, {12, 5}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 5,  .length = 0.40 , .next = {{4, 12}, {13, 6}}, .next_legal = {{true,  false}, {false,  true}}},
  {.id = 6,  .length = 2.60 , .next = {{5, 13}, {13, 7}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 7,  .length = 0.40 , .next = {{6, 13}, {11, 8}}, .next_legal = {{true,  false}, {false,  true}}}, 
  {.id = 8,  .length = 0.76 , .next = {{7, 11}, {10, 9}}, .next_legal = {{true, false}, {true,  true}}},
  {.id = 9,  .length = 0.14 , .next = {{8, 10}, { 1, 0}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 10, .length = 0.10 , .next = {{9,  8}, { 2, 1}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 11, .length = 0.89 , .next = {{8,  7}, { 3, 2}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 12, .length = 1.30 , .next = {{4,  3}, { 5, 4}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 13, .length = 0.53 , .next = {{6,  5}, { 7, 6}}, .next_legal = {{false, true}, {true, false}}}
};
*/

// Inner Circle
const irm_map_segment_t map[MAP_SIZE] = {
  {.id = 0,  .length = 0.11 ,  .next = {{9,  0}, {0,  1}}, .next_legal = {{true, false}, {false, true}}}, // For this segment there is no clockwise/counterclockwise
  {.id = 1,  .length = 0.10 , .next = {{0,  9}, {10, 2}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 2,  .length = 0.24 , .next = {{1, 10}, {11, 3}}, .next_legal = {{true,  false}, {false, true}}},
  {.id = 3,  .length = 0.15 , .next = {{2, 11}, {12, 4}}, .next_legal = {{true,  true}, {true,  false}}},
  {.id = 4,  .length = 1.10 , .next = {{3, 12}, {12, 5}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 5,  .length = 0.40 , .next = {{4, 12}, {13, 6}}, .next_legal = {{false,  true}, {true,  false}}},
  {.id = 6,  .length = 2.60 , .next = {{5, 13}, {13, 7}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 7,  .length = 0.40 , .next = {{6, 13}, {11, 8}}, .next_legal = {{false,  true}, {true,  true}}}, 
  {.id = 8,  .length = 0.76 , .next = {{7, 11}, {10, 9}}, .next_legal = {{true, false}, {true,  true}}},
  {.id = 9,  .length = 0.14 , .next = {{8, 10}, { 1, 0}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 10, .length = 0.10 , .next = {{9,  8}, { 2, 1}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 11, .length = 0.89 , .next = {{8,  7}, { 3, 2}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 12, .length = 1.30 , .next = {{4,  3}, { 5, 4}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 13, .length = 0.53 , .next = {{6,  5}, { 7, 6}}, .next_legal = {{false, true}, {true, false}}}
};

// Map for TWB
/**
 * @brief Hard-coded map of the showcase scenario.
 * The first dimension of next and next_legal are the neighbouring segments in clockwise direction.
 * The second dimension are the neighbouring segments in counterclockwise direction.
 */

/*
const irm_map_segment_t map[MAP_SIZE] = {
  {.id = 0,  .length = 0.11 ,  .next = {{9,  0}, {0,  1}}, .next_legal = {{true, false}, {false, true}}}, // For this segment there is no clockwise/counterclockwise
  {.id = 1,  .length = 0.10 , .next = {{0,  9}, {10, 2}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 2,  .length = 5.65 , .next = {{1, 10}, {10, 9}}, .next_legal = {{true,  true}, {true, true}}},
  {.id = 3,  .length = 0.15 , .next = {{2, 11}, {12, 4}}, .next_legal = {{true,  true}, {true,  false}}},
  {.id = 4,  .length = 1.10 , .next = {{3, 12}, {12, 5}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 5,  .length = 0.40 , .next = {{4, 12}, {13, 6}}, .next_legal = {{false,  true}, {true,  false}}},
  {.id = 6,  .length = 2.60 , .next = {{5, 13}, {13, 7}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 7,  .length = 0.40 , .next = {{6, 13}, {11, 8}}, .next_legal = {{false,  true}, {true,  true}}}, 
  {.id = 8,  .length = 0.76 , .next = {{7, 11}, {10, 9}}, .next_legal = {{true, false}, {true,  true}}},
  {.id = 9,  .length = 0.14 , .next = {{2, 10}, { 1, 0}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 10, .length = 0.10 , .next = {{9,  2}, { 2, 1}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 11, .length = 0.89 , .next = {{8,  7}, { 3, 2}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 12, .length = 1.30 , .next = {{4,  3}, { 5, 4}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 13, .length = 0.53 , .next = {{6,  5}, { 7, 6}}, .next_legal = {{false, true}, {true, false}}}
};
*/
/*
// Long Map -> ASE Racing
const irm_map_segment_t map[MAP_SIZE] = {
  {.id = 0,  .length = 6.00 ,  .next = {{0,  0}, {0,  0}}, .next_legal = {{true, false}, {false, true}}}, // For this segment there is no clockwise/counterclockwise
  {.id = 1,  .length = 0.10 , .next = {{0,  9}, {10, 2}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 2,  .length = 5.65 , .next = {{1, 10}, {10, 9}}, .next_legal = {{true,  true}, {true, true}}},
  {.id = 3,  .length = 0.15 , .next = {{2, 11}, {12, 4}}, .next_legal = {{true,  true}, {true,  false}}},
  {.id = 4,  .length = 1.10 , .next = {{3, 12}, {12, 5}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 5,  .length = 0.40 , .next = {{4, 12}, {13, 6}}, .next_legal = {{false,  true}, {true,  false}}},
  {.id = 6,  .length = 2.60 , .next = {{5, 13}, {13, 7}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 7,  .length = 0.40 , .next = {{6, 13}, {11, 8}}, .next_legal = {{false,  true}, {true,  true}}}, 
  {.id = 8,  .length = 0.76 , .next = {{7, 11}, {10, 9}}, .next_legal = {{true, false}, {true,  true}}},
  {.id = 9,  .length = 0.14 , .next = {{2, 10}, { 1, 0}}, .next_legal = {{true, false}, {false, true}}},
  {.id = 10, .length = 0.10 , .next = {{9,  2}, { 2, 1}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 11, .length = 0.89 , .next = {{8,  7}, { 3, 2}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 12, .length = 1.30 , .next = {{4,  3}, { 5, 4}}, .next_legal = {{false, true}, {true, false}}},
  {.id = 13, .length = 0.53 , .next = {{6,  5}, { 7, 6}}, .next_legal = {{false, true}, {true, false}}}
};
*/

// -------------------------------------------

// -------------------------------------------
// Motor variables

/**
 * @brief Setting this speed as motor command will stop the motor entirely.
 */
const float rpmStop[2] = {NAN, NAN};

/**
 * @brief Halt the movement without stopping the motor.
 */
const float rpmHalt[2] = {0.0, 0.0};

/**
 * @brief Default forward speed and rotary speed for a left-wise rotation.
 */
const float rpmTurnLeft[2] = {0.0, 2.0};

/**
 * @brief Default forward speed and rotary speed for a right-wise rotation.
 */
const float rpmTurnRight[2] = {0.0, -2.0};


float departure_speed[2] = {FORWARD_SPEED, 0.0};

/**
 * @brief Variable which saves the forward speed and rotary speed sent to the motor.
 */
float rpmSpeed[2] = {0, 0};

/**
 * @brief Variable which saves the currently used forward or reverse speed.
 */
float dynamicVelocity;

float forward_speed = 0.07;
float reverse_speed = -0.07;
// -------------------------------------------

// -------------------------------------------
// Navigation Variables

/**
 * @brief Matrix to store the shortest distance matrices between all map nodes in two directions.
 */
float M_sd[2][MAP_SIZE][MAP_SIZE];

/**
 * @brief Indicates whether the robot should currently navigate towards the charging station.
 */
bool returning_to_charge = false;

// -------------------------------------------

// -------------------------------------------
// Status Variables

void* _imagerecordingmotion;

/**
 * @brief Variable which saves the forward speed and rotary speed sent to the motor.
 */
float last_rpm[2] = {0, 0};

uint8_t id;
/**
 * @brief Edge following strategy of the robot.
 */
irm_strategy_t strategy;

/**
 * @brief Orientation of the map, to which the current strategy refers to.
 */
irm_map_orientation_t orientation = 0;

/**
 * @brief Direction of the robot on current map segment.
 */
irm_map_direction_t next_junction_dir;

/**
 * @brief Position on the map, which is the id of the current map segment.
 */
irm_map_position_t location;
uint8_t map_pos;
uint8_t run_counter = 0;
/**
 * @brief ID of the following map segment.
 */
uint8_t map_pos_next;
// -------------------------------------------

// -------------------------------------------
// Odometry Variables

/**
 * @brief Odometry values of last measurement.
 */
float old_odom[2] = {0.0, 0.0};

/**
 * @brief Accumulates the distances of odometry values for the current map segment.
 */
float distance_meter = 0.0;
// -------------------------------------------

// -------------------------------------------
// Miscellaneous variables

/**
 * @brief Increases when the robot deviated from the line, which indicates to stop it.
 */
int whiteFlag = 0;

/**
 * @brief TODO. REMOVE AND REPLACE WITH CONSTANT.
 */
int transBias = 0;

/**
 * @brief The current batterylevel of the robot in percentage.
 */
int batterylevel = 0;

/**
 * @brief The amount of subsequent docking attempts. 
 *        A high number indicates that the docking maneuver has failed.
 */
uint8_t dockingAttempts = 0;

/**
 * @brief Simple counter for the current state of the light animation.
 */
int lightAnimationCounter = 0;

/**
 * @brief Universal variable for time-scripted events in the robot maneuvers.
 *        For example used for rotation, docking and departure.
 * Only one event can be active at the same time. An event_time of 0 indicates there is no current event.
 */
float event_time = 0;
// -------------------------------------------

// -------------------------------------------
// PID controller components
// Parameters for 1. SpecialWheel AMiRo, 2. Normal BlackMiRo
//float PID_parameters[2][3] = {{2.2, 0.0, 0.0},{1.2,0.0,0.0}};

/**
 * @brief Proportional control variable  of the PID controller.
 */
float K_p = FORWARD_K_P;

/**
 * @brief Integral control variable of the PID controller.
 */
float K_i = 0.0;

/**
 * @brief Derivative control variable of the PID controller.
 */
float K_d = 0.0;

/**
 * @brief Accumulated history of errors. For the integral control.
 */
float accumHist = 0;

/**
 * @brief Error of previous control step. For the derivative control.
 */
float oldError = 0;

bool irm_online = true;
urt_osTime_t irm_current_time;
urt_osTime_t irm_update_time;
// -------------------------------------------

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief Updates the battery lights and the lights blinking in target directions. 
 *        Also checks if low battery requires return to charge or a standby.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 * 
 * @return True, if light data has changed.
 */
bool irm_updateLights(void* self) {
  imagerecordingmotion_node_t* _self = (imagerecordingmotion_node_t*)self;
  bool lights_changed = false;
  int batterylevel = 0;

  // Set lights 6 to 17 green depending on battery level
  batterylevel = _self->battery_data.data.percentage;

  if (batterylevel == 0) {
    return false;
  }

  const int charge_per_led = 10;
  // Show battery level as amount of glowing LEDs
  if (_self->state == CHARGING) {
    // Show battery level with an animation increasing each iteration up to (theoretically) 100 %
    if (lightAnimationCounter < (batterylevel / charge_per_led)) {
      _self->light_data.data.color[11-lightAnimationCounter] = GREEN;
      _self->light_data.data.color[12+lightAnimationCounter] = GREEN;
      lightAnimationCounter += 1;
    } else {
      lightAnimationCounter = 0;
      for (int i = 0; i < (batterylevel / charge_per_led); i++) {
        _self->light_data.data.color[11-i] = OFF;
        _self->light_data.data.color[12+i] = OFF;
      }
    }
    lights_changed = true;
  } else {
    for (int i = 0; i < 7; i++) {
      // Show battery level up to 60%
      if (batterylevel > (i+1) * charge_per_led) {
        // battery level signals to light the i-th LED
        if ((_self->light_data.data.color[11-i] != GREEN) || (_self->light_data.data.color[12+i] != GREEN)) {
          _self->light_data.data.color[11-i] = GREEN;
          _self->light_data.data.color[12+i] = GREEN;
          lights_changed = true;
        }
      } else if ((_self->light_data.data.color[11-i] != OFF) || (_self->light_data.data.color[12+i] != OFF)) {
        // battery level below the i-th LED, it doesn't need to be lighted anymore 
        _self->light_data.data.color[11-i] = OFF;
        _self->light_data.data.color[12+i] = OFF;
        lights_changed = true;
      }
    }
  }

  // Update the blinker (lights 2-4 left and 19-21 right)
  if (_self->light_data.data.color[4] != OFF || _self->light_data.data.color[19] != OFF) {
    for (int i = 0; i < 3; i++) {
      _self->light_data.data.color[i+2] = OFF;
      _self->light_data.data.color[21-i] = OFF;
    }
    lights_changed = true;
  } else if (_self->state == ROTATING) {
    // Blink in both directions
    for (int i = 0; i < 3; i++) {
      _self->light_data.data.color[21-i] = YELLOW;
      _self->light_data.data.color[i+2] = YELLOW;
    }
    lights_changed = true;
  } else if (turn_imminent) {
    // Blink left or right
    if (next_junction_dir == TO_LEFT) {
      for (int i = 0; i < 3; i++) _self->light_data.data.color[21-i] = YELLOW;
    } else if (next_junction_dir == TO_RIGHT) {
      for (int i = 0; i < 3; i++) _self->light_data.data.color[i+2] = YELLOW;
    }
    lights_changed = true;
  }

  return lights_changed;
}


/**
 * @brief Reads the floor sensors and calculates an error size based on target values.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 * @return Error size, based on strategy.
 */
float lf_getError(void* self){
  int threshWhite = 40000;
  // int threshBlack = 12000;
  int BThresh = 4000; //vorher 5000;
  int WThresh = 22000;

  // Get actual sensor data of both front sensors
  int FL = ((imagerecordingmotion_node_t*)self)->floor_data.data.sensors.left_front;
  int FR = ((imagerecordingmotion_node_t*)self)->floor_data.data.sensors.right_front;
  // urtPrintf("Strategy = %i - ", strategy);
  // urtPrintf("FL: %i, FR: %i\n", FL, FR);
  

  int targetL = BThresh;
  int targetR = WThresh;
  int error = 0;
  switch (strategy)
  {
  case EDGE_RIGHT:
      error = (FL + FR - targetL - targetR) / 100;
      break;
  case EDGE_LEFT:
      error = (targetL + targetR - FL - FR) / 100;
      break;
  case TRANSITION_L_R: case TRANSITION_R_L: case ROTATION_R_L: case ROTATION_L_R:
      error = lf_transitionError(FL, FR) / 100;
      break;
  default:
      break;
  }
  // Register white values
  if (FL+FR > threshWhite){
      whiteFlag += 1;
  } else if (whiteFlag > 0) {
      whiteFlag -= 1;
  }
  float result = (float) error;
  return (float) result;
}

/**
 * @brief Calculate the error size for a transition between two edges.
 * 
 * @param[in] FL Left front sensor readings.
 * @param[in] FR Right front sensor readings.
 * 
 * @return Error size, based on strategy.
 */
int lf_transitionError(int FL, int FR){
    int error = 0;
    switch (strategy)
    {
    case TRANSITION_R_L:
        error = transBias;
        break;
    case TRANSITION_L_R:
        error = -transBias;
        break;
    case ROTATION_R_L:
        error = -12000;
        whiteFlag = 0;
        break;
    case ROTATION_L_R:
        error = 12000;
        whiteFlag = 0;
        break;
    default:
        break;
    }
    //urtPrintf("transBias: %i\n", transBias);
    transBias += 400;
    if (FL+FR <= RAND_TRESH) {
      switch (strategy)
      {
        case TRANSITION_R_L:
            strategy = EDGE_LEFT;
            break;
        case TRANSITION_L_R:
            strategy = EDGE_RIGHT;
            break;
        case ROTATION_L_R:
            strategy = EDGE_RIGHT;
            break;
        case ROTATION_R_L:
            strategy = EDGE_LEFT;
        default:
            break;
      }
      transBias = 0;
    }
  return error;
}

/**
 * @brief Calculates a correction speed from floor sensor error and sets the rpm speed according to strategy.
 * 
 * @param[in] self Pointer to the imagerecordingmotion structure.
 *                Must not be NULL.
 * @param[in] velocity Velocity at which to move. Excludes any rotation/correction speed.
 */
void lf_updateCorrectionSpeed(imagerecordingmotion_node_t* self, float velocity)
{
  float error = lf_getError(self) / 100.0;
  // ---- Simplified PID Calculations ----
  float correctionSpeed = K_p * error;
  // ---- PID Calculations ----
  // float sloap = oldError - error;
  // float correctionSpeed = K_p * error + K_i * accumHist + K_d * sloap;
  // oldError = error;
  // // Protect of overflow
  // if (accumHist + error > 1000.0) {
  //   accumHist = 1000.0;
  // } else if (accumHist - error < -1000.0) {
  //   accumHist = -1000.0;
  // } else {
  //   accumHist += error;
  // }

  if (fabs(correctionSpeed) > 20.0) {
    //urtErrPrintf("correction speed at death wheel spin. aborted.");
    whiteFlag = 100;
  }
  // urtPrintf("correction speed: %.2f\n error: %.2f\n sloap: %.2f\n accumHist: %.2f", correctionSpeed, error, sloap, accumHist);
  //float correctedSpeed[2] = {NAN, 0.0};
  float correctedSpeed[2] = {velocity, correctionSpeed};

  copyRpmSpeed(correctedSpeed, rpmSpeed);
  return;

  
}

/**
 * @brief Sets the rpm speed into the target array.
 * 
 * @param[in] source Array of two floats. First one denotes the forward speed, second one the rotary speed.
 * @param[out] target Array of two floats. First one denotes the forward speed, second one the rotary speed.
 */
void copyRpmSpeed(const float* source, float* target) {
  // forward speed
  target[0] = source[0];
  // positive rotary speed rotates left, negative speed rotates right
  target[1] = source[1];
};

/**
 * @brief Retrieve new data based on events indicating their availability.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void irm_getData(void* self, urt_osEventMask_t event) {
  urtDebugAssert(self != NULL);

  // local variables
  urt_status_t status;

  if (event & PROXFLOOREVENT) {
    // fetch NRT of the floor proximity data
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->floor_data.nrt,
                                         &((imagerecordingmotion_node_t*)self)->floor_data.data,
                                         &((imagerecordingmotion_node_t*)self)->floor_data.data_size,
                                         NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
  }
  if (event & PROXENVEVENT) {
    // fetch NRT of the environment sensor data
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->environment_data.nrt,
                                         &((imagerecordingmotion_node_t*)self)->environment_data.data,
                                         &((imagerecordingmotion_node_t*)self)->environment_data.data_size,
                                         NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
  }
  // fetch NRT of the odometry data
  if (event & ODOMEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->odom_data.nrt,
                                        &((imagerecordingmotion_node_t*)self)->odom_data.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~ODOMEVENT;
  }
  // fetch NRT of the odometry data
  if (event & BATTERYEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->battery_data.nrt,
                                        &((imagerecordingmotion_node_t*)self)->battery_data.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~BATTERYEVENT;
  }

  if (event & CHARGINGSTATUS_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->chargingstatus_topic.nrt,
                                        &((imagerecordingmotion_node_t*)self)->chargingstatus_topic.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    event &= ~CHARGINGSTATUS_TOPICEVENT;
    //returning_to_charge = ((imagerecordingmotion_node_t*)self)->chargingstatus_topic.data.charging;
    //urtPrintf("Charging status: %i\n", returning_to_charge);
  }

  if (event & STATEUPDATE_TOPICEVENT) {
    do {
      status = urtNrtSubscriberFetchNext(&((imagerecordingmotion_node_t*)self)->StateUpdate_topic.nrt,
                                        &((imagerecordingmotion_node_t*)self)->StateUpdate_topic.data,
                                        NULL, NULL, NULL);
    } while (status != URT_STATUS_FETCH_NOMESSAGE);
    urtTimeNow(&irm_update_time);
    event &= ~STATEUPDATE_TOPICEVENT;
  }

  return;
}

void irm_publishStateUpdateRequest(imagerecordingmotion_node_t* self){
  urt_osTime_t timestamp;
  urtTimeNow(&timestamp);
  urtPublisherPublish(&self->irm_StateUpdateRequest_publisher,
                        &self->irm_stateupdate_request,
                        sizeof(StateUpdateRequest_t),
                        &timestamp,
                        URT_PUBLISHER_PUBLISH_LAZY);
}
/**
 * @brief Signal the led light service to update current colors
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void irm_signalLightService(void* self) {
  urtDebugAssert(self != NULL);

  if (urtNrtRequestTryAcquire(&((imagerecordingmotion_node_t*)self)->light_data.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&((imagerecordingmotion_node_t*)self)->light_data.request,
                        urtCoreGetService(((imagerecordingmotion_node_t*)self)->light_data.serviceid),
                        sizeof(((imagerecordingmotion_node_t*)self)->light_data.data),
                        0);
  } else {
    //urtPrintf("Could not acquire urt while signaling light service!");
  }
  return;
}

/**
 * @brief Signal the odometry app to reset the position to 0,0
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void irm_signalOdometryResetService(imagerecordingmotion_node_t* self) {
  urtDebugAssert(self != NULL);

  self->odom_data.data.location.axes[0] = 0.0;
  self->odom_data.data.location.axes[1] = 0.0;
  self->odom_data.data.location.axes[2] = 0.1;
  self->odom_data.data.orientation.angle = 0.0;
  
  if (urtNrtRequestTryAcquire(&self->odom_data.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&self->odom_data.request,
                        self->odom_data.service,
                        sizeof(self->odom_data.data),
                        0);
  } else {
    //urtPrintf("Could not acquire urt while signaling odomReset service!");
  }
  old_odom[0] = self->odom_data.data.location.axes[0];
  old_odom[1] = self->odom_data.data.location.axes[1];
  return;
}

/**
 * @brief Signal the motor service with new data.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 * @param[in] rpm  Array of two floats. First one denotes the forward speed, second one the rotary speed.
 */
void irm_signalMotorService(imagerecordingmotion_node_t* self, const float* rpm) {
  urtDebugAssert(self != NULL);

  self->motor_data.data.translation.axes[0] = rpm[0];
  self->motor_data.data.rotation.vector[2] = rpm[1];
  if (urtNrtRequestTryAcquire(&self->motor_data.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&self->motor_data.request,
                        self->motor_data.service,
                        sizeof(self->motor_data.data),
                        0);
  } else {
    urtPrintf("Could not acquire urt while signaling motor service!");
  }
  last_rpm[0] = rpm[0];
  last_rpm[1] = rpm[1];
  return;
}

/**
 * @brief Sets variables and light to return to charge station
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void map_returnToCharge(void* self) {
  imagerecordingmotion_node_t* _self = (imagerecordingmotion_node_t*)self;
  _self->light_data.data.color[0] = GREEN;
  _self->light_data.data.color[23] = GREEN;
  irm_signalLightService(self);
  // Set parameters
  returning_to_charge = true;
  // Potentially update strategy
  map_pos_next = map_pos;
  map_updatePosition(self);
  turn_imminent = true;
}

/**
 * @brief Updates the robot's position and withal the direction, strategy and following position.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void map_updatePosition(void* self){
  (void)self;

  ((imagerecordingmotion_node_t*)self)->irm_stateupdate_request.Action = RELEASE_STATE_UPDATE_REQUEST;
  ((imagerecordingmotion_node_t*)self)->irm_stateupdate_request.ObjectID = map_pos;
  irm_publishStateUpdateRequest(((imagerecordingmotion_node_t*)self));

  // Update position
  map_pos = map_pos_next;

  /*((imagerecordingmotion_node_t*)self)->irm_stateupdate_request.Action = BOOK_STATE_UPDATE_REQUEST;
  ((imagerecordingmotion_node_t*)self)->irm_stateupdate_request.ObjectID = map_pos;
  irm_publishStateUpdateRequest(((imagerecordingmotion_node_t*)self));*/

  // Update next target
  if (map[map_pos].next_legal[orientation][TO_LEFT] && map[map_pos].next_legal[orientation][TO_RIGHT]) {
    // Both are legal
    if (returning_to_charge) {
      forward_speed = FORWARD_SPEED;
      // Take the direction which brings you closer to charging station
      next_junction_dir = (irm_map_direction_t) \
              (M_sd[orientation][map[map_pos].next[orientation][TO_LEFT]][map_chargestation_pos] > \
               M_sd[orientation][map[map_pos].next[orientation][TO_RIGHT]][map_chargestation_pos]);
      next_junction_dir = (irm_map_direction_t) orientation; // Overrides next junction direction to Right if orientation = counterclockwise & to Left if orientation = clockwise
    } else {
      // Take random direction
      //next_junction_dir = (irm_map_direction_t) rand() % 2;
      next_junction_dir = (irm_map_direction_t) (orientation == 0) ? 1 : 0; // --> driving always the outer circle
      //next_junction_dir = (irm_map_direction_t) (orientation == 0) ? 0 : 1; // --> driving always the inner circle
      //next_junction_dir = (irm_map_direction_t) TO_RIGHT; //Overrides next junction direction to Right
    }
  } else {
    // Take the only legal direction
    next_junction_dir = (irm_map_direction_t) map[map_pos].next_legal[orientation][TO_RIGHT];
    //next_junction_dir = (irm_map_direction_t) TO_LEFT; //Overrides next junction direction to Right
  } 
  // Update strategy
  if ((strategy == EDGE_LEFT) && (next_junction_dir == TO_RIGHT)) {
    strategy = TRANSITION_L_R;
  } else if (strategy == EDGE_RIGHT && next_junction_dir == TO_LEFT) {
    strategy = TRANSITION_R_L;
  }
  // check how close the next junction is
  if (map[map_pos].length > 0.15) {
    turn_imminent = false;
  } 
  map_pos_next = map[map_pos].next[orientation][next_junction_dir];
  distance_meter = 0.0;

  urtTimeNow(&((imagerecordingmotion_node_t*)self)->latestTime);
  ((imagerecordingmotion_node_t*)self)->location.map_pos = map_pos;
  urtPublisherPublish(&((imagerecordingmotion_node_t*)self)->position_publisher,
                      &((imagerecordingmotion_node_t*)self)->location,
                      sizeof(((imagerecordingmotion_node_t*)self)->location),
                      &((imagerecordingmotion_node_t*)self)->latestTime,
                      URT_PUBLISHER_PUBLISH_LAZY);

  if (map_pos == 2 && !returning_to_charge) {
    forward_speed = FORWARD_SPEED * (((float)rand() / RAND_MAX) * (1.5f - 0.5f) + 0.5f);
  }
  
}

/**
 * @brief Checks for black junction lines based on sideward floor sensors. Should be called continuously.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 * 
 * @return True, if there was a junction which has ended.
 */
bool irm_checkForJunctionEnd(void* self){
  int blackStartFalling = 10000;
  int LW = ((imagerecordingmotion_node_t*)self)->floor_data.data.sensors.left_wheel;
  int RW = ((imagerecordingmotion_node_t*)self)->floor_data.data.sensors.right_wheel;
  // urtPrintf("LW: %i, RW: %i \n", LW, RW);

  // Only search for junctions which are anticipated
  if (turn_imminent) {
    if (next_junction_dir == TO_LEFT) {
      if ((!junction_line_detected && RW <= blackStartFalling)) {
        junction_line_detected = true;
      } else if (junction_line_detected && RW > blackStartFalling) {
        // Anticipated junction has ended.
        junction_line_detected = false;
        return true;
      }
    } else if (next_junction_dir == TO_RIGHT) {
      if ((!junction_line_detected && LW <= blackStartFalling)) {
        junction_line_detected = true;
      } else if (junction_line_detected && LW > blackStartFalling) {
        // Anticipated junction has ended.
        junction_line_detected = false;
        return true;
      }
    }
  }
  return false;
}

/**
 * @brief Checks if the front sensors detect a vertical obstacle.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 * 
 * @return True, if both front sensors are closer than 3000.
 */
bool irm_checkForWall(void* self) {
  int proximityClose_front = 550;
  int proximityClose_side = 2000;
  int NNW = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.ssw;
  int NNE = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.sse;
  //int ESE = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.ese;
  //int WSW = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.wsw;

  return (NNW > proximityClose_front || NNE > proximityClose_front); // || ESE > proximityClose_side || WSW > proximityClose_side);
}

/**
 * @brief Checks for a touch signal at the two western proximity sensors.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 * 
 * @return True, if both western side sensors are closer than 10000 which equals a loose touch.
 */
bool irm_checkForSignal(void* self) {
  int proximityClose = 10000;
  int WNW = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.wnw;
  int WSW = ((imagerecordingmotion_node_t*)self)->environment_data.data.sensors.wsw;

  return (WSW > proximityClose && WNW > proximityClose);
}

/**
 * @brief Checks if the charging pins are connected to power.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 * 
 * @return True, if the charging pins have power.
 */
bool irm_checkForPower(void* self) {
  (void)self;
  uint8_t charging_active;
  ltc4412_lld_get_stat(&moduleLldPowerPathController, &charging_active);
  // Update time
  urtTimeNow(&((imagerecordingmotion_node_t*)self)->latestTime);
  // Publish power check result
  urtPublisherPublish(&((imagerecordingmotion_node_t*)self)->power_publisher,
                      &charging_active,
                      sizeof(charging_active),
                      &((imagerecordingmotion_node_t*)self)->latestTime,
                      URT_PUBLISHER_PUBLISH_LAZY);
  return (bool) charging_active;
}

/**
 * @brief Updates the distance counter of the robot based on odometry data.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void irm_updateDistance(void* self) {
  urtDebugAssert(self != NULL);
  // Calculate Euclidean Distance
  distance_meter += sqrt(pow(old_odom[0] - ((imagerecordingmotion_node_t*)self)->odom_data.data.location.axes[0], 2) + \
                          pow(old_odom[1] - ((imagerecordingmotion_node_t*)self)->odom_data.data.location.axes[1], 2));
  old_odom[0] = ((imagerecordingmotion_node_t*)self)->odom_data.data.location.axes[0];
  old_odom[1] = ((imagerecordingmotion_node_t*)self)->odom_data.data.location.axes[1];
  return;
}

/**
 * @brief Updates the state and publishes state to topic.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 * @param[in] next_state         State to be set and published.
 */
void status_setState(void* self, irm_state_t next_state) {
  // Update state
  ((imagerecordingmotion_node_t*)self)->state = next_state;
  // Update time
  urtTimeNow(&((imagerecordingmotion_node_t*)self)->latestTime);
  // Publish state
  urtPublisherPublish(&((imagerecordingmotion_node_t*)self)->state_publisher,
                      &((imagerecordingmotion_node_t*)self)->state,
                      sizeof(((imagerecordingmotion_node_t*)self)->state),
                      &((imagerecordingmotion_node_t*)self)->latestTime,
                      URT_PUBLISHER_PUBLISH_LAZY);
  
  if (((imagerecordingmotion_node_t*)self)->backup_state != STANDBY) {
    // Publish the backup state as well
    urtPublisherPublish(&((imagerecordingmotion_node_t*)self)->nextstate_publisher,
                        &((imagerecordingmotion_node_t*)self)->backup_state,
                        sizeof(((imagerecordingmotion_node_t*)self)->backup_state),
                        &((imagerecordingmotion_node_t*)self)->latestTime,
                        URT_PUBLISHER_PUBLISH_LAZY);
  }
}

/**
 * @brief Changes the robot state to rotating state and starts an event with fixed rotary speed.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_initiateRotation(void* self) {
  imagerecordingmotion_node_t* _self = (imagerecordingmotion_node_t*)self;
  //urtPrintf("ROTATING\n");
  // Begin a rotation headstart event
  if (strategy == EDGE_LEFT) {
    strategy = ROTATION_L_R;
    copyRpmSpeed(rpmTurnLeft, rpmSpeed);
  } else {
    strategy = ROTATION_R_L;
    copyRpmSpeed(rpmTurnRight, rpmSpeed);
  }
  irm_signalMotorService(self, rpmSpeed);
  urtTimeNow(&_self->latestTime);
  event_time = (float) _self->latestTime.time;
  // Save state as backup to be restored
  _self->backup_state = _self->state;
  status_setState(self, ROTATING);
}


/**
 * @brief Reinstates the robot's backup state. Continues according to the state before the rotation.
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_concludeRotation(void* self) {  
  imagerecordingmotion_node_t* _self = (imagerecordingmotion_node_t*)self;
  // Restore state from prior to the rotation
  status_setState(self, _self->backup_state);
  _self->backup_state = STANDBY;
  switch (_self->state) {
    case DOCKING:
      //urtPrintf("DOCKING\n");
      // begin event timer
      urtTimeNow(&_self->latestTime);
      event_time = (float) _self->latestTime.time;
      break;
    case DRIVING:
      //urtPrintf("DRIVING\n");
      orientation = (irm_map_orientation_t) (orientation == MAP_CLOCKWISE); // Invert orientation
      map_pos_next = map_pos;
      map_updatePosition(self);
      turn_imminent = true;
      break;
    default:
      break;
  }
}

/**
 * @brief Changes the robot state to standby and stops the motor. 
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_beginStandby(void* self) {
  //urtPrintf("STANDBY \n");
  status_setState(self, STANDBY);
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[0] = YELLOW;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[23] = YELLOW;
  irm_signalLightService(self);
  // Halt any movement
  lf_updateCorrectionSpeed(((imagerecordingmotion_node_t*)self), HALT_SPEED);
  copyRpmSpeed(rpmHalt, rpmSpeed);
  irm_signalMotorService(self, rpmSpeed);
  urtThreadSleep(0.25);
  // Shut off motor
  copyRpmSpeed(rpmStop, rpmSpeed);
  irm_signalMotorService(self, rpmSpeed);
  turn_imminent = false; // to disable blinker
}

/**
 * @brief Changes the robot state to driving and prepares parameters accordingly. 
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_beginDriving(void* self) {
  //urtPrintf("DRIVING \n");
  irm_signalMotorService(self, rpmSpeed);
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[0] = TURQUOISE;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[23] = TURQUOISE;
  irm_signalLightService(self);
  status_setState(self, DRIVING);
  // Reset to initial values
  irm_signalOdometryResetService(self);
  K_p = FORWARD_K_P;
  whiteFlag = 0;
  accumHist = 0;
  oldError = 0;
  dockingAttempts = 0;
  distance_meter = 0.0;
  turn_imminent = true;
  junction_line_detected = false;
  returning_to_charge = false;
  // Set start at charging station
  map_pos = 0;
  strategy = EDGE_LEFT;
  //orientation = (irm_map_direction_t) rand() % 2;
  orientation = (irm_map_orientation_t) 0;
  //orientation = MAP_COUNTERCLOCKWISE;
  map_pos_next = map_pos;
  map_updatePosition(self);
}

/**
 * @brief Changes the robot state to docking. 
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_beginDocking(void* self) {// Initiate docking at the charging station
  //urtPrintf("DOCKING\n");
  status_setState(self, DOCKING);
  if (strategy == EDGE_LEFT) { // Docking station is on right side
    strategy = TRANSITION_L_R;
  }
  turn_imminent = false;
  dynamicVelocity = FORWARD_SPEED;
  whiteFlag = 0;
  distance_meter = 0.0;
}

/**
 * @brief Changes the robot state to charging, stops the motor and activates charging pins. 
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_beginCharging(void* self) {
  //urtPrintf("CHARGING \n");
  status_setState(self, CHARGING);
  // Set parameters
  copyRpmSpeed(rpmStop, rpmSpeed);
  irm_signalMotorService(self, rpmSpeed);
  dynamicVelocity = forward_speed;
  returning_to_charge = false;
  lightAnimationCounter = 0;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[0] = GREEN;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[23] = GREEN;
  irm_signalLightService(self);
  // Enable actual charging
  ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_ACTIVE);
}

/**
 * @brief Changes the robot state to departing and prepares detachment from magnet. 
 * 
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 */
void status_beginDeparture(void* self) {
  ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[0] = GREEN;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[23] = TURQUOISE;
  irm_signalLightService(self);
  irm_updateDistance(self);
  //urtPrintf("DEPARTING \n");
  status_setState(self, DEPARTING);
  ((imagerecordingmotion_node_t*)self)->backup_state = STANDBY;
  // Reset to initial values
  dynamicVelocity = 0.1; //0.8* FORWARD_SPEED;
  K_p = FORWARD_K_P;
  whiteFlag = 0;
  accumHist = 0;
  oldError = 0;
  dockingAttempts = 0;
  distance_meter = 0.0;
  // Set strategy
  strategy = EDGE_LEFT;
  //strategy = EDGE_RIGHT;
}

/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _imagerecordingmotion_triggercb(virtual_timer_t* timer, void* params)
{
  // local constants
  imagerecordingmotion_node_t* const _self = (imagerecordingmotion_node_t*)params;

  // signal node thread to read proximity data
  if (timer == &_self->timer.vt) {
    urtEventSignal(_self->node.thread, TIMER_TRIGGEREVENT);
  }

  // signal node thread to read environment data
  if (timer == &_self->blinker_timer.vt) {
    urtEventSignal(_self->node.thread, BLINKER_TRIGGEREVENT);
  }

  // signal node thread to read environment data
  if (timer == &_self->request_timer.vt) {
    urtEventSignal(_self->node.thread, TIMER_REQUESTEVENT);
  }

  return;
}

/**
 * @brief   Setup callback function for imagerecordingmotion nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] self    Pointer to the imagerecordingmotion structure.
 *                               Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _imagerecordingmotion_Setup(urt_node_t* node, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // local constants
  imagerecordingmotion_node_t* const _self = (imagerecordingmotion_node_t*)self;

  // set thread name
  chRegSetThreadName(_app_name);

  // Start timer
  aosTimerPeriodicInterval(&_self->timer, (1.0f / 100.0f) * MICROSECONDS_PER_SECOND + 0.5f, _imagerecordingmotion_triggercb, _self);
  aosTimerPeriodicInterval(&_self->blinker_timer, 0.4f * MICROSECONDS_PER_SECOND + 0.5f, _imagerecordingmotion_triggercb, _self);
  aosTimerPeriodicInterval(&_self->request_timer, 0.5f * MICROSECONDS_PER_SECOND + 0.5f, _imagerecordingmotion_triggercb, _self);

  // Set initial lights to off
  for (uint8_t led = 1; led < 23; led++) { // 24 LEDs at BlackMiro
    ((imagerecordingmotion_node_t*)self)->light_data.data.color[led] = OFF;
  }
  // Set front lights for standby mode
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[0] = YELLOW;
  ((imagerecordingmotion_node_t*)self)->light_data.data.color[23] = YELLOW;
  status_setState(self, STANDBY);

  // subscribe to the topics
  urt_topic_t* const floor_data_topic = urtCoreGetTopic(_self->floor_data.topicid);
  urtDebugAssert(floor_data_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->floor_data.nrt, floor_data_topic, PROXFLOOREVENT);

  urt_topic_t* const environment_data_topic = urtCoreGetTopic(_self->environment_data.topicid);
  urtDebugAssert(environment_data_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->environment_data.nrt, environment_data_topic, PROXENVEVENT);

  urt_topic_t* const odom_data_topic = urtCoreGetTopic(_self->odom_data.topicid);
  urtDebugAssert(odom_data_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->odom_data.nrt, odom_data_topic, ODOMEVENT);

  urt_topic_t* const battery_data_topic = urtCoreGetTopic(_self->battery_data.topicid);
  urtDebugAssert(battery_data_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->battery_data.nrt, battery_data_topic, BATTERYEVENT);

  urt_topic_t* const chargingstatus_topic = urtCoreGetTopic(_self->chargingstatus_topic.topicid);
  urtDebugAssert(chargingstatus_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->chargingstatus_topic.nrt, chargingstatus_topic, CHARGINGSTATUS_TOPICEVENT);

  urt_topic_t* const StateUpdate_topic = urtCoreGetTopic(_self->StateUpdate_topic.topicid);
  urtDebugAssert(StateUpdate_topic != NULL);
  urtNrtSubscriberSubscribe(&_self->StateUpdate_topic.nrt, StateUpdate_topic, STATEUPDATE_TOPICEVENT);

  // setup shortest distance matrices
  // set initial distances
  for (irm_map_orientation_t o = MAP_CLOCKWISE; o <= MAP_COUNTERCLOCKWISE; o++) {
    for (int i = 0; i < MAP_SIZE; i++) {
      for (int j = 0; j < MAP_SIZE; j++) {
        if (i == j)
          M_sd[o][i][i] = 0;
        else
          M_sd[o][i][j] = 1000.0;
      }
      for (int adjacent = 0; adjacent < 2; adjacent++)
        if (map[i].next_legal[o][adjacent]) 
          M_sd[o][i][map[i].next[o][adjacent]] = map[i].length;
    }
    // floyd warshall
    for (int k = 0; k < MAP_SIZE; k++)
      for (int i = 0; i < MAP_SIZE; i++)
        for (int j = 0; j < MAP_SIZE; j++)
          if (M_sd[o][i][j] > M_sd[o][i][k] + M_sd[o][k][j]) 
            M_sd[o][i][j] = M_sd[o][i][k] + M_sd[o][k][j];
  }

  srand((unsigned int) _self->latestTime.time);
  return TIMER_TRIGGEREVENT | PROXFLOOREVENT | PROXENVEVENT | ODOMEVENT | BLINKER_TRIGGEREVENT | BATTERYEVENT | CHARGINGSTATUS_TOPICEVENT | STATEUPDATE_TOPICEVENT | TIMER_REQUESTEVENT;
}

/**
 * @brief   Loop callback function for imagerecordingmotion nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] self   Pointer to the imagerecordingmotion structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _imagerecordingmotion_Loop(urt_node_t* node, urt_osEventMask_t event, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;

  // local constants
  imagerecordingmotion_node_t* const _self = (imagerecordingmotion_node_t*)self;
  _imagerecordingmotion = _self;

  if (event & (TIMER_TRIGGEREVENT | PROXFLOOREVENT | PROXENVEVENT | ODOMEVENT | BLINKER_TRIGGEREVENT | BATTERYEVENT | CHARGINGSTATUS_TOPICEVENT | STATEUPDATE_TOPICEVENT | TIMER_REQUESTEVENT)) { 
    // get the proximity data of the ring, floor and environment sensors
    irm_getData(self, event);
    if (event & BLINKER_TRIGGEREVENT) {
      if (irm_updateLights(self) == true) {
        irm_signalLightService(self);
      }
      event &= ~BLINKER_TRIGGEREVENT;
    }
    if (event & TIMER_REQUESTEVENT) {
      if (_self->StateUpdate_topic.data.UsedByID != _self->irm_stateupdate_request.AMiRo_ID)  {
        _self->irm_stateupdate_request.Action = BOOK_STATE_UPDATE_REQUEST;
        _self->irm_stateupdate_request.ObjectID = map_pos_next;
        irm_publishStateUpdateRequest(self);
      }
      event &= ~TIMER_REQUESTEVENT;
    }
    if (event & TIMER_TRIGGEREVENT) {
      urtTimeNow(&irm_current_time);
      uint64_t diff = urtTimeDiff(&irm_update_time, &irm_current_time);
      irm_online = (diff <= (uint64_t)(20 * MICROSECONDS_PER_SECOND));

      // Check for state change
      if ((_self->environment_data.data.sensors.wnw > 65000)
            && (_self->environment_data.data.sensors.ene > 65000)) {
          ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
          if (_self->state == STANDBY) {
            status_beginDriving(self);
          } else {
            status_beginStandby(self);
          }
          //Wait a little bit so that the environment sensors are untouched
          urtThreadSSleep(2);
      } else {
        // Do state action
        switch (_self->state) {
          case STANDBY:
            urtThreadSSleep(2);
            // Check for intended departure before checking for power
            if (_self->backup_state == DEPARTING) {
              // This case occurs when departing from the station
              _self->backup_state = STANDBY;
              urtThreadSleep(10);
              status_beginDeparture(self);
            } else {
              // Default code for standby
              // Regularly check for manual insertion to charging station
              _self->light_data.data.color[1] = YELLOW;
              irm_signalLightService(self);
              ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_ACTIVE);
              urtThreadSSleep(1);
              if (irm_checkForPower(self)) {
                status_beginCharging(self);
                _self->light_data.data.color[1] = OFF;
                irm_signalLightService(self);
                break;
              }
              ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
              _self->light_data.data.color[1] = OFF;
              irm_signalLightService(self);
            }
            break;
          case DRIVING:
            // Default code for standby
            // Navigate the map and return to charge at low battery 

            irm_updateDistance(self);
            lf_updateCorrectionSpeed(_self, forward_speed);
            if (!turn_imminent && distance_meter > 0.4 * map[map_pos].length) {
              turn_imminent = true;
            }
            if (turn_imminent && irm_online) {
              if (_self->StateUpdate_topic.data.UsedByID != _self->irm_stateupdate_request.AMiRo_ID)  {
                copyRpmSpeed(rpmStop, rpmSpeed);
              }
            }
            if (irm_checkForJunctionEnd(_self)) {
              map_updatePosition(self);
              // Update time
              urtTimeNow(&_self->latestTime);
              // Publish current position
              //_self->location.map_pos = map_pos;
              //urtPublisherPublish(&_self->position_publisher,
              //                    &location,
              //                    sizeof(location),
              //                    &_self->latestTime,
              //                    URT_PUBLISHER_PUBLISH_LAZY);
              // Publish current strategy
              urtPublisherPublish(&_self->strategy_publisher,
                                  &strategy,
                                  sizeof(strategy),
                                  &_self->latestTime,
                                  URT_PUBLISHER_PUBLISH_LAZY);
            }
            irm_signalMotorService(self, rpmSpeed);
            if (irm_checkForWall(self)) {
              status_initiateRotation(_self);
            } 
            // Check if return to charge is required or signalled
            if (!returning_to_charge && (_self->battery_data.data.percentage < MIN_CHARGELEVEL || irm_checkForSignal(self))) {
              // Don't change directions close to junction
              if (distance_meter + 0.1 < map[map_pos].length && map_pos != 0) {
                map_returnToCharge(_self);
              }
            }
            // Check if already arrived at charging station
            if (returning_to_charge && map_pos == 0) {
              status_beginDocking(self);
            } 
            if (whiteFlag > WHITE_STOP) {
              status_beginStandby(self);
              _self->light_data.data.color[0] = RED;
              _self->light_data.data.color[23] = RED;
              irm_signalLightService(self);
            } 
            break;
          case ROTATING:
            // Event defines a rotation headstart with fixed correction speed
            if (event_time != 0) {
              urtTimeNow(&_self->latestTime);
              if (((float) _self->latestTime.time > event_time + 1.3 * MICROSECONDS_PER_SECOND)) {
                event_time = 0;
              }
            } else {
              lf_updateCorrectionSpeed(_self, HALT_SPEED);
              irm_signalMotorService(self, rpmSpeed);
              if ((strategy == EDGE_LEFT) || (strategy == EDGE_RIGHT)) {
                status_concludeRotation(self);
              }
            }
            break;
          case DOCKING:
            // Follow docking event chain
            // 1. Drive until close to docking station -> Rotate!
            lf_updateCorrectionSpeed(_self, dynamicVelocity);
            irm_updateDistance(self);
            if (irm_checkForWall(self)) {
              status_initiateRotation(_self);
            } 
            // 2. Drive backwards and forwards until the docking is complete
            if (event_time != 0) {
              urtTimeNow(&_self->latestTime);
              if (dynamicVelocity > 0) {
                if (((float) _self->latestTime.time > event_time + 1.0 * MICROSECONDS_PER_SECOND)) {
                  event_time = (float) _self->latestTime.time;
                  dynamicVelocity = REVERSE_SPEED;
                  K_p = 0.25; //REVERSE_K_P;
                }
              } else if (dynamicVelocity < 0) {
                if (((float) _self->latestTime.time > event_time + 2.5 * MICROSECONDS_PER_SECOND)) { //Driving 3.5 seconds backwards? 
                  // Deactivate motor to fully attach to station
                  irm_signalMotorService(self, rpmStop);
                  urtThreadSleep(0.5);
                  // Check if the AMiRo is fixed to magnet and thus did not move since last iteration
                  //if (distance_meter < 0.05) {
                    // Activate power pins and begin charging
                    ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_ACTIVE);
                    _self->light_data.data.color[1] = GREEN;
                    irm_signalLightService(self);
                    urtThreadSleep(1);
                    _self->light_data.data.color[1] = OFF;
                    irm_signalLightService(self);
                    for (int j=0; j<5;j++){
                      irm_checkForPower(self);
                      urtThreadSleep(0.5);
                    }
                    if (irm_checkForPower(self)) {
                      event_time = 0;
                      status_beginCharging(self);
                      break;
                    }
                    ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
                  //} 
                  dockingAttempts += 1;
                  if (dockingAttempts >= 7) {
                    // Abort docking attempts for now
                    event_time = 0;
                    status_beginDeparture(self);
                  } else {
                    // Try again to drive forwards and backwards
                    urtTimeNow(&_self->latestTime);
                    event_time = (float) _self->latestTime.time;
                    irm_updateDistance(self);
                    distance_meter = 0.0;
                    dynamicVelocity = 0.2 * FORWARD_SPEED;
                    K_p = FORWARD_K_P;
                    // Publish attempt counter
                    urtPublisherPublish(&_self->docking_publisher,
                                        &dockingAttempts,
                                        sizeof(dockingAttempts),
                                        &_self->latestTime,
                                        URT_PUBLISHER_PUBLISH_LAZY);
                  }
                }
              }
              // Also check for divertion from line
              if (whiteFlag > WHITE_STOP) {
                status_beginStandby(self);
                _self->light_data.data.color[0] = RED;
                _self->light_data.data.color[23] = RED;
                irm_signalLightService(self);
              }
            }
            
            irm_signalMotorService(self, rpmSpeed);
            break;
          case CHARGING:
            if (irm_checkForPower(self)) {
              // Charge until full
              batterylevel = _self->battery_data.data.percentage;
              if (batterylevel >= MAX_CHARGELEVEL) { 
                ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
                urtThreadSleep(2);
                if (!irm_checkForPower(self)) {
                  _self->backup_state = DEPARTING;
                  status_beginStandby(self);
                  urtThreadSleep(1);
                }
              }
              urtThreadSleep(1);
            } else {
              // Abort charging
              ltc4412_lld_set_ctrl(&moduleLldPowerPathController, LTC4412_LLD_CTRL_INACTIVE);
              urtThreadSSleep(1);
              status_beginDeparture(self);
            }
            break;
          case DEPARTING:
            lf_updateCorrectionSpeed(_self, dynamicVelocity);
            // Begin each departing attempt at standstill
            //irm_signalMotorService(self, rpmHalt);
            urtThreadSleep(0.25);
            // Increase the velocity until successful departure
            copyRpmSpeed(departure_speed, rpmSpeed);
            irm_signalMotorService(self, rpmSpeed);
            if (dynamicVelocity <= 2.0 * FORWARD_SPEED) {
              dynamicVelocity += 0.005;
            }
            // Enter driving state upon successful departure
            irm_updateDistance(self);
            if (distance_meter > 0.05) {
              status_beginDriving(_self);
            }
            break;
        }
      }
      event &= ~TIMER_TRIGGEREVENT;
    }
  } 

  return TIMER_TRIGGEREVENT | PROXFLOOREVENT | PROXENVEVENT | ODOMEVENT | BLINKER_TRIGGEREVENT | BATTERYEVENT | CHARGINGSTATUS_TOPICEVENT | STATEUPDATE_TOPICEVENT | TIMER_REQUESTEVENT;
}

/**
 * @brief   Shutdown callback function for imagerecordingmotion nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] floor  Pointer to the imagerecordingmotion structure.
 *                    Must nor be NULL.
 */
void _imagerecordingmotion_Shutdown(urt_node_t* node, urt_status_t reason, void* self)
{
  urtDebugAssert(self != NULL);
  (void)node;
  (void)reason;

  // local constants
  imagerecordingmotion_node_t* const _self = (imagerecordingmotion_node_t*)self;

  // unsubscribe
  urtNrtSubscriberUnsubscribe(&_self->floor_data.nrt);
  urtNrtSubscriberUnsubscribe(&_self->environment_data.nrt);
  urtNrtSubscriberUnsubscribe(&_self->odom_data.nrt);
  urtNrtSubscriberUnsubscribe(&_self->battery_data.nrt);
  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
int imagerecordingmotion_ShellCallback(BaseSequentialStream* stream, int argc, const char* argv[], imagerecordingmotion_node_t* self) {
  
  urtDebugAssert(self != NULL);
  (void)argc;
  (void)argv;

  bool print_help = (argc < 2) || (argc > 3);
  if (!print_help) {
    int case_number = atoi(argv[1]);
    if (case_number == 0) {
      int value = atoi(argv[2]);
      if (value == 0) {
        // Special case for standby
        status_beginStandby(self);
      } else if (value == 1) {
        // Special case for driving
        status_beginDriving(self);
      } else if (value == 2) {
        // Special case for rotating
        status_initiateRotation(self);
      } else if (value == 3) {
        // Special case for docking
        status_beginDocking(self);
      } else if (value == 4) {
        // Special case for charging
        status_beginCharging(self);
      } else if (value == 5) {
        // Special case for departing
        status_beginDeparture(self);
      } else
      {
        chprintf(stream, "Invalid State.\n");
      }
    } else if (case_number == 1){
      int value = atoi(argv[2]);
      map_pos_next = value;
      map_updatePosition(self);
      chprintf(stream, "Set Position.\n");
    } else if (case_number == 2){
      float value = atof(argv[2]);
      K_p = value;
      // Will be activated immediately
      //chprintf(stream, "!You must touch the ? side sensors to start/end the ObstacleAvoidance!\n");
    } else {
      print_help = true;
    }
  }

  if (print_help) {
    chprintf(stream, "Usage: %s Parameter Modification\n", argv[0]);
    chprintf(stream, "0 - STATE:\n");
    chprintf(stream, "  State to be set. Must be an int:\n");
    chprintf(stream, "  0 - 1 - 2 - 3 - 4 - 5 \n");
    chprintf(stream, "1 - POSITION:\n");
    chprintf(stream, "  Position on the Map. Must be an int:\n");
    chprintf(stream, "2 - P-Value:\n");
    chprintf(stream, "  P-Value of the LineFollowing Controller. Must be a float:\n");
    return AOS_INVALIDARGUMENTS;
  }
  return AOS_OK;
}
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */


/**
 * @brief   Initialize a imagerecordingmotion node.
 *
 * @param[in] self           Pointer to the imagerecordingmotion node to initialize.
 * @param[in] dmc_target_serviceid      Service identifier for the DifferentialMotorControl target motion service.
 * @param[in] odom_reset_serviceid      Service identifier for the Odometry position reset service.
 * @param[in] led_light_serviceid       Service identifier for the LED lights.
 * @param[in] proximity_topicid         Topic ID to subscribe proximity data.
 * @param[in] environment_topicid       Topic ID to subscribe environment data.
 * @param[in] odom_topicid              Topic ID to subscribe odometry data.
 * @param[in] battery_topicid           Topic ID to subscribe battery level data.
 * @param[in] state_topicid             Topic ID to publish state data.
 * @param[in] nextstate_topicid         Topic ID to publish state data.
 * @param[in] strategy_topicid          Topic ID to publish state data.
 * @param[in] position_topicid          Topic ID to publish state data.
 * @param[in] docking_topicid           Topic ID to publish state data.
 * @param[in] power_topicid             Topic ID to publish state data.
 * @param[in] prio                      Priority of the imagerecordingmotion node thread.
 */
void imagerecordingmotionInit(imagerecordingmotion_node_t* self,
                 uint8_t boardid, 
                 urt_serviceid_t dmc_target_serviceid,
                 urt_serviceid_t odom_reset_serviceid,
                 urt_serviceid_t led_light_serviceid,
                 urt_topicid_t proximity_topicid,
                 urt_topicid_t environment_topicid,
                 urt_topicid_t odom_topicid,
                 urt_topicid_t battery_topicid,
                 urt_topicid_t state_topicid,
                 urt_topicid_t nextstate_topicid,
                 urt_topicid_t strategy_topicid,
                 urt_topicid_t position_topicid,
                 urt_topicid_t docking_topicid,
                 urt_topicid_t power_topicid,
                 urt_topicid_t chargingstatus_topicid,     // Topic to subscribe charging status
                 urt_topicid_t topicID_StateUpdate,
                 urt_topicid_t topicID_StateUpdateRequest,
                 urt_osThreadPrio_t prio)
{
  self->state = STANDBY;
  self->backup_state = STANDBY;
  self->floor_data.topicid = proximity_topicid;
  self->environment_data.topicid = environment_topicid;
  self->odom_data.topicid = odom_topicid;
  self->battery_data.topicid = battery_topicid;
  self->chargingstatus_topic.topicid = chargingstatus_topicid;
  self->StateUpdate_topic.topicid = topicID_StateUpdate;
  self->floor_data.data_size = sizeof(((imagerecordingmotion_node_t*)self)->floor_data.data);
  self->environment_data.data_size = sizeof(((imagerecordingmotion_node_t*)self)->environment_data.data);
  self->odom_data.data_size = sizeof(((imagerecordingmotion_node_t*)self)->odom_data.data);
  self->battery_data.data_size = sizeof(((imagerecordingmotion_node_t*)self)->battery_data.data);
  self->chargingstatus_topic.data_size = sizeof(((imagerecordingmotion_node_t*)self)->chargingstatus_topic.data);
  self->StateUpdate_topic.data_size = sizeof(self->StateUpdate_topic.data);

  self->irm_stateupdate_request.AMiRo_ID = boardid;

  // initialize the node
  urtNodeInit(&self->node, (urt_osThread_t*)self->thread, sizeof(self->thread), prio,
              _imagerecordingmotion_Setup, self,
              _imagerecordingmotion_Loop, self,
              _imagerecordingmotion_Shutdown, self);

  // initialize service requests
  urtNrtRequestInit(&self->motor_data.request, &self->motor_data.data);
  urtNrtRequestInit(&self->odom_data.request, &self->odom_data.data);
  urtNrtRequestInit(&self->light_data.request, &self->light_data.data);
  self->motor_data.service = urtCoreGetService(dmc_target_serviceid);
  self->odom_data.service = urtCoreGetService(odom_reset_serviceid);
  self->light_data.serviceid = led_light_serviceid;

  // initialize trigger timer
  aosTimerInit(&self->timer);
  aosTimerInit(&self->blinker_timer);
  aosTimerInit(&self->request_timer);

  // initialize subscriber
  urtNrtSubscriberInit(&((imagerecordingmotion_node_t*)self)->floor_data.nrt);
  urtNrtSubscriberInit(&((imagerecordingmotion_node_t*)self)->environment_data.nrt);
  urtNrtSubscriberInit(&((imagerecordingmotion_node_t*)self)->odom_data.nrt);
  urtNrtSubscriberInit(&((imagerecordingmotion_node_t*)self)->battery_data.nrt);
  urtNrtSubscriberInit(&((imagerecordingmotion_node_t*)self)->chargingstatus_topic.nrt);
  urtNrtSubscriberInit(&self->StateUpdate_topic.nrt);
  
  // initialize publisher
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->state_publisher, urtCoreGetTopic(state_topicid));
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->nextstate_publisher, urtCoreGetTopic(nextstate_topicid));
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->position_publisher, urtCoreGetTopic(position_topicid));
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->strategy_publisher, urtCoreGetTopic(strategy_topicid));
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->docking_publisher, urtCoreGetTopic(docking_topicid));
  urtPublisherInit(&((imagerecordingmotion_node_t*)self)->power_publisher, urtCoreGetTopic(power_topicid));
  urtPublisherInit(&self->irm_StateUpdateRequest_publisher, urtCoreGetTopic(topicID_StateUpdateRequest));
  return;
}
