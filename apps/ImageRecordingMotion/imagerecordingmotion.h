/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    imagerecordingmotion.h
 * @brief   Example app testing motor functions.
 *
 * @addtogroup imagerecordingmotion
 * @{
 */

#ifndef IMAGERECORDINGMOTION_H
#define IMAGERECORDINGMOTION_H

#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/positiondata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/chargedata.h"
#include "../../messagetypes/imagerecordingmotion_data.h"
#include "../../messagetypes/AutonomousChargingData.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(imagerecordingmotion_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of floor threads.
 */
#define imagerecordingmotion_STACKSIZE 1024
#endif /* !defined(imagerecordingmotion_STACKSIZE) */

#define RAND_TRESH 20000
#define MAX_CORRECTED_SPEED 1000000*100
#define MAP_SIZE 14
#define MAX_CHARGELEVEL 90
#define MIN_CHARGELEVEL 70
#define REQ_CHARGELEVEL 5
#define FORWARD_K_P 0.9f
#define REVERSE_K_P 1.5f
#define FORWARD_SPEED 0.07f
#define REVERSE_SPEED -0.07f
#define HALT_SPEED 0.0f
#define WHITE_STOP 100

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/
/**
 * @brief A map segment consists of one line between two junctions. 
 * Each junction consists of two forking lines, so there is a total of four neighbouring map segments.
 */
typedef struct irm_map_segment {
  /**
   * @brief Unique id of the map segment on the map.
   */
  int id;

  /**
   * @brief Length of the line in meter.
   */
  float length;

  /**
   * @brief Neighbouring map segment ids. First dimension is the direction, second dimension is left or right.
   */
  uint8_t next[2][2];

  /**
   * @brief Describes whether the transition from the current map segment to the respective neighbour is legal.
   * True, if the transition is legal. First dimension is the direction, second dimension is left or right.
   */
  bool next_legal[2][2];
} irm_map_segment_t;

/**
* @brief  Data of the light
*/
typedef struct irm_light {
  light_led_data_t data;
  urt_nrtrequest_t request;
  urt_serviceid_t serviceid;
}irm_light_t;

typedef struct irm_chargingstatus_topic {
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 autonomouscharging_status_t data;
 size_t data_size;
}irm_chargingstatus_topic_t;

/**
* @brief   Trigger related data of the floor sensors.
*/
typedef struct irm_floor{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 floor_proximitydata_t data;
 size_t data_size;
}irm_floor_t;

/**
* @brief   Trigger related data of the environment sensors.
*/
typedef struct irm_environment{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 proximitysensor_proximitydata_t data;
 size_t data_size;
}irm_environment_t;

/**
* @brief   Trigger related data of the odometry data.
*/
typedef struct irm_odom{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 position_cv_ccsi data;
 size_t data_size;
 urt_nrtrequest_t request;
 urt_service_t* service;
}irm_odom_t;

/**
* @brief   Trigger related data of the battery data.
*/
typedef struct irm_battery{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 battery_data_t data;
 size_t data_size;
}irm_battery_t;

/**
* @brief  Data of the motor
*/
typedef struct irm_motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t* service;
}irm_motor_t;

typedef struct irm_stateupdate_topic {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  StateUpdate_t data;
  size_t data_size;
} irm_stateupdate_topic_t;

/**
 * @brief   imagerecordingmotion node.
 * @struct  imagerecordingmotion_node
 */
typedef struct imagerecordingmotion_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, imagerecordingmotion_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief   state 
   */
  irm_state_t state;

  /**
   * @brief   Position on the map 
   */
  irm_map_position_t location;



  /**
   * @brief  Backup of the latest state. 
   *         Used for temporary state changes to return to an intended state.
  */
  irm_state_t backup_state;

  /**
   * @brief   Publisher to publish the state data
   */
  urt_publisher_t state_publisher;

  /**
   * @brief   Publisher to publish the backup state data
   */
  urt_publisher_t nextstate_publisher;

  /**
   * @brief   Publisher to publish the strategy
   */
  urt_publisher_t strategy_publisher;

  /**
   * @brief   Publisher to publish the debug msg
   */
  urt_publisher_t debug_publisher;

  /**
   * @brief   Publisher to publish the map position
   */
  urt_publisher_t position_publisher;

  /**
   * @brief   Publisher to publish the docking attempts
   */
  urt_publisher_t docking_publisher;

  /**
   * @brief   Publisher to publish the power check results
   */
  urt_publisher_t power_publisher;

  /**
   * @brief   Contains data of the LED lights and service.
   */
  irm_light_t light_data;

  /**
   * @brief  Contains floor proximity subscriber and data.
   */
  irm_floor_t floor_data;

  /**
   * @brief  Contains odometry subscriber and data.
   */
  irm_odom_t odom_data;

  /**
   * @brief  Contains odometry subscriber and data.
   */
  irm_battery_t battery_data;

  irm_chargingstatus_topic_t chargingstatus_topic;

  urt_publisher_t irm_StateUpdateRequest_publisher;
  StateUpdateRequest_t irm_stateupdate_request;

  irm_stateupdate_topic_t StateUpdate_topic;

  /**
   * @brief  Contains sensor environment subscriber and data.
   */
  irm_environment_t environment_data;

  /**
   * @brief  Contains data for request to motor.
   */
  irm_motor_t motor_data;

  /**
   * @brief   Timer to trigger loop for motor request.
   */
  aos_timer_t timer;

  /**
   * @brief   Timer to trigger a blinking light.
   */
  aos_timer_t blinker_timer;

  aos_timer_t request_timer;

  /**
   * @brief   Time of latest measurement.
   */
  urt_osTime_t latestTime;

} imagerecordingmotion_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void imagerecordingmotionInit(imagerecordingmotion_node_t* imagerecordingmotion,
                 uint8_t boardid,
                 urt_serviceid_t dmc_target_serviceid,
                 urt_serviceid_t odom_reset_serviceid,
                 urt_serviceid_t led_light_serviceid,
                 urt_topicid_t proximity_topicid,
                 urt_topicid_t environment_topicid,
                 urt_topicid_t odom_topicid,
                 urt_topicid_t battery_topicid,
                 urt_topicid_t state_topicid,
                 urt_topicid_t nextstate_topicid,
                 urt_topicid_t strategy_topicid,
                 urt_topicid_t position_topicid,
                 urt_topicid_t docking_topicid,
                 urt_topicid_t power_topicid,
                 urt_topicid_t chargingstatus_topicid,
                 urt_topicid_t topicID_StateUpdate,
                 urt_topicid_t topicID_StateUpdateRequest,
                 urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

// map and navigation related functions
void map_returnToCharge(void* self);
void map_updatePosition(void* self);

// line following related functions
void copyRpmSpeed(const float* source, float* target);
int lf_transitionError(int FL, int FR);
float lf_getError(void* imagerecordingmotion);
void lf_updateCorrectionSpeed(imagerecordingmotion_node_t* pmn, float velocity);

// robot state related functions
void status_initiateRotation(void* self);
void status_concludeRotation(void* self);
void status_beginStandby(void* self);
void status_beginDriving(void* self);
void status_beginDocking(void* self);
void status_beginCharging(void* self);
void status_beginDeparture(void* self);

// functions related to process of data received outside of the app
void irm_getData(void* self, urt_osEventMask_t event);
bool irm_checkForJunctionEnd(void* self);
bool irm_checkForWall(void* self);
bool irm_checkForSignal(void* self);
bool irm_checkForPower(void* self);
void irm_updateDistance(void* self);
bool irm_updateLights(void* self);

// functions related to services outside the app
void irm_signalLightService(void* self);
void irm_signalOdometryResetService(imagerecordingmotion_node_t* self);
void irm_signalMotorService(imagerecordingmotion_node_t* self, const float* rpm);

int imagerecordingmotion_ShellCallback(BaseSequentialStream* stream, int argc, const char* argv[], imagerecordingmotion_node_t* self);
/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* IMAGERECORDINGMOTION_H */

/** @} */
