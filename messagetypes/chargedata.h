/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    chargedata.h
 * @brief   Data type holding charge information.
 *
 * @defgroup msgtypes_charge Charge
 * @ingroup	msgtypes
 * @brief   todo
 * @details todo
 *
 * @addtogroup msgtypes_charge
 * @{
 */

#ifndef CHARGEDATA_H
#define CHARGEDATA_H

#include <stdint.h>
#include <stdbool.h>
/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Charge data using µV representation.
 * @struct chargedata_u
 */
typedef struct chargedata_u {
  /**
   * @brief   Voltage in µV.
   */
  uint32_t volts;

  /**
   * @brief   Flag whether charging is active.
   */
  bool charging;
} chargedata_u;

typedef struct adcdata_u {
  /**
   * @brief   Voltage in µV.
   */
  uint32_t volts;

  /**
   * @brief   Flag whether charging is active.
   * 0 is non charging, 1 is charging
   */
  uint8_t charging;
} adcdata_u;

/**
 * @brief   Charge data using SI representation.
 * @struct chargedata_si
 */
typedef struct chargedata_si {
  /**
   * @brief   Voltage in V.
   */
  float volts;

  /**
   * @brief   Flag whether charging is active.
   */
  bool charging;
} chargedata_si;

/**
 * @brief   Charge data of the battery level.
 * @struct battery_data_t
 */
typedef struct battery_data {

  /**
   * @brief   Average battery level over front and rear battery in percent.
   */
  uint16_t percentage;

  /**
   * @brief   Average battery level over front and rear battery in mAh.
   */
  uint16_t size;

} battery_data_t;

typedef struct battery_cell {
  uint16_t SoC;
  uint16_t SoH;
  uint16_t voltage;
  uint16_t avg_current;
  uint16_t cycleCount;
  uint16_t fullChargeCapacity;
  uint16_t fullAvailableCapacity;
} battery_cell_t;

typedef struct amiro_battery_pack {
  battery_cell_t front;
  battery_cell_t rear;
} amiro_battery_pack_t;

typedef enum systemcontrol_data {
  START,
  SHUTDOWN,
  RESTART
} systemcontrol_data_t;


/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* CHARGEDATA_H */

/** @} */
