#ifndef FPGAMANAGERD_H
#define FPGAMANAGERD_H

#include <stdint.h>

#define FPGA_SERIAL_DATA_SIZE_IN_BYTES 32

typedef union fpga_status 
{
	struct {
	uint8_t done:      1;
	uint8_t prog_n:    1;
	uint8_t spi_dir:   1;
	} signals;
	uint32_t data;
} fpga_status_t;

typedef enum {
  FPGA_SET_PROGN,
  FPGA_CHANGE_SPI
} fpga_control_action_t;

typedef struct {
  fpga_control_action_t action;
  uint8_t param;
} fpga_control_t;

typedef struct fpga_serial_data
{
	uint8_t data[FPGA_SERIAL_DATA_SIZE_IN_BYTES];
} fpga_serial_data_t;

#endif