/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    imagerecordingmotion_data.h
 * @brief   Data type describing data of the Perpetual Motion App  
 *
 * @defgroup msgtypes_imagerecordingmotion imagerecordingmotion
 * @ingroup	msgtypes
 * @brief   todo
 * @details todo
 *
 * @addtogroup msgtypes_remote
 * @{
 */

#ifndef IMAGERECORDINGMOTIONDATA_H
#define IMAGERECORDINGMOTIONDATA_H

#include <stdint.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/


// Buffer for sensor values
typedef struct sensorRecord {
  int FL;
  int FR;
  int delta;
  int error;
} sensorRecord_t;

/**
 * @brief imagerecordingmotion states
 */
typedef enum irm_state {
  STANDBY,
  DRIVING,
  ROTATING,
  DOCKING,
  CHARGING,
  DEPARTING
}irm_state_t;

typedef struct irm_map_position {
  uint8_t map_pos;
} irm_map_position_t;

/**
 * @brief imagerecordingmotion directions at a fork on the map.
 */
typedef enum irm_map_direction {
  TO_LEFT,
  TO_RIGHT
} irm_map_direction_t;

/**
 * @brief imagerecordingmotion orientations for viewing the map.
 */
typedef enum irm_map_orientation {
  MAP_CLOCKWISE,
  MAP_COUNTERCLOCKWISE
} irm_map_orientation_t;

typedef enum irm_strategy{
  EDGE_LEFT,      // driving on the left edge of a black line
  TRANSITION_L_R, // Transition from left to right edge
  TRANSITION_R_L, // transition from right to left edge
  EDGE_RIGHT,     // driving on the right edge of a black line
  ROTATION_L_R,   // Transition from left to right edge via rotation
  ROTATION_R_L,   // transition from right to left edge via rotation
  REVERSE,
  NONE
} irm_strategy_t;







/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* REMOTEDATA_H */

/** @} */
