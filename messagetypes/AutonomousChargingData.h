#ifndef AUTOCHARGEDATA_H
#define AUTOCHARGEDATA_H

#include <stdint.h>
#include <stdbool.h>

typedef struct autonomouscharging_status {
	bool charging;
} autonomouscharging_status_t;

typedef enum autonomouscharging_state{
  REQUEST,
  RELEASE
} autonomouscharging_state_t;

typedef struct StateUpdateRequest {
  uint8_t AMiRo_ID;
  uint8_t ObjectID;
  enum {
    QUERY_STATE_UPDATE_REQUEST,
    BOOK_STATE_UPDATE_REQUEST,
    RELEASE_STATE_UPDATE_REQUEST
  } Action;
} StateUpdateRequest_t;

typedef struct StateUpdate {
  uint8_t ObjectID;
  uint8_t UsedByID;
  uint16_t costs;
} StateUpdate_t;

#endif /* AUTOCHARGEDATA_H */