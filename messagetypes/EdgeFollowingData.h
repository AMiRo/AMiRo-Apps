/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    EdgeFolowingData.h
 * @brief   Data types for EdgeFolowing App
 *
 * @defgroup msgtypes_edgefollowing EdgeFolowing
 * @ingroup	msgtypes
 * @brief   todo
 * @details todo
 *
 * @addtogroup msgtypes_edgefollowing
 * @{
 */

#ifndef EDGEFOLLOWINGDATA_H
#define EDGEFOLLOWINGDATA_H

#include <stdint.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief EdgeFollowing states
 */
typedef enum ef_instruction {
  EF_IDLE,
  EF_FOLLOW_LEFT,
  EF_FOLLOW_RIGHT,
  EF_TURN_AROUND,
  EF_FOLLOW_STRAIGHT,
  EF_BLOCKED,
  EF_INSTRUCTION_COUNT
} ef_instruction_t;

/**
 * @brief EdgeFollowing events
 */
typedef enum ef_event {
  EF_INSTRUCTION_CHANGED_EVENT,
  EF_BLOCKED_EVENT,
  EF_INVALID_EVENT
} ef_event_t;

/**
 * @brief EdgeFollowing event payload
 */
typedef struct edgefollowing_event_data {
  uint8_t event;
  uint8_t instruction;
  uint8_t data1;
  uint8_t data2;
} edgefollowing_event_data_t;

typedef struct edgefollowing_service_data {
  ef_instruction_t instruction;
  float velocity;
} edgefollowing_service_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/


#endif /* EDGEFOLLOWINGDATA_H */

/** @} */

