/*
AMiRo-Apps is a collection of applications and configurations for the
Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2022  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU (Lesser) General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU (Lesser) General Public License for more details.

You should have received a copy of the GNU (Lesser) General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    perpetualmotion_data.h
 * @brief   Data type describing data of the Perpetual Motion App  
 *
 * @defgroup msgtypes_perpetualmotion PerpetualMotion
 * @ingroup	msgtypes
 * @brief   todo
 * @details todo
 *
 * @addtogroup msgtypes_remote
 * @{
 */

#ifndef PERPETUALMOTIONDATA_H
#define PERPETUALMOTIONDATA_H

#include <stdint.h>
#include <stdbool.h>
/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

typedef enum pm_mode {
  PM_ON,
  PM_OFF,
} pm_mode_t;

typedef enum pm_state {
  PM_IDLE,
  PM_DRIVING,
  PM_DOCKING,
  PM_ROTATE,
  PM_CHARGING,
  PM_ERROR,
} pm_state_t;

typedef enum pm_driving_modes{
  EF_DRIVING_STOP,
  EF_DRIVING_LEFT,
  EF_DRIVING_RIGHT,
  EF_DRIVING_ROTATE,
} pm_driving_modes_t;

//Debug message
typedef struct debug_msg {
  uint8_t msg_id;
  //float msg;   
  float msg[2];
} pm_debug_t;

typedef struct pm_debug_id {
    uint8_t id; // Debug messag only id is needed
} pm_debug_id_t;






/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* REMOTEDATA_H */

/** @} */
