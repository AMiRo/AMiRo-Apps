#ifndef BREAKOUTBOARDMANAGERD_H
#define BREAKOUTBOARDMANAGERD_H

#include <stdint.h>

typedef enum {
  V33 = 0,
  V50 = 1
} breakoutboard_control_action_t;

typedef struct {
  breakoutboard_control_action_t action;
  uint8_t param;
} breakoutboard_control_t;

#endif